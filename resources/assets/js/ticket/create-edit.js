showSubCategory($("#category option:selected").val());
$(document).ready(function (){
    $('#category').on('change', function () {
        showSubCategory($(this).val())
    });
    $("div[class^='image-preview']").each( function (){
        // $.uploadPreview({
        //     input_field: '.image-upload-',   // Default: .image-upload
        //     preview_box: '.image-preview',  // Default: .image-preview
        //     label_field: '.image-label',    // Default: .image-label
        //     label_default: 'Choose File',   // Default: Choose File
        //     label_selected: 'Change File',  // Default: Change File
        //     no_label: false,                // Default: false
        //     success_callback: null,          // Default: null
        // });
    });

    $.uploadPreview({
        input_field: '#image-upload',   // Default: .image-upload
        preview_box: '#image-preview',  // Default: .image-preview
        label_field: '#image-label',    // Default: .image-label
        label_default: 'Choose File',   // Default: Choose File
        label_selected: 'Change File',  // Default: Change File
        no_label: false,                // Default: false
        success_callback: null,          // Default: null
    });

    $(document).on('click','.chk-color',function (){
        if($(this).is(":checked")){
            $(this).parent().parent().next().removeClass('d-none');
            $(this).parent().parent().next().find('.color-img-input').attr('required',true);
        }else {
            $(this).parent().parent().next().addClass('d-none');
            $(this).parent().parent().next().find('.color-img-input').attr('required',false);
        }
    });

    $(document).on('click','.btn-add-file',function (){
        let html = $(this).siblings('.colo-input-main').find('.color-input-div:first').clone();
        html.append('<button type="button"  class="d-inline-block btn-icon-round ml-3  btn btn-sm btn-outline-danger btn-delete-file"><i class="fa fa-times-circle"></i></button>');
        $(this).siblings('.colo-input-main').append(html);
    });

    $(document).on('click','.btn-add-file',function (){
        let html = $(this).siblings('.colo-input-main').find('.color-input-div:first').clone();
        html.append('<button type="button"  class="d-inline-block btn-icon-round ml-3  btn btn-sm btn-outline-danger btn-delete-file"><i class="fa fa-times-circle"></i></button>');
        $(this).siblings('.colo-input-main').append(html);
    });

    $(document).on('click','.btn-delete-file',function (){
        $(this).parent('.color-input-div').remove();
    });
});
function showSubCategory(id){
    $.ajax({
        url: getSubCategoriesURL,
        type: 'get',
        dataType: 'json',
        data: { category_id: id },
        success: function (data) {
            $('#subCategory').empty();
            // $('#subCategory').
            //     append(
            //         $('<option value=""></option>').text('Select Sub Category'));
            $.each(data.data, function (i, v) {
                if(oldSubCategory != '' && oldSubCategory == i){
                    $('#subCategory').
                        append($('<option selected></option>').attr('value', i).text(v));
                }else{
                    $('#subCategory').
                        append($('<option></option>').attr('value', i).text(v));
                }
            });
        },
    });
}
