$(document).ready(function (){
    const create_pickr = Pickr.create({
        el: '#createForm .color-picker',
        theme: 'classic', // or 'monolith', or 'nano'

        swatches: [
            'rgba(244, 67, 54, 1)',
            'rgba(233, 30, 99, 0.95)',
            'rgba(156, 39, 176, 0.9)',
            'rgba(103, 58, 183, 0.85)',
            'rgba(63, 81, 181, 0.8)',
            'rgba(33, 150, 243, 0.75)',
            'rgba(3, 169, 244, 0.7)',
            'rgba(0, 188, 212, 0.7)',
            'rgba(0, 150, 136, 0.75)',
            'rgba(76, 175, 80, 0.8)',
            'rgba(139, 195, 74, 0.85)',
            'rgba(205, 220, 57, 0.9)',
            'rgba(255, 235, 59, 0.95)',
            'rgba(255, 193, 7, 1)'
        ],

        components: {

            // Main components
            preview: true,
            opacity: true,
            hue: true,

            // Input / output Options
            interaction: {
                hex: true,
                rgba: true,
                hsla: true,
                hsva: true,
                cmyk: true,
                input: true,
                clear: true,
                save: true
            }
        }
    });
    const edit_pickr = Pickr.create({
        el: '#editForm .color-picker',
        theme: 'classic', // or 'monolith', or 'nano'

        swatches: [
            'rgba(244, 67, 54, 1)',
            'rgba(233, 30, 99, 0.95)',
            'rgba(156, 39, 176, 0.9)',
            'rgba(103, 58, 183, 0.85)',
            'rgba(63, 81, 181, 0.8)',
            'rgba(33, 150, 243, 0.75)',
            'rgba(3, 169, 244, 0.7)',
            'rgba(0, 188, 212, 0.7)',
            'rgba(0, 150, 136, 0.75)',
            'rgba(76, 175, 80, 0.8)',
            'rgba(139, 195, 74, 0.85)',
            'rgba(205, 220, 57, 0.9)',
            'rgba(255, 235, 59, 0.95)',
            'rgba(255, 193, 7, 1)'
        ],

        components: {

            // Main components
            preview: true,
            opacity: true,
            hue: true,

            // Input / output Options
            interaction: {
                hex: true,
                rgba: true,
                hsla: true,
                hsva: true,
                cmyk: true,
                input: true,
                clear: true,
                save: true
            }
        }
    });
    $(document).on('submit', createFormId, function (e) {
        let formId = '#' + $(this).attr('id');
        $(formId).find('#name').val(create_pickr.getColor().toHEXA().toString());
        e.preventDefault();
        processingBtn(formId, '#btnSave', 'loading');
        let url = $(this).attr('action');
        let data = $(this).serialize();
        submitLivewireForm(url, 'POST', data, formId, createModelId);
    });

    $(document).on('click', deleteButtonSelector, function (event) {
        let Id = $(this).attr('data-id');
        let deleteUrl = indexUrl + Id;
        deleteLivewireItem(deleteUrl, 'Color');
    });
    
    $(document).on('click', editButtonSelector, function (event) {
        let id = $(this).attr('data-id');
        renderData(id);
    });

    window.renderData = function (id) {
        $.ajax({
            url: indexUrl + id + '/edit',
            type: 'GET',
            success: function (result) {
                if (result.success) {
                    let data = result.data;
                    $('#editId').val(data.id);
                    edit_pickr.setColor(data.name);
                    $(editModelId).appendTo('body').modal('show');
                }
            },
            error: function (result) {
                displayErrorMessage(result.responseJSON.message);
            },
        });
    };
    
    $(document).on('submit', editFormId, function (e) {
        e.preventDefault();
        let formId = '#' + $(this).attr('id');
        $(formId).find('#name').val(edit_pickr.getColor().toHEXA().toString());
        processingBtn(formId, '#btnSave', 'loading');
        const id = $('#editId').val();
        let url = indexUrl + id;
        let data = $(this).serialize();
        submitLivewireForm(url, 'PUT', data, formId, editModelId);
    });
    
});
