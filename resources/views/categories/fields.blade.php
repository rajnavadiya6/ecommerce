<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control '. ($errors->has('name') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
</div>

<div class="form-group col-sm-6">
    <div id="image-preview" class="image-preview {{ $errors->has('image') ? 'border-danger' :''  }}" style="background-image: url('{{ isset($category) ? $category->image : '' }}')">
        <label for="image-upload" id="image-label">Choose File</label>
        <input type="file" name="image" id="image-upload">
    </div>
    <div class="text-danger">{{ $errors->first('image') }}</div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('categories.index') }}" class="btn btn-light">Cancel</a>
</div>
