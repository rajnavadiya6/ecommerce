<div class="table-responsive-sm">
    <table class="table table-striped table-bordered" id="categories-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Image</th>
            <th class="action-column" style="width: 15%">Total Sub Categories</th>
            <th class="action-column">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{ $category->name }}</td>
                <td><img class="table-img" src="{{ $category->image }}" alt="no image"></td>
                <td class="text-center"><a href="{{ route('subCategories.index') }}" class="badge badge-primary">{{ $category->total_sub_category }}</a> </td>
                <td class="text-center">
                    {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('categories.show', [$category->id]) !!}" class='btn btn-light action-btn '><i
                                    class="fa fa-eye"></i></a>
                        <a href="{!! route('categories.edit', [$category->id]) !!}"
                           class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
