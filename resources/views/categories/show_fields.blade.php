<!-- Name Field -->
<div class="form-group col-md-6">
    {!! Form::label('name', 'Name:',['class' => 'font-weight-bold']) !!}
    <p>{{ $category->name }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:',['class' => 'font-weight-bold']) !!}
    <p>{{ $category->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:',['class' => 'font-weight-bold']) !!}
    <p>{{ $category->updated_at }}</p>
</div>

<!-- Image Field -->
<div class="form-group col-md-12">
    {!! Form::label('id', 'Image:',['class' => 'font-weight-bold']) !!}
    <img src="{{ $category->image }}" />
</div>
