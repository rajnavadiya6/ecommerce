@php
    $setting = \App\Models\Setting::pluck('value','key')->toArray();
@endphp
    <!DOCTYPE html>
<html lang="en">
<!--[if IE 9]>
<link href="/assets/css/bootstrap-ie9.min.css" rel="stylesheet">
<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="fragment" content="!">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> Accessories | {{config('app.name')}}</title>
    <link rel="shortcut icon" href="{{ $setting['favicon'] }}" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1">
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('theme/css/custom.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('theme/css/web_theme.css') }}" rel="stylesheet" type="text/css" media="all"/>
    @livewireStyles
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/pagination.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/hover-min.css') }}" />
</head>
<body>
<div class="ng-tns-c2-0 ">
    @include('by_pass.layout.header')
    <div class="col-xs-12 padding-zero"></div>
    <div class="popupblock popupFromBottom ng-tns-c2-0 " id="headerOfferPopup" style="display:none;">
        <div class="popuptbl">
            <div class="popuptr">
                <div class="popupinner">
                    <div class="popupcontentDiv offerTemplatePopup">
                        <a class="popupClose" href="javascript:void(0)" onclick="closePopup('headerOfferPopup')"><span class="kz-close"></span></a>
                        <div class="col-12 popupbody" appnavigation="">pdp image issue fixing
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div>
    <div class="col-12 mainProductlistdiv ">
        <div class="container">
            <div class="col-12 bredcrumbsBlock">
                <ul class="col-12 bredcrumbs">
                    <li>
                        <a href="{{ url('/') }}" class="router-link-active">Home</a>
                    </li>
                    <li>
                        <a href="#" class="router-link-active">Accessories</a>
                    </li>
                </ul>
            </div>
            <div class="mainProductlistdiv-inner">
                <div class="clearfix"></div>
                <div class="col-12 productTitle ">
                    <div class="productTitleInnerDiv ">
                        <h1>
                            <span>Accessories</span><small>({{ count($subCategoryProduct) }}
                                Items)</small>
                        </h1>
                    </div>
                </div>
                @if(Session::has('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('success') }}</strong>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('error') }}</strong>
                    </div>
                @endif
                <div class="row refineRow ">
                    <div class="refineSortBy">
                        <a class="btnRefine" href="javascript:void(0)" onclick="filtersOpen()">Refine
                            By</a>
                        <div class="sortByBlock">
                            <div class="sortByBlockInner">
                                <div class="selectedSortItem">Sort by</div>
                                <ul class="sortList">
                                    <li>Popular</li>
                                    <li>Price : Low to high</li>
                                    <li>Price : High to low</li>
                                    <li>Best seller</li>
                                </ul>
                            </div>
                        </div>
                        <div class="productsItemsCountMobile ">{{ count($subCategoryProduct) }} Items</div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row productsBlockRow">
                    @livewire('accessories-list-product')
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@include('by_pass.layout.footer')
<div class="scrollup" onclick="scrollUP()"><span class="kz-up-top"></span></div>
<script src="{{ asset('theme/js/main.js') }}"></script>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script src="{{ asset('assets/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ mix('assets/js/custom/custom.js') }}"></script>
<script src="{{ mix('assets/js/profile.js') }}"></script>

{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
    let removeProductToBag = '{{ route('removeProductToBag') }}';

    $(document).on('click', '.color-hover', function () {
        let id = $(this).attr('data-id');
        let url = $(this).attr('data-url');
        document.querySelector('.product-image-' + id).src = url;
    });
    $('.selectedSortItem').click(function () {
        $(this).siblings('.sortList').toggleClass('sortByBlockActive');
    });

    $('.categoryMenu').click(function () {
        $(this).siblings('.filterListBody').toggleClass('sortByCategoryOpen');
        $(this).siblings('.filterListBody').toggleClass('panel-collapseActive');
    });
    $('.mobileFilterCategoryHeading').click(function (){
        $(this).parents('.filterListBody').removeClass('panel-collapseActive');
    });
    // close Mobile model
    $('.btnCloseFilters').click(function (){
        $(this).siblings('.panel-group').removeClass('panel-groupActive');
        $(this).parents('.filtersPanel').removeClass('filtersActive');
        $(this).siblings('.filterListBody').removeClass('panel-collapseActive');
    });

    // Footer
    $('.footerData').click(function (){
        $(this).toggleClass('footerheadActive');
        $(this).siblings('.footerLinkDiv').toggleClass('displayData');
    });
</script>
<style>
    .displayData{
        display:block !important;
    }
    @media screen and (min-width: 800px) {
        .sortByCategoryOpen{
            display: none !important;
        }
    }
</style>
@livewireScripts
</body>
</html>


