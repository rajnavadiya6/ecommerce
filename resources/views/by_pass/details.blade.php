@php
  $setting = \App\Models\Setting::pluck('value','key')->toArray();
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> Details | {{config('app.name')}}</title>
    <link rel="shortcut icon" href="{{ $setting['favicon'] }}" type="image/x-icon">
    <link href="{{ asset('theme/css/custom.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('theme/css/men_details.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
          rel="stylesheet">
    <link href="{{ asset('theme/css/web_theme.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/pagination.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/hover-min.css') }}" />
    @livewireStyles
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'AW-653151205');

    </script>
    <!-- End of global snippet: Please do not remove -->
</head>

<body>
    <app-root _nghost-sc0="" ng-version="8.2.14">
        <ng-progress  _nghost-sc1="">
            <ng-progress-bar  _nghost-sc4="">
                <!---->
                <div _ngcontent-sc4="" class="ng-progress ng-star-inserted">
                    <div _ngcontent-sc4="" class="bar"
                         style="transition:all 200ms linear;background-color:rgb(255, 255, 255);margin-left:-100%;">
                        <div _ngcontent-sc4="" class="bar-shadow"
                             style="box-shadow:0 0 10px rgb(255, 255, 255), 0 0 5px rgb(255, 255, 255);"></div>
                    </div>
                    <!---->
                </div>
            </ng-progress-bar>
        </ng-progress>
        <app-header _nghost-sc2="" class="ng-tns-c2-0 ">
            @include('by_pass.layout.header')
                <div class="col-xs-12 padding-zero">
                </div>
                <div class="popupblock popupFromBottom ng-tns-c2-0 " id="headerOfferPopup" style="display:none;">
                <div class="popuptbl">
                    <div class="popuptr">
                        <div class="popupinner">
                            <div class="popupcontentDiv offerTemplatePopup"><a
                                    class="popupClose" href="javascript:void(0)"
                                    onclick="closePopup('headerOfferPopup')"><span
                                        class="kz-close"></span></a>
                                <div class="col-12 popupbody" appnavigation="">pdp image issue fixing
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </app-header>
        <div class="clearfix"></div>
        <router-outlet></router-outlet>
        <app-product-details class="">
            <div class="col-12 bodyContent ">
                <div class="row pdpRowBlock">
                    <div class="container">
                        <div class="col-12 bredcrumbsBlock">
                            <ul class="col-12 bredcrumbs ">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="">
                                    <a href="{{ route('new_arrivals') }}" routerlinkactive="router-link-active">Men</a>
                                </li>
                                <li class="">
                                    <a href="#" routerlinkactive="router-link-active">Details</a>
                                </li>
                            </ul>
                        </div>
                        <app-offer-template class="">
                            <div class="col-12 offer-placementDiv" onclick="openPopup('offerPopup')">
                                <div appnavigation="" class="offer-placementDivinner " compile="">
                                    <style>
                                        .pdppageblankimg,
                                        .listpageblankimg {
                                            opacity: 0 !important;
                                        }
                                    </style>
                                </div>
                            </div>
                            <div class="popupblock " id="pageofferpopup" style="display:none">
                                <div class="popuptbl">
                                    <div class="popuptr">
                                        <div class="popupinner">
                                            <div class="popupcontentDiv pageofferpopup">
                                                <a class="popup-close" href="javascript:void(0)"
                                                    onclick="closePopup('pageofferpopup')">
                                                </a>
                                                <div appnavigation="" class="col-12 padding-0">pdp
                                                    image issue fixing
                                                </div>
                                                <a class="btnClose-popup" href="javascript:void(0)"
                                                   onclick="closePopup('pageofferpopup')">Close
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="popupblock popupFromBottom " id="offerPopup" style="display:none;">
                                <div class="popuptbl">
                                    <div class="popuptr">
                                        <div class="popupinner">
                                            <div class="popupcontentDiv offerTemplatePopup">
                                                <a class="popupClose" href="javascript:void(0)"
                                                    onclick="closePopup('offerPopup')">
                                                    <span class="kz-close"></span>
                                                </a>
                                                <div appnavigation="" class="col-12 popupbody">pdp
                                                    image issue fixing
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </app-offer-template>
                        <div class="clearfix"></div>
                        @livewire('details',['productId' => $id])
                        <app-recommended>
                        <div class="clearfix"></div>
                        <div class="col-12 newArrivalsItemsBlock relatedProductsBlock ">
                            <h2 class="rcomTitle">Recommended For You</h2>
                            <div class="relatedProductsSlider">
                            {{-- Image Silder--}}
                                <div class="slideItemDiv">
                                    <div class="col-12 popularProBlock">
                                        <div class="popularProDivBlock">
                                            <div class="image-container_slider cursor-pointer">
                                                @foreach($products as $prod)
                                                    <a href="{{ route('men.details',$prod->id) }}" class="container image-container mr-2 ml-2 mb-5 hvr-grow"
                                                         style="outline: none;">
                                                        <div class="image-single-slider single-brand-logo mx-2 my-6 d-inline">
                                                            <img src="{{ $prod->image }}" alt="brands_logo" style="object-fit: cover;width: 230px;height: 350px;"/>
                                                        </div>
                                                        <div class="content mt-2 w-100 text-center">
                                                            <h6>{{ $prod->name }}</h6>
                                                            <span
                                                                class="spnNewArrPrice">MRP ₹ {{ $prod->price }}.00</span>
                                                        </div>
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </app-recommended>
                </div>
            </div>
        </div>
    </app-product-details>
    <div class="clearfix"></div>
        @include('by_pass.layout.footer')
        <div  class="scrollup" onclick="scrollUP()">
            <span  class="kz-up-top"></span>
        </div>
</app-root>

    <div class="scrollup" onclick="scrollUP()"><span class="kz-up-top"></span></div>
    @routes

    <script src="{{ asset('theme/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/moment.min.js') }}"></script>
    <script src="{{ mix('assets/js/custom/custom.js') }}"></script>
    <script>
        let registerURL = '{{ route('register') }}';
        let removeProductToBag = '{{ route('removeProductToBag') }}';
    </script>
    <script src="{{ mix('assets/js/profile.js') }}"></script>
    <script>
        // Footer
        $('.footerData').click(function (){
            $(this).toggleClass('footerheadActive');
            $(this).siblings('.footerLinkDiv').toggleClass('displayData');
        });

        $('.sideBarImage').on('click',function (){
            let image = $(this).attr('src');
            $('.sideBarImageDisplay').attr('src',image);
        });

        $(document).on('click','.productColorImage',function (){
            let image = $(this).children().children().children().children().attr('src');
            $('.sideBarImageDisplay').attr('src',image);
        });

        // $(document).on('click','.variantproductSize',function () {
        //     document.addEventListener('livewire:load', function (event) {
        //         let id = $(this).attr('data-id');
        //         setTimeout(function () {
        //             $(this).addClass('sizeSelected');
        //         }, 2000);
        //     });
        // });

        $(document).on('click','.variantproductSize',function (){
            let id = $(this).attr('data-id');
            $(this).addClass('sizeSelected');
        });

        $('.image-container_slider').slick({
            dots: false,
            infinite: true,
            arrows: false,
            speed: 500,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            arrow:true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ],
        });

        window.addEventListener('refresh', function () {
            location.reload();
        });
    </script>
    @livewireScripts

    <style>
        .displayData{
            display:block !important;
        }

        .brand-logo {
                @media screen and (min-width: 768px) {
                    width: 180px;
                    text-align: center;
                }
            padding: 5px 0;
        }

        /* Image sldier */
        .image-container_slider {;
        .iamge-slick-slide {
            outline: none;
            height: auto;
            text-align: center;

                img {
                    display: inline-block;
                }
            }
        }

        /*.owl-carousel .owl-item {*/
        /*     background: white !important;*/
        /*    border: 1px solid gainsboro !important;*/
        /*}*/
        /*.owl-prev {*/
        /*    color: white !important;*/
        /*    position: absolute;*/
        /*    left: 25px;*/
        /*    top: 28px;*/
        /*    font-size: 38px !important;*/
        /*    outline: none !important;*/
        /*    opacity: 1;*/
        /*}*/
        /*.owl-next{*/
        /*    outline: none !important;*/
        /*    opacity: 1;*/
        /*    right: 25px;*/
        /*    position: absolute;*/
        /*    top: 28px;*/
        /*    color: white !important;*/
        /*    font-size: 38px !important;*/
        /*}*/

        .image-container{
            transition-duration: 1s;
            box-shadow: 0 8px 12px 0 rgb(0 0 0 / 10%);
            border-radius: 4%;
        }

        .cartGlobalblock {
            background: #fff;
            height: 100%;
            float: right;
            border: 0;
            position: absolute;
            top: 0;
            right: 0;
            z-index: 25;
            overflow-y: auto;
            overflow-x: hidden;
            transition: all ease-in-out .8s
        }

        .btnLoginTab {
            float: left;
            width: 50%;
            padding: 20px;
            font-family: 'Jost* 500';
            font-size: 12px;
            color: #a6a6a6;
            text-align: center;
            text-transform: uppercase;
            border-bottom: 2px solid #e5e5e5;
            transition: border-color .6s linear;
            -webkit-transition: border-color .6s linear;
        }

        .btnLoginTabSelected {
            color: #000;
            border-bottom-color: #000;
        }
    </style>
</body>
</html>
