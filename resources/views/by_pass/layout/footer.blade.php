@php
    $setting = \App\Models\Setting::pluck('value', 'key')->toArray();
    $setting['footer_image'] = \App\Models\Setting::whereId(5)->first()->toArray()['footer_image_url'];
@endphp
<footer>
    <div class="clearfix"></div>
    <footer class="col-12 footerContainer">
        <div class="container">
{{--            <div class="footerThreeBlock">--}}
{{--            <div class="col-4 footerContactDetails float-right footerNewsletterBlock">--}}
{{--                <h4>Manufactured By</h4>--}}
{{--                <div class="col-12 padding-0">--}}
{{--                    <div class="contactAddress spnPageindus"> {{ $setting['manufactured_company_name'] }}--}}
{{--                    </div>--}}
{{--                    <div class="contactAddress contactAddressManfactred">--}}
{{--                        {!! $setting['manufactured_company_address'] !!}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="row footerMobileLinks">--}}
{{--                <div class="col-4 footerLeftLinks align-items-center">--}}
{{--                    <a class="franchiseLinkFooter" href="#">Apply--}}
{{--                        for franchise</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-4 footerContactDetails">--}}
{{--                <h4 class="footerData">Contact</h4>--}}
{{--                <div class="col-12 padding-0 footerLinkDiv" id="divContact">--}}
{{--                    <div class="contactAddress"> {!! $setting['address'] !!}--}}
{{--                    </div>--}}
{{--                    <div class="clearfix"></div>--}}
{{--                    <a class="contactMobile" href="{{ $setting['phone'] }}">--}}
{{--                        <div>{{ $setting['phone'] }}--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <div class="clearfix"></div>--}}
{{--                    <a class="contactEmail reverse" href="mailto:{{ $setting['email'] }}" title="care@jockeyindia.com">--}}
{{--                        {{ $setting['email'] }}</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="container">
            <div class="col-12 footerLinks">
                <div class="row">

                    <div class="col-3 footerRightinner ">
                        <h6 class="footerData">Get in touch</h6>
                        <div class="col-12 padding-0 footerLinkDiv" id="divContact">
                            <div class="contactAddress"> {!! $setting['address'] !!}
                            </div>
                            <div class="clearfix"></div>
                            <a class="contactEmail reverse" href="mailto:{{ $setting['email'] }}" title="{{ $setting['email'] }}">
                                {{ $setting['email'] }}</a>
                            <div class="clearfix"></div>
                            <a class="contactMobile" href="{{ $setting['phone'] }}">
                                <div>{{ $setting['phone'] }}
                                </div>
                            </a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
{{--                    <div class="col-4 footerLeftLinks align-items-center">--}}
{{--                        <a class="locateAStoreLink" href="https://stores.jockey.in/" target="_blank">Locate a--}}
{{--                            store</a><a class="franchiseLinkFooter" href="#">Apply--}}
{{--                            for franchisee</a>--}}
{{--                    </div>--}}
                    <div class="col-8 footerRightLinks">
                        <div class="row">
                            <div class="col-3 footerRightinner ">
                                <h6 class="footerData">Categories</h6>
                                <div class="col-12 footerLinkDiv " id="footerLinksHeadings1">
                                    <ul class="ulFooterLink">
                                        <li>
                                            <a href="{{ route('new_arrivals') }}">MENS</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('women_arrivals') }}">WOMENS</a>
                                        </li>
                                        <li>
                                            <a href="#">ACCESSORIES</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-3 footerRightinner ">
                                <h6 class="footerData">Information</h6>
                                <div class="col-12 footerLinkDiv " id="footerLinksHeadings3">
                                    <ul class="ulFooterLink">
                                        <li>
                                            <a href="{{ route('page.privacy-policy') }}">Privacy
                                                Policy</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('page.return-policy') }}">Return
                                                Policies</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('page.offer-policy') }}">Offer
                                                Policies</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('page.return-exchange') }}">Returns &amp; Exchange</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-3 footerRightinner ">
                                <h6 class="footerData">Quick Links</h6>
                                <div class="col-12 footerLinkDiv " id="footerLinksHeadings0">
                                    <ul class="ulFooterLink">
                                        {{--                                        <li>--}}
                                        {{--                                            <a href="#" target="_blank">FAQs</a>--}}
                                        {{--                                        </li>--}}
                                        <li>
                                            <a href="{{ route('page.contact-us') }}">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-3 footerRightinner ">
                                <h6 class="footerData">DMCA Protection</h6>
                                <div class="col-12 footerLinkDiv " id="footerLinksHeadings2">
                                    <ul class="ulFooterLink">
                                        <li>
{{--                                            <img src="{{ asset('img/dmca_protection.jpg') }}" class="w-100">--}}
                                            <img src="{{ $setting['footer_image'] }}" class="w-100">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="mt-3">
                        @if($setting['facebook_url'] != null)
                            <a class="socialLink" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Facebook" href="{{ $setting['facebook_url'] }}">
                                <span class="kz-facebook"></span>
                            </a>
                        @endif
                        @if($setting['instagram_url'] != null)
                            <a class="socialLink" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Instagram" href="{{ $setting['instagram_url'] }}">
                                <span class="kz-instagram"></span>
                            </a>
                        @endif
                        @if($setting['twitter_url'] != null)
                            <a class="socialLink" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Twitter" href="{{ $setting['twitter_url'] }}">
                                <span class="kz-twitter"></span>
                            </a>
                        @endif
                        @if($setting['pinterest_url'] != null)
                            <a class="socialLink" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Pinterest" href="{{ $setting['pinterest_url'] }}">
                                <span class="kz-pinterest"></span>
                            </a>
                        @endif
                        @if($setting['youtube_url'] != null)
                            <a class="socialLink iconYoutube" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Youtube" href="{{ $setting['youtube_url'] }}"></a>
                        @endif
                        <div class="clearfix"></div>
                        <h6 style="margin-left: 8px;">Find Us On</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
{{--        <div class="col-12 footerCopyrights">--}}
{{--            <div class="footer-inner-container">--}}
{{--                <div class="container">--}}
{{--                    <div class="footerSocialLink">--}}
{{--                        @if($setting['facebook_url'] != null)--}}
{{--                        <a class="socialLink" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Facebook" href="{{ $setting['facebook_url'] }}">--}}
{{--                            <span class="kz-facebook"></span>--}}
{{--                        </a>--}}
{{--                        @endif--}}
{{--                        @if($setting['instagram_url'] != null)--}}
{{--                        <a class="socialLink" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Instagram" href="{{ $setting['instagram_url'] }}">--}}
{{--                            <span class="kz-instagram"></span>--}}
{{--                        </a>--}}
{{--                        @endif--}}
{{--                        @if($setting['twitter_url'] != null)--}}
{{--                        <a class="socialLink" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Twitter" href="{{ $setting['twitter_url'] }}">--}}
{{--                            <span class="kz-twitter"></span>--}}
{{--                        </a>--}}
{{--                        @endif--}}
{{--                        @if($setting['pinterest_url'] != null)--}}
{{--                        <a class="socialLink" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Pinterest" href="{{ $setting['pinterest_url'] }}">--}}
{{--                            <span class="kz-pinterest"></span>--}}
{{--                        </a>--}}
{{--                        @endif--}}
{{--                        @if($setting['youtube_url'] != null)--}}
{{--                        <a class="socialLink iconYoutube" rel="nofollow noopener noreferrer" target="_blank" title="Jockey India Youtube" href="{{ $setting['youtube_url'] }}"></a>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                    <div class="copyRight"><span class="copy">©2013 - {{ \Carbon\Carbon::now()->format('Y') }}--}}
{{--                        Bypass all rights reserved</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </footer>
</footer>
