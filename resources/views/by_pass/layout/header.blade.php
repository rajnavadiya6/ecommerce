@php
    $menCategories = \App\Models\SubCategory::whereCategoryId('1')->get();
    $woMenCategory = \App\Models\SubCategory::whereCategoryId('2')->get();
    $accessories = \App\Models\SubCategory::whereCategoryId('3')->get();
    $setting = \App\Models\Setting::pluck('value','key')->toArray();
    $setting['logo'] = \App\Models\Setting::whereId(3)->first()->toArray()['logo_url'];
    $addToCarts = \App\Models\AddToBag::where('user_id',Auth::id())->get();
@endphp
<header  class="col-12 headerContainer" id="scrollUp">
    <div  class="col-12 headerTop">
        <div  class="container">
            <div  class="headerOffers  ng-star-inserted"
                 onclick="openPopup('headerOfferPopup')">
                <div  class="headerOfferInner" appnavigation="" compile="">
                    <style>
                        .pdppageblankimg,
                        .listpageblankimg {
                            opacity: 0 !important;
                        }
                    </style>
                </div>
            </div>
            <div class="row" style="margin-top: -15px;">
                <div class="col-6">
                    <div class="float-left text-white">
                        <span class="kz-phone"></span> {{ $setting['phone'] }}  |  <span class="kz-email"></span> {{ $setting['email'] }}
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-white" style="text-align: start;">
                        {{ $setting['home_header_title'] }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div  class="container">
        <div  class="row flex-nowrap">
            <div  class="float-left brandLogoFloatLeft ml-5">
                <a  class="brandLogo router-link-active" href="{{ url('/') }}"
                    routerlinkactive="router-link-active">
                    <img src="{{ asset($setting['logo']) }}">
                </a>
            </div>
            <a  class="iconMenu" href="javascript:void(0)" onclick="menuOpen()"></a>

            <div  class="float-right">
                <div  class="clearfix"></div>
                <div  class="linkHeaderLogin d-inline">
{{--                    <span  class="kz-search-space"></span>--}}
{{--                    <small  class="">Search</small></div>--}}
                @if(Auth::check())
                    <a  class="linkHeaderLogin  ng-star-inserted showLoginModal"
                        href="{{ route('mySetting') }}">
                        <span class="kz-user"></span>
                        <small class="">My account</small>
                    </a>
                @else
                    <a class="linkHeaderLogin  ng-star-inserted showLoginModal"
                        href="javascript:void(0)"  data-toggle="modal" data-target="#showLoginModal">
                        <span class="kz-user"></span>
                        <small class="">Login</small>
                    </a>
                    <a class="linkHeaderLogin  ng-star-inserted showLoginModal"
                       href="javascript:void(0)"  data-toggle="modal" data-target="#showSignUpModal">
                        <span class="kz-user"></span>
                        <small class="">SignUp</small>
                    </a>
                @endif
                <a  class="linkHeaderCart showCartModal"  data-toggle="modal" data-target="#showCartModal" href="javascript:void(0)">
                    @if(count($addToCarts) > 0)
                        <span class="cartCount" style="width: 18px;height: 18px;margin: 0;position: absolute;z-index: 2;font-size: 10px;color: #fff;background: #151515;text-align: center;border-radius: 50%;left: 29px;top: 0;line-height: 18px;">
                            {{ count($addToCarts) }}
                        </span>
                    @endif
                    <small class="">My Bag</small>
                </a>
            </div>
        </div>
    </div>
    <div  class="clearfix"></div>
    <div  class="headerMenu">
        <a  class="menuClose" href="javascript:void(0)" onclick="menuClose()"></a>
        <nav  class="navbar navbar-expand-sm justify-content-center">
            <ul  class="navbar-nav  ng-star-inserted">
                <li  class="nav-item  ng-star-inserted">
                    <a  class="nav-link  ng-star-inserted" href="javascript:void(0)"
                              onclick="fncMegaMenu(event)">Men</a>
                    <div  class="megamenuDiv-block" appnavigation="" id="jockeyHeaderMenu0">
                        <div class="col-12 megamenuDiv-innerblock">
                            <div class="mobileMenuHeading"
                                 onclick="fncMegaBack('megamenuDiv-block', 'megamenuDivAcive')">Men
                            </div>
                            <div class="megamenu-inner-content">
                                <div class="col-12 menuBlock">
                                    <div class="row">
                                        <div class="col-3 menuLeft hideInMobile">
                                            {{--                                            <ul class="menuList float-right">--}}
                                            {{--                                                <li><a href="{{ route('new_arrivals')}}">New Arrivals</a></li>--}}
                                            {{--                                                <li><a href="multi-packs.html">multipacks </a></li>--}}
                                            {{--                                                <li><a href="accessories/unisex-face-mask.html">Adult Unisex--}}
                                            {{--                                                        Masks</a></li>--}}
                                            {{--                                                <li><a href="thermals">Thermals</a></li>--}}
                                            {{--                                                <li><a href="socks.html">Socks</a></li>--}}
                                            {{--                                                <li><a href="towel.html">Towels</a></li>--}}
                                            {{--                                            </ul>--}}
                                        </div>
                                        <div class="col-6 menuMiddle">
                                            <div class="row">
                                                {{--                                                <div class="col-4 menuMiddleInnerLeft">--}}
                                                {{--                                                    <ul class="menuList fullWidth">--}}
                                                {{--                                                        <li>--}}
                                                {{--                                                            <a href="innerwear.html">Innerwear</a>--}}
                                                {{--                                                            <div class="subMenuDiv" id="menInnerwearMenu">--}}
                                                {{--                                                                <div class="mobileMenuHeading"--}}
                                                {{--                                                                     onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                {{--                                                                    Innerwear--}}
                                                {{--                                                                </div>--}}
                                                {{--                                                                <div class="clearfix"></div>--}}
                                                {{--                                                                <ul class="subMenuList">--}}
                                                {{--                                                                    <li><a href="innerwear/briefs.html">Briefs</a>--}}
                                                {{--                                                                    </li>--}}
                                                {{--                                                                    <li><a href="innerwear/trunks.html">Trunks</a>--}}
                                                {{--                                                                    </li>--}}
                                                {{--                                                                    <li><a href="innerwear/boxer-briefs.html">Boxer--}}
                                                {{--                                                                            Briefs</a></li>--}}
                                                {{--                                                                    <li><a href="innerwear/inner-boxers.html">Inner--}}
                                                {{--                                                                            Boxers</a></li>--}}
                                                {{--                                                                </ul>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </li>--}}
                                                {{--                                                        <li>--}}
                                                {{--                                                            <a href="vests.html">Vests</a>--}}
                                                {{--                                                            <div class="subMenuDiv" id="menVestMenu">--}}
                                                {{--                                                                <div class="mobileMenuHeading"--}}
                                                {{--                                                                     onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                {{--                                                                    Vests--}}
                                                {{--                                                                </div>--}}
                                                {{--                                                                <ul class="subMenuList">--}}
                                                {{--                                                                    <li><a href="vests/sleeveless-vests.html">Sleeveless--}}
                                                {{--                                                                            Vests</a></li>--}}
                                                {{--                                                                    <li><a href="vests/sleeved-vests.html">Sleeved--}}
                                                {{--                                                                            Vests</a></li>--}}
                                                {{--                                                                    <li><a href="vests/gym-vests.html">Gym--}}
                                                {{--                                                                            Vests</a></li>--}}
                                                {{--                                                                </ul>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </li>--}}
                                                {{--                                                    </ul>--}}
                                                {{--                                                </div>--}}
                                                <div class="col-12 menuMiddleInnerRight">
                                                    <ul class="menuList halfWidth">
                                                        <li>
                                                            {{--                                                            <a href="apparel-tops.html">Apparel Tops</a>--}}
                                                            <div class="subMenuDiv" id="menApparelTops">
                                                                {{--                                                                <div class="mobileMenuHeading"--}}
                                                                {{--                                                                     onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                                {{--                                                                    Apparel Tops--}}
                                                                {{--                                                                </div>--}}
                                                                <ul class="subMenuList">
                                                                    @foreach($menCategories as $menCategory)
                                                                        <li><a href="{{ url('men?category='.$menCategory->name) }}">{{ $menCategory->name }}</a></li>
                                                                    @endforeach
                                                                    {{--                                                                    <li><a href="{{ route('new_arrivals') }}">Shirt</a></li>--}}
                                                                    {{--                                                                    <li>--}}
                                                                    {{--                                                                        <a href="apparel-tops/t-shirts.html">T-shirts</a>--}}
                                                                    {{--                                                                    </li>--}}
                                                                    {{--                                                                    <li><a href="apparel-tops/polos.html">Woodies </a>--}}
                                                                    {{--                                                                    </li>--}}
                                                                    {{--                                                                    <li><a href="apparel-tops/henleys.html">Track pants</a>--}}
                                                                    {{--                                                                    </li>--}}
                                                                    {{--                                                                    <li><a href="apparel-tops/sweatshirts.html">Joggers</a>--}}
                                                                    {{--                                                                    </li>--}}
                                                                    {{--                                                                    <li><a href="apparel-tops/jackets-hoodies.html">Shorts</a></li>--}}
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <ul class="menuList halfWidth">
                                                        <li>
                                                            {{--                                                            <a href="apparel-bottoms.html">Apparel--}}
                                                            {{--                                                                Bottoms</a>--}}
                                                            <div class="subMenuDiv" id="menApparelBottoms">
                                                                {{--                                                                <div class="mobileMenuHeading"--}}
                                                                {{--                                                                     onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                                {{--                                                                    Apparel Bottoms--}}
                                                                {{--                                                                </div>--}}
                                                                {{--                                                                <ul class="subMenuList">--}}
                                                                {{--                                                                    <li><a href="apparel-bottoms/boxer-shorts.html">Underwear</a></li>--}}
                                                                {{--                                                                    <li><a href="apparel-bottoms/shorts.html">Socks</a>--}}
                                                                {{--                                                                    </li>--}}
                                                                {{--                                                                </ul>--}}
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Men Mobile Menu --}}
                                        <div class="col-3 menuLeft showInMobile">
                                            <ul class="menuList float-right">
                                                @foreach($menCategories as $menCategory)
                                                    <li><a href="{{ url('men?category='.$menCategory->name) }}">{{ $menCategory->name }}</a></li>
                                                @endforeach
                                                {{--                                                    <li><a href="{{ route('new_arrivals') }}">Shirt</a></li>--}}
                                                {{--                                                    <li>--}}
                                                {{--                                                        <a href="apparel-tops/t-shirts.html">T-shirts</a>--}}
                                                {{--                                                    </li>--}}
                                                {{--                                                    <li><a href="apparel-tops/polos.html">Woodies </a>--}}
                                                {{--                                                    </li>--}}
                                                {{--                                                    <li><a href="apparel-tops/henleys.html">Track pants</a>--}}
                                                {{--                                                    </li>--}}
                                                {{--                                                    <li><a href="apparel-tops/sweatshirts.html">Joggers</a>--}}
                                                {{--                                                    </li>--}}
                                                {{--                                                    <li><a href="apparel-tops/jackets-hoodies.html">Shorts</a></li>--}}
                                                {{--                                                    <li><a href="apparel-bottoms/boxer-shorts.html">Underwear</a></li>--}}
                                                {{--                                                    <li><a href="apparel-bottoms/shorts.html">Socks</a>--}}
                                                {{--                                                    </li>--}}
                                            </ul>
                                        </div>
                                        <div class="col-3 menuRight">
                                            {{--                                            <ul class="menuList">--}}
                                            {{--                                                <li>--}}
                                            {{--                                                    <a href="javascript:void(0)"--}}
                                            {{--                                                       onclick="fncMegaMenuSecondLevel('menShopbyCollection', this)">Shop--}}
                                            {{--                                                        By Collection</a>--}}
                                            {{--                                                    <div class="subMenuDiv" id="menShopbyCollection">--}}
                                            {{--                                                        <div class="mobileMenuHeading"--}}
                                            {{--                                                             onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                            {{--                                                            Shop By Collection--}}
                                            {{--                                                        </div>--}}
                                            {{--                                                        <div class="clearfix"></div>--}}
                                            {{--                                                        <ul class="subMenuList">--}}
                                            {{--                                                            <li><a href="collection/modern-classic.html">Modern--}}
                                            {{--                                                                    Classic</a></li>--}}
                                            {{--                                                            <li><a href="collection/usa-originals.html">USA--}}
                                            {{--                                                                    Originals</a></li>--}}
                                            {{--                                                            <li><a href="collection/international-collection.html">International--}}
                                            {{--                                                                    Collection</a></li>--}}
                                            {{--                                                            <li><a href="collection/heritage.html">Heritage--}}
                                            {{--                                                                    Collection</a></li>--}}
                                            {{--                                                            <li><a href="collection/new-york-city.html">NYC--}}
                                            {{--                                                                    Collection</a></li>--}}
                                            {{--                                                            <li><a href="collection/1876.html">1876</a></li>--}}
                                            {{--                                                            <li><a href="collection/move.html">Move</a></li>--}}
                                            {{--                                                            <li><a href="collection/athleisure.html">Athleisure</a>--}}
                                            {{--                                                            </li>--}}
                                            {{--                                                            <li><a href="collection/elance.html">Elance</a>--}}
                                            {{--                                                            </li>--}}
                                            {{--                                                            <li><a href="collection/zone.html">Zone</a></li>--}}
                                            {{--                                                            <li><a href="collection/zone-stretch.html">Zone--}}
                                            {{--                                                                    Stretch</a></li>--}}
                                            {{--                                                            <li><a href="collection/pop.html">Pop Colour</a>--}}
                                            {{--                                                            </li>--}}
                                            {{--                                                            <li><a href="collection/sport-performance.html">Sport--}}
                                            {{--                                                                    Performance</a></li>--}}
                                            {{--                                                        </ul>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                </li>--}}
                                            {{--                                            </ul>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li  class="nav-item  ng-star-inserted">
                    <!----><a  class="nav-link  ng-star-inserted" href="javascript:void(0)"
                              onclick="fncMegaMenu(event)">Women</a>
                    <!---->
                    <div  class="megamenuDiv-block" appnavigation="" id="jockeyHeaderMenu1">
                        <div class="col-12 megamenuDiv-innerblock">
                            <div class="mobileMenuHeading"
                                 onclick="fncMegaBack('megamenuDiv-block', 'megamenuDivAcive')">Women
                            </div>
                            <div class="megamenu-inner-content womenMegaMenuContent">
                                <div class="col-12 menuBlock">
                                    <div class="row">
                                        <div class="col-3 menuLeft hideInMobile">
                                            {{--                                            <ul class="menuList float-right">--}}
                                            {{--                                                <li><a href="../women/new-arrivals.html">T-shirt</a>--}}
                                            {{--                                                </li>--}}
                                            {{--                                                <li><a href="../women/multi-packs.html">Track pants</a></li>--}}
                                            {{--                                                <li><a href="../women/thermals.html">Thermals</a></li>--}}
                                            {{--                                                <li><a href="../women/shapewear.html">Shorts </a></li>--}}
                                            {{--                                                <li><a href="../women/socks.html">Sport bra--}}
                                            {{--                                                    </a></li>--}}
                                            {{--                                                <li><a href="../women/towel.html">Running bra--}}
                                            {{--                                                    </a></li>--}}
                                            {{--                                            </ul>--}}
                                        </div>
                                        <div class="col-6 menuMiddle">
                                            <div class="row">
                                                {{--                                                <div class="col-6 menuMiddleInnerLeft">--}}
                                                {{--                                                    <div class="row">--}}
                                                {{--                                                        <div class="col-6 padding-0">--}}
                                                {{--                                                            <ul class="menuList fullWidth">--}}
                                                {{--                                                                <li>--}}
                                                {{--                                                                    <a href="../women/bras.html">Bras</a>--}}
                                                {{--                                                                    <div class="subMenuDiv" id="womenBrasMenu">--}}
                                                {{--                                                                        <div class="mobileMenuHeading"--}}
                                                {{--                                                                             onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                {{--                                                                            Bras--}}
                                                {{--                                                                        </div>--}}
                                                {{--                                                                        <div class="clearfix"></div>--}}
                                                {{--                                                                        <ul class="subMenuList">--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/bras/everyday-bras.html">Everyday--}}
                                                {{--                                                                                    Bras</a></li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/bras/t-shirt-bras.html">T-shirt--}}
                                                {{--                                                                                    Bras</a></li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/bras/active-bras.html">Active--}}
                                                {{--                                                                                    Bras</a></li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/bras/beginners-bras.html">Beginners--}}
                                                {{--                                                                                    Bras</a></li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/bras/strapless-bras.html">Strapless--}}
                                                {{--                                                                                    Bras</a></li>--}}
                                                {{--                                                                            <li><a href="../women/bras/sleep-bras.html">Sleep--}}
                                                {{--                                                                                    Bras</a></li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/bras/nursing-bras.html">Nursing--}}
                                                {{--                                                                                    Bras</a></li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/bras/plus-size-bras.html">Plus--}}
                                                {{--                                                                                    Size Bras</a></li>--}}
                                                {{--                                                                        </ul>--}}
                                                {{--                                                                    </div>--}}
                                                {{--                                                                </li>--}}
                                                {{--                                                            </ul>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                        <div class="col-6 padding-0">--}}
                                                {{--                                                            <ul class="menuList fullWidth">--}}
                                                {{--                                                                <li>--}}
                                                {{--                                                                    <a href="../women/panties.html">Panties</a>--}}
                                                {{--                                                                    <div class="subMenuDiv" id="womenPantiesMenu">--}}
                                                {{--                                                                        <div class="mobileMenuHeading"--}}
                                                {{--                                                                             onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                {{--                                                                            Panties--}}
                                                {{--                                                                        </div>--}}
                                                {{--                                                                        <div class="clearfix"></div>--}}
                                                {{--                                                                        <ul class="subMenuList">--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/panties/hipsters.html">Hipsters</a>--}}
                                                {{--                                                                            </li>--}}
                                                {{--                                                                            <li><a href="../women/panties/bikinis.html">Bikinis</a>--}}
                                                {{--                                                                            </li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/panties/shorties.html">Shorties</a>--}}
                                                {{--                                                                            </li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/panties/full-briefs.html">Full--}}
                                                {{--                                                                                    Briefs</a></li>--}}
                                                {{--                                                                            <li><a href="../women/panties/boy-leg.html">Boy--}}
                                                {{--                                                                                    Leg</a></li>--}}
                                                {{--                                                                        </ul>--}}
                                                {{--                                                                    </div>--}}
                                                {{--                                                                </li>--}}
                                                {{--                                                                <li>--}}
                                                {{--                                                                    <a href="../women/innerwear-tops.html">Innerwear--}}
                                                {{--                                                                        Tops</a>--}}
                                                {{--                                                                    <div class="subMenuDiv" id="menInnerwearTopsMenu">--}}
                                                {{--                                                                        <div class="mobileMenuHeading"--}}
                                                {{--                                                                             onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                {{--                                                                            Innerwear Tops--}}
                                                {{--                                                                        </div>--}}
                                                {{--                                                                        <div class="clearfix"></div>--}}
                                                {{--                                                                        <ul class="subMenuList">--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/innerwear-tops/crop-tops.html">Crop--}}
                                                {{--                                                                                    Tops</a></li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/innerwear-tops/camisoles.html">Camisoles</a>--}}
                                                {{--                                                                            </li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/innerwear-tops/kurta-slips.html">Kurta--}}
                                                {{--                                                                                    Slips</a></li>--}}
                                                {{--                                                                            <li>--}}
                                                {{--                                                                                <a href="../women/innerwear-tops/kurti-slips.html">Kurti--}}
                                                {{--                                                                                    Slips</a></li>--}}
                                                {{--                                                                        </ul>--}}
                                                {{--                                                                    </div>--}}
                                                {{--                                                                </li>--}}
                                                {{--                                                            </ul>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </div>--}}
                                                <div class="col-12 menuMiddleInnerRight">
                                                    <div class="row">
                                                        <div class="col-6 padding-0">
                                                            <ul class="menuList fullWidth">
                                                                <li>
                                                                    {{--                                                                    <a href="../women/apparel-tops.html">Apparel--}}
                                                                    {{--                                                                        Tops</a>--}}
                                                                    <div class="subMenuDiv" id="womenApparelTopsMenu">
                                                                        {{--                                                                        <div class="mobileMenuHeading"--}}
                                                                        {{--                                                                             onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                                        {{--                                                                            Apparel Tops--}}
                                                                        {{--                                                                        </div>--}}
                                                                        <div class="clearfix"></div>
                                                                        <ul class="subMenuList">
                                                                            @foreach($woMenCategory as $womenCategory)
                                                                                <li><a href="{{ url('women?category='.$womenCategory->name) }}">{{ $womenCategory->name }}</a></li>
                                                                            @endforeach
                                                                            {{--                                                                                <li><a href="../women/new-arrivals.html">T-shirt</a>--}}
                                                                            {{--                                                                                </li>--}}
                                                                            {{--                                                                                <li><a href="../women/multi-packs.html">Track pants</a></li>--}}
                                                                            {{--                                                                                <li><a href="../women/thermals.html">Thermals</a></li>--}}
                                                                            {{--                                                                                <li><a href="../women/shapewear.html">Shorts </a></li>--}}
                                                                            {{--                                                                                <li><a href="../women/socks.html">Sport bra--}}
                                                                            {{--                                                                                    </a></li>--}}
                                                                            {{--                                                                                <li><a href="../women/towel.html">Running bra--}}
                                                                            {{--                                                                                    </a></li>--}}
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                {{--                                                                <li>--}}
                                                                {{--                                                                    <a href="../women/tops-for-teens.html">Tops--}}
                                                                {{--                                                                        for teens</a>--}}
                                                                {{--                                                                    <div class="subMenuDiv" id="womenTopsforTeenMenu">--}}
                                                                {{--                                                                        <div class="mobileMenuHeading"--}}
                                                                {{--                                                                             onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                                {{--                                                                            Tops for teens--}}
                                                                {{--                                                                        </div>--}}
                                                                {{--                                                                        <div class="clearfix"></div>--}}
                                                                {{--                                                                        <ul class="subMenuList">--}}
                                                                {{--                                                                            <li>--}}
                                                                {{--                                                                                <a href="../women/tops-for-teens/beginners-bras.html">Beginners--}}
                                                                {{--                                                                                    Bras</a></li>--}}
                                                                {{--                                                                            <li>--}}
                                                                {{--                                                                                <a href="../women/tops-for-teens/uniform-bras.html">Uniform--}}
                                                                {{--                                                                                    Bras</a></li>--}}
                                                                {{--                                                                            <li>--}}
                                                                {{--                                                                                <a href="../women/tops-for-teens/crop-tops.html">Crop--}}
                                                                {{--                                                                                    Tops</a></li>--}}
                                                                {{--                                                                            <li>--}}
                                                                {{--                                                                                <a href="../women/tops-for-teens/camisoles.html">Camisoles</a>--}}
                                                                {{--                                                                            </li>--}}
                                                                {{--                                                                        </ul>--}}
                                                                {{--                                                                    </div>--}}
                                                                {{--                                                                </li>--}}
                                                            </ul>
                                                        </div>
                                                        <div class="col-6 padding-0">
                                                            <ul class="menuList fullWidth padding-0">
                                                                <li>
                                                                    {{--                                                                    <a href="../women/apparel-bottoms.html">Apparel--}}
                                                                    {{--                                                                        Bottoms</a>--}}
                                                                    <div class="subMenuDiv" id="womenApparelBottomMenu">
                                                                        {{--                                                                        <div class="mobileMenuHeading"--}}
                                                                        {{--                                                                             onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                                                        {{--                                                                            Apparel Bottoms--}}
                                                                        {{--                                                                        </div>--}}
                                                                        <div class="clearfix"></div>
                                                                        <ul class="subMenuList">
                                                                            {{--                                                                                <li>--}}
                                                                            {{--                                                                                    <a href="../women/apparel-bottoms/shorts.html">Maternity bra--}}
                                                                            {{--                                                                                    </a>--}}
                                                                            {{--                                                                                </li>--}}
                                                                            {{--                                                                                <li>--}}
                                                                            {{--                                                                                    <a href="../women/apparel-bottoms/capris.html">Panties</a>--}}
                                                                            {{--                                                                                </li>--}}
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- WoMen Mobile Menu --}}
                                        <div class="col-3 menuLeft showInMobile">
                                            <ul class="menuList float-right">
                                                @foreach($woMenCategory as $womenCategory)
                                                    <li><a href="{{ url('women?category='.$womenCategory->name) }}">{{ $womenCategory->name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-3 menuRight">
                                            {{--                                            <ul class="menuList">--}}
                                            {{--                                                <li>--}}
                                            {{--                                                    <a onclick="fncMegaMenuSecondLevel('womenShopbyCollection', this)">Shop--}}
                                            {{--                                                        By Collection</a>--}}
                                            {{--                                                    <div class="subMenuDiv" id="womenShopbyCollection">--}}
                                            {{--                                                        <div class="mobileMenuHeading"--}}
                                            {{--                                                             onclick="fncMegaBack('subMenuDiv', 'subMenuDivActive')">--}}
                                            {{--                                                            Shop By Collection--}}
                                            {{--                                                        </div>--}}
                                            {{--                                                        <div class="clearfix"></div>--}}
                                            {{--                                                        <ul class="subMenuList">--}}
                                            {{--                                                            <li><a href="../women/collection/usa-originals.html">USA--}}
                                            {{--                                                                    Originals</a></li>--}}
                                            {{--                                                            <li>--}}
                                            {{--                                                                <a href="../women/collection/international-collection.html">International--}}
                                            {{--                                                                    Collection</a></li>--}}
                                            {{--                                                            <li><a href="../women/collection/move.html">Move--}}
                                            {{--                                                                </a></li>--}}
                                            {{--                                                            <li><a href="../women/collection/soft-wonder.html">Soft--}}
                                            {{--                                                                    Wonder</a></li>--}}
                                            {{--                                                            <li>--}}
                                            {{--                                                                <a href="../women/collection/athleisure.html">Athleisure</a>--}}
                                            {{--                                                            </li>--}}
                                            {{--                                                            <li><a href="../women/collection/relax.html">Relax</a>--}}
                                            {{--                                                            </li>--}}
                                            {{--                                                            <li><a href="../women/collection/seamless-shapewear.html">Shapewear</a>--}}
                                            {{--                                                            </li>--}}
                                            {{--                                                            <li><a href="../women/collection/active.html">Active--}}
                                            {{--                                                                    Athleisure</a></li>--}}
                                            {{--                                                            <li><a href="../women/collection/fashion-essentials.html">Fashion--}}
                                            {{--                                                                    Essentials</a></li>--}}
                                            {{--                                                            <li><a href="../women/collection/essence.html">Essence</a>--}}
                                            {{--                                                            </li>--}}
                                            {{--                                                            <li><a href="../women/collection/comfies.html">Comfies</a>--}}
                                            {{--                                                            </li>--}}
                                            {{--                                                            <li><a href="../women/collection/simple-comfort.html">Simple--}}
                                            {{--                                                                    Comfort</a></li>--}}
                                            {{--                                                            <li><a href="../women/collection/miss-jockey.html">Miss--}}
                                            {{--                                                                    Jockey </a></li>--}}
                                            {{--                                                        </ul>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                </li>--}}
                                            {{--                                            </ul>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li  class="nav-item  ng-star-inserted">
                    <a  class="nav-link  ng-star-inserted" href="javascript:void(0)"
                              onclick="fncMegaMenu(event)">ACCESSORIES</a>
                                        <div  class="megamenuDiv-block" appnavigation="" id="jockeyHeaderMenu3">
                                            <div class="col-12 megamenuDiv-innerblock">
                                                <div class="mobileMenuHeading"
                                                     onclick="fncMegaBack('megamenuDiv-block', 'megamenuDivAcive')">Accessories
                                                </div>
                                                <div class="megamenu-inner-content">
                                                    <div class="col-12 menuBlock">
                                                        <div class="row">
                                                            <div class="col-4 menuLeft hideInMobile">

                                                            </div>
                                                            <div class="col-4 menuMiddle" style="border-right:0;">
                                                                <div class="row">
                                                                    <div class="col-4 menuMiddleInnerLeft border-0">
                                                                        <ul class="menuList fullWidth">
                                                                            <li>
                                                                                <div class="subMenuDiv" id="accMenSocksMenu">
                                                                                    <div class="clearfix"></div>
                                                                                    <ul class="subMenuList">
                                                                                        @foreach($accessories as $accessory)
                                                                                                <li>
                                                                                                    <a href="{{ url('accessories?category='.$accessory->name) }}">{{ $accessory->name }}</a>
                                                                                                </li>
                                                                                        @endforeach
                                                                                    </ul>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-4 menuMiddleInnerRight">
                                                                        <ul class="menuList fullWidth">
                                                                            <li>
                                                                                <div class="subMenuDiv" id="accMenSocksMenu">
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <ul class="menuList fullWidth">
                                                                            <li>
                                                                                <div class="subMenuDiv" id="accWomenSocksMenu">
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-4 menuMiddleInnerRight">
                                                                        <ul class="menuList fullWidth">
                                                                            <li>
                                                                                <div class="subMenuDiv" id="accTowelswoMen">
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-3 menuLeft showInMobile">
                                                                <ul class="menuList float-right">
                                                                    @foreach($accessories as $accessory)
                                                                        <li>
                                                                             <a href="{{ url('accessories?category='.$accessory->name) }}">{{ $accessory->name }}</a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                </li>
                {{--                <li  class="nav-item  ng-star-inserted">--}}
                {{--                    <a  class="nav-link  ng-star-inserted"--}}
                {{--                              routerlinkactive="router-link-active" href="../blog.html">Blog</a>--}}
                {{--                    <div  class="megamenuDiv-block" appnavigation="" id="jockeyHeaderMenu4">--}}
                {{--                        <div style="display: none"></div>--}}
                {{--                    </div>--}}
                {{--                </li>--}}
            </ul>
            <ul  class="mobileHeaderLinks">
                <li  class="">
                    @if(Auth::check())
                        <a  class="" href="{{ route('mySetting') }}" routerlinkactive="router-link-active">
                            <i class="iconHeaderMa"></i>My Accounts
                        </a>
                    @endif
                </li>
{{--                <li  class="">--}}
{{--                    <a  class="" href="#" routerlinkactive="router-link-active">--}}
{{--                        <i class="iconHeaderTO"></i>Track your order--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li  class="">--}}
{{--                    <a  class="" href="#" routerlinkactive="router-link-active">--}}
{{--                        <i class="iconHeaderBl"></i>Blogs--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li  class="">--}}
{{--                    <a  class="" href="#" routerlinkactive="router-link-active">--}}
{{--                        <i class="iconHeaderHelp"></i>Need Help?--}}
{{--                    </a>--}}
{{--                </li>--}}
            </ul>
        </nav>
    </div>
    <div  class="headerSearch">
        <div  class="headerSearchInnerBlock">
            <div  class="container">
                <a  class="searchClose" href="javascript:void(0)">Close</a>
                <span class="spnLooking">What are you looking for?</span>
                <div  class="headerSearchInner">
                    <input class="searchInput ng-untouched ng-pristine ng-valid" placeholder="Search products" type="text" value="">
                    <a class="headerSearchBtn" href="javascript:void(0);" title="Search">
                        <span class="kz-search-space"></span>
                    </a>
                </div>
                <div  class="clearfix"></div>
            </div>
        </div>
    </div>
</header>

{{--Login Form--}}
<div class="modal fade" id="showLoginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form autocomplete="off"  action="{{ route('login') }}" method="post" novalidate="" class="">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12 loginFormInner" id="divlogin">
                        <div class="col-12 loginForm text-left">
                            <div class="inputBlock">
                                <label for="">Registered email address*</label>
                                <input id="email" type="email" name="email" required autocomplete="off" placeholder="Enter Email" class="form-control @error('password') is-invalid @enderror">

                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
{{--                                <input type="email" name="email" required class="form-control form-input" formcontrolname="email" onblur="fncOnblur(event,'inputBlock')" onfocus="fncFocus(event,'inputBlock')" >--}}
                            </div>
                            <div class="inputBlock">
                                <label for="">Password*</label>
                                <input id="password" type="password" name="password" required autocomplete="off" placeholder="Enter Password" class="form-control @error('password') is-invalid @enderror">

                                @error('password')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
{{--                                <a class="recoverpassword showpassword" href="javascript:void(0);"> Show</a>--}}
                            </div>
                            <div class="InputBlok">
                                <div class="divRememberMe">
                                    <div class="checkbox" style="display: none;">
                                        <input id="rememberMe" type="checkbox" value="lsRememberMe">
                                        <label for="rememberMe">Remember me</label>
                                    </div>
                                </div>
{{--                                <a class="recoverpassword" href="javascript:void(0);">Forgot Password?</a>--}}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-12 loginBtns d-flex">
{{--                            <div class="col-6">--}}
{{--                                <button type="button" class="btn btnLogin" data-dismiss="modal">Close</button>--}}
{{--                            </div>--}}
                                {{ Form::button('Sign In', ['type'=>'submit','class' => 'btn btnLogin','id'=>'btnLogin','data-loading-text'=>"<span class='spinner-border spinner-border-sm mr-2'></span> Processing..."]) }}
                        </div>
                        <div class="clearfix"></div>
{{--                        <div class="col-12 orBlock">--}}
{{--                            <hr><span>Or</span>--}}
{{--                        </div>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                        <div class="col-12 loginSocailBlock">--}}
{{--                            <div class="row ng-star-inserted">--}}
{{--                                <div class="col-6">--}}
{{--                                    <a class="loginFB" href="https://www.facebook.com">--}}
{{--                                        <div class="spnTitle">--}}
{{--                                            <span class="kz-facebook"></span> Login with Facebook--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                                <div class="col-6"><a class="loginGP" href="https://accounts.google.com">--}}
{{--                                        <div class="spnTitle">--}}
{{--                                            <span class="kz-google-plus"></span> Login with Google--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
{{--End Login Form--}}

{{--Singup Modal --}}
<div class="modal fade" id="showSignUpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form autocomplete="off" name="singUpForm"  id="singUpForm" method="POST" novalidate="">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12 loginFormInner" id="divlogin">
                        <div class="col-12 loginForm text-left">
                            <div class="inputBlock">
                                <label for="">Full Name*</label>
                                <input id="name" type="text" name="name" required autocomplete="off" placeholder="Enter Full Name" class="form-control @error('name') is-invalid @enderror">

                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

{{--                                <input type="text" required autocomplete="off" class="form-control ng-pristine ng-invalid  ng-touched" formcontrolname="FirstName" maxlength="128" name="name" onblur="fncOnblur(event,'inputBlock')" onfocus="fncFocus(event,'inputBlock')" title="Name" >--}}
                            </div>
                            <div  class="inputBlock">
                                <label  class="">Email address*</label>
                                <input id="email" type="email" name="email" required autocomplete="off" placeholder="Enter Email" class="form-control @error('email') is-invalid @enderror">

                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
{{--                                <input required autocomplete="off" class="form-control ng-pristine ng-invalid  ng-touched" formcontrolname="email" name="email" onblur="fncOnblur(event,'inputBlock')" onfocus="fncFocus(event,'inputBlock')" title="Email address" type="email"><!---->--}}
                            </div>
                            <div  class="inputBlock">
                                <label>Password*</label>
                                <input id="password" type="password" name="password" required autocomplete="off" placeholder="Enter Password" class="form-control @error('password') is-invalid @enderror">

                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
{{--                                <input  autocomplete="off" readonly="" style="display:none" type="password">--}}
{{--                                <input required autocomplete="off" class="form-control form-input " formcontrolname="password" name="password" onblur="fncOnblur(event,'inputBlock')" onfocus="fncFocus(event,'inputBlock')" title="Password" type="password"><!---->--}}
{{--                                <a  class="recoverpassword showpassword" href="javascript:void(0);">Show</a>--}}
                            </div>
                            <div  class="inputBlock">
                                <label  class="">Repeat Password*</label>
                                <input type="password" name="password_confirmation" required autocomplete="off" placeholder="Enter Repeat Password" class="form-control @error('password_confirmation') is-invalid @enderror">

                                @error('password_confirmation')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
{{--                                <input  autocomplete="off" readonly="" style="display:none" type="password">--}}
{{--                                <input required autocomplete="off" class="form-control form-input " formcontrolname="confirmPassword" name="c" onblur="fncOnblur(event,'inputBlock')" onfocus="fncFocus(event,'inputBlock')" required="" title="Repeat Password" type="c"><!---->--}}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btnLogin" data-dismiss="modal">Close</button>
                    {{ Form::button('Create An Account', ['type'=>'submit','class' => 'btn btnLogin','id'=>'btnSingUp','data-loading-text'=>"<span class='spinner-border spinner-border-sm mr-2'></span> Processing..."]) }}
                </div>
            </div>
        </form>
    </div>
</div>
{{--End Singup Modal --}}




{{-- Add to Cart--}}
<div class="modal fade" id="showCartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="cartGlobalblock mCustomScrollbar _mCS_1 mCS-autoHide ng-star-inserted" malihu-scrollbar="" style="overflow: visible;">
        <div id="mCSB_1" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" style="max-height: none;" tabindex="0">
            <div id="mCSB_1_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
                <div class="cartGlobalblockInner">
                    <div class="col-12 homeCartItemsTitle">
                        <a class="cartGlobalClose" data-dismiss="modal" href="javascript:void(0);" onclick="location.reload();"></a>
                        <div class="col-12 homeCartItemsTitleInner">
                            <h2>Your Shopping Bag</h2>
                            <span class="globalItemsCountSpn ng-star-inserted">{{ count($addToCarts) }} Item(s)</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-12 homeCartItemsBlock" style="padding-left: 15px;padding-right: 15px;">
                        @if(count($addToCarts) > 0 && Auth::check())
                            @foreach($addToCarts as $addToCart)
                                <div class="col-12 homeCartItem ng-star-inserted">
                                    <div class="col-12 homeCartItemInner">
                                        <a class="homePro ng-star-inserted" href="{{ route('men.details',$addToCart->product->id) }}" title="{{ $addToCart->product->name }}">
                                            <span>
                                                <img src="{{ $addToCart->product->image }}" alt="{{ $addToCart->product->name }}">
                                            </span>
                                        </a>
                                        <div class="col-xs-12 homeCartProDetails">
                                            <a class="removeItemGlobal ng-star-inserted removeProductToBag"  data-id="{{ $addToCart->id }}" href="javascript:void(0)"></a>
                                            <h4 title="{{ $addToCart->product->name }}">{{ $addToCart->product->name }}</h4>
                                            <span class="spnPriceGlobal ng-star-inserted"> {{ $addToCart->product_qty }} X <b>₹</b>  {{ $addToCart->product->price }}.00 </span>
                                            <div class="clearfix"></div>
                                            <div class="lblSizeSmGlobal ng-star-inserted">Size :
                                                <div class="globalCartSizeSelctionDiv" onclick="fncCartSize(event)">
                                                    <div class="">{{ $addToCart->product_size }}</div>
                                                </div>
                                            </div>
                                            <div class="lblSizeSmGlobal ng-star-inserted"> Qty :
                                                <div class="globalCartSizeSelctionDiv cartQtyDropDown"
                                                     onclick="fncCartSize(event)">
                                                    <div class="">{{ $addToCart->product_qty }}</div>
                                                </div>
                                                <span></span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-12 removeProduct align-items-center" id="wishListPopup">
                                        <div class="divRemoveItemtext">
                                            <span>Added to wishlist</span>
                                            <a href="javascript:void(0)">Undo</a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-12 removeProduct align-items-center" id="removePopup">
                                        <div class="divRemoveItemtext">
                                            <span>Item Removed</span>
                                            <a href="javascript:void(0)">Undo</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                            <div class="col-12 cartGlobalTotal">
                                <div class="row">
                                    <div class="col-12 cartGlobalTotalInner">
                                        <div class="col-12 cart-order-summary-area">
                                            <div class="col-12 cart-order-summary-line-items">
                                                <h6>Bag Total</h6>
                                                <div class="col-12 cart-grid-block">
                                                    <div class="col-12 cart-grid-row">
                                                        <div class="row">
                                                            <div class="col-7 cart-grid-cell">Sub - Total</div>
                                                            <div class="col-5 cart-grid-cell text-right ng-star-inserted">
                                                                <span>₹</span>
                                                                <?php $price = 0 ?>
                                                                <?php $charge = 0 ?>
                                                                @foreach($addToCarts as $addToCart)
                                                                    <?php
                                                                        $price += ($addToCart->product_qty*$addToCart->product->price);
                                                                        $charge = ($price*5/100);
                                                                    ?>
                                                                @endforeach
                                                                {{ $price }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 cart-grid-row">
                                                        <div class="row">
                                                            <div class="col-7 cart-grid-cell">Shipping Charges</div>
                                                            <div class="col-5 cart-grid-cell text-right ng-star-inserted">
                                                                <span>₹</span>
                                                                {{ $charge }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 cart-grid-row cart-total-price">
                                                        <div class="row">
                                                            <div class="col-5 cart-grid-cell">Total Amount</div>
                                                            <div class="col-7 cart-grid-cell text-right ng-star-inserted">
                                                                <b>₹</b>
                                                                @php
                                                                    $grandTotal = ($price + $charge );
                                                                @endphp
                                                                {{ $grandTotal }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- place order--}}
                                        <div class="col-12 cartButtonGlobal ng-star-inserted">
                                            <a class="btnCheckoutGlobal" onclick="event.preventDefault();  document.getElementById('placeOrder-form').submit();"
                                               href="{{ url('placeOrder') }}">
                                                <span>Place Order</span>
                                            </a>
                                            <form id="placeOrder-form" action="{{ url('/placeOrder') }}" method="POST" class="d-none">
                                                {{ csrf_field() }}
                                            </form>
                                        </div>
                                        {{-- end place order--}}
                                    </div>
                                </div>
                            </div>
                        @else
                            {{-- Cart Empty Start --}}
                            <div class="col-12  ng-star-inserted">
                                <div class="col-12 homeCartItemInner">
                                    <div class="">
                                        <img alt="Cart Empty" src="{{ asset('theme/images/cart-empty.png')  }}">
                                        <div class="globalCartEmptyLabel" style="text-align: center;"> YOUR BAG IS EMPTY</div>
                                        <a class="btnCheckoutGlobal router-link-active" href="/" routerlinkactive="router-link-active">
                                            <span>Start Shopping</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            {{--Cart Empty End--}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- End Add to Cart--}}
