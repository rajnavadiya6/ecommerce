@php
    $setting = \App\Models\Setting::pluck('value','key')->toArray();
    $frontSetting = \App\Models\FrontSetting::pluck('value','key')->toArray();
@endphp
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> Home | {{config('app.name')}}</title>
    <link rel="shortcut icon" href="{{ $setting['favicon'] }}" type="image/x-icon">
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link href="{{ asset('theme/css/web_theme.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/pagination.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/css/hover-min.css') }}"/>
</head>
<body>
@include('by_pass.layout.header')</ngx-json-ld>
</app-home>
<div class="clearfix"></div>
<div class="scrollup" onclick="scrollUP()">
    <span class="kz-up-top"></span>
</div>
</app-root>
<div id="hometemplate">
    <div class="col-12 homeBannerBlock">
        <div class="col-12 homeBannerInner">

            <div id="homeSlider" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#homeSlider" data-slide-to="0" class=""></li>
                    <li data-target="#homeSlider" data-slide-to="1" class="active"></li>
                    <li data-target="#homeSlider" data-slide-to="2" class=""></li>
                    <li data-target="#homeSlider" data-slide-to="3" class=""></li>

                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    @foreach($heroImage as $img)
                        <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                            <a href="#">
                                <picture>
                                    <source srcset="{{ $img->name }}" type="image/webp">
                                    <source srcset="{{ $img->name }}" type="image/jpeg">
                                    <img src="{{ $img->name }}" alt="Jockey Banners">
                                </picture>
                            </a>
                        </div>
                    @endforeach
                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev sliderArrow" href="#homeSlider" data-slide="prev">
                    <span class="kz-angle-left"></span>
                </a>
                <a class="carousel-control-next sliderArrow" href="#homeSlider" data-slide="next">
                    <span class="kz-angle-right"></span>
                </a>

            </div>

        </div>
    </div>
    <!-- Banner Ends -->
    <div class="col-12 padding-0">
        <!-- Three block -->
        <div class="clearfix"></div>
        <div class="col-12 homeThreeBlocks">
            <div class="container homeContainer">
                <div class="row justify-content-between d-flex mt-5">
                    <div class="category-title-mobile">
                        <h2 class="category-title">Popular Men Categories</h2>
                    </div>
                    <div class="menCategory" style="margin-bottom: .5rem;">
                        <a class="btn" href="{{ route('new_arrivals') }}">View All</a>
                    </div>
                </div>
                <div class="men-container_slider cursor-pointer">
                    @if(count($mens) > 0)
                        @foreach($mens as $men)
                            <div class="men-single-slider hvr-wobble-horizontal single-brand-logo mx-2 my-6 d-inline" style="outline: none !important;">
                                <a href="{{ route('men.details',$men->id) }}" style="outline: none !important;">
                                <img src="{{ $men->image }}" alt="brands_logo" style="object-fit: cover;width: 230px;height: 350px;"/>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="w-100 text-center mt-1">
                            <h5 class="category-no-found">Men Categories Not Available</h5>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- Three block Ends -->
        <!-- Video -->
        <div class="clearfix"></div>
        <div class="col-12 homeThreeBlocks">
            <div class="container homeContainer">
                <div class="row justify-content-between d-flex mt-5">
                    <div class="category-title-mobile">
                        <h2 class="category-title">Popular Women Categories</h2>
                    </div>
                    <div class="menCategory" style="margin-bottom: .5rem;">
                        <a class="btn" href="{{ route('women_arrivals') }}">View All</a>
                    </div>
                </div>
                <div class="women-container_slider cursor-pointer">
                    @if(count($womens) > 0)
                        @foreach($womens as $women)
                            <div class="women-single-slider hvr-wobble-horizontal single-brand-logo mx-2 my-6 d-inline" style="outline: none !important;">
                                <a href="{{ route('men.details',$women->id) }}" style="outline: none !important;">
                                    <img src="{{ $women->image }}" alt="brands_logo" style="object-fit: cover;width: 230px;height: 350px;"/>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="w-100 text-center mt-5">
                            <h5 class="category-no-found">Women Categories Not Available</h5>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="col-12 homeThreeBlocks">
            <div class="container homeContainer">
                <div class="row justify-content-between d-flex mt-5">
                    <div class="category-title-mobile">
                        <h2 class="category-title">Popular Accessories Categories</h2>
                    </div>
                    <div class="menCategory" style="margin-bottom: .5rem;">
                        <a class="btn" href="{{ route('accessories_arrivals') }}">View All</a>
                    </div>
                </div>
                <div class="accessories-container_slider cursor-pointer">
                    @if(count($accessories) > 0)
                        @foreach($accessories as $accessory)
                            <div class="accessories-single-slider hvr-wobble-horizontal single-brand-logo mx-2 my-6 d-inline" style="outline: none !important;">
                                <a href="{{ route('men.details',$accessory->id) }}" style="outline: none !important;">
                                    <img src="{{ $accessory->image }}" alt="brands_logo" style="object-fit: cover;width: 230px;height: 350px;"/>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="w-100 text-center mt-5">
                            <h5 class="category-no-found">Accessories Categories Not Available</h5>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- Video Ends -->
        <!-- Best Sellers -->
        <div class="clearfix"></div>
        <!-- Best Sellers Ends -->
        <!-- SEO content -->
    {{--        <div class="col-12 jockeyHomeFooterContent">--}}
    {{--            <div class="container">--}}
    {{--                <h1>{{ $frontSetting['title_1'] }}</h1>--}}
    {{--                <p>{!! $frontSetting['title_1_description'] !!}</p>--}}
    {{--                <h3>{{ $frontSetting['title_2'] }}</h3>--}}
    {{--                <p>{!! $frontSetting['title_2_description'] !!}</p>--}}
    {{--                <h3>{{ $frontSetting['title_3'] }}</h3>--}}
    {{--                <p>{!! $frontSetting['title_3_description'] !!}</p>--}}
    {{--                <h3>{{ $frontSetting['title_4'] }}</h3>--}}
    {{--                <p>{!! $frontSetting['title_4_description'] !!}</p>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    <!-- SEO content ends -->
    </div>
</div>
@include('by_pass.layout.footer')
<div class="scrollup" onclick="scrollUP()"><span class="kz-up-top"></span></div>
@routes
<script src="{{ asset('theme/js/main.js') }}"></script>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script src="{{ asset('assets/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ mix('assets/js/custom/custom.js') }}"></script>
<script>
    let registerURL = '{{ route('register') }}';
    let removeProductToBag = '{{ route('removeProductToBag') }}';
</script>
<script src="{{ mix('assets/js/profile.js') }}"></script>
<script>
    function fncHomeVideoPlay (videoURL) {
        var videoid = document.getElementById('homevideo');
        if (videoid.paused) {
            $(videoid).html('<source src="' + videoURL + '" type="video/mp4"></source>');
            setTimeout(function () {
                videoid.play();
                $('.iconHomePlay').addClass('iconHomePause');
                $('.videoPosterDiv').css('visibility', 'hidden');
                $('#homevideo').parent().addClass('homeVideoPlaying');
            }, 1000);
        } else {
            videoid.pause();
            $('.iconHomePlay').removeClass('iconHomePause');
            $('.videoPosterDiv').css('visibility', 'show');
            $('#homevideo').parent().removeClass('homeVideoPlaying');
        }
    }

    // Footer
    $('.footerData').click(function () {
        $(this).toggleClass('footerheadActive');
        $(this).siblings('.footerLinkDiv').toggleClass('displayData');
    });

    $('.men-container_slider').slick({
        dots: false,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        // prevArrow: '<span class="prev-slick-arrow slick-arrow"></span>',
        // nextArrow: '<span class="next-slick-arrow slick-arrow"></span>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });

    $('.women-container_slider').slick({
        dots: false,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        // prevArrow: '<span class="prev-slick-arrow slick-arrow"><i class="fas fa-arrow-left"></i></span>',
        // nextArrow: '<span class="next-slick-arrow slick-arrow"><i class="fas fa-arrow-right"></i></span>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });

    $('.accessories-container_slider').slick({
        dots: false,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        // prevArrow: '<span class="prev-slick-arrow slick-arrow"><span class="fas fa-arrow-left"></span></span>',
        // nextArrow: '<span class="next-slick-arrow slick-arrow"><span class="fas fa-arrow-right"></span></span>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });
</script>
<style>
    .displayData {
        display: block !important;
    }

    .brand-logo {

    @media screen and (min-width: 768px) {
        width:

    180px

    ;
        text-align: center

    ;
    }

    padding:

    5
    px

    0
    ;
    }

    /* WoMen sldier */
    .women-container_slider {

    .women-slick-slide {
        outline: none;
        height: auto;
        text-align: center;

    img {
        display: inline-block;
    }

    }
    }

    /*.slick-prev.slick-arrow{*/
    /*    width: 22px;*/
    /*    height: 22px;*/
    /*    bottom: auto;*/
    /*    top: 50%;*/
    /*    border-radius: 100%;*/
    /*    transform: translate(-50%, -50%);*/
    /*    -webkit-transform: translate(-50%, -50%);*/
    /*}*/
    /*.slick-prev {*/
    /*    left: -18px;*/
    /*}*/
    /*.slick-next{*/
    /*    left: auto;*/
    /*    right: -49px !important;*/
    /*}*/
    /*.slick-next.slick-arrow{*/
    /*    width: 22px;*/
    /*    height: 22px;*/
    /*    bottom: auto;*/
    /*    top: 50%;*/
    /*    border-radius: 100%;*/
    /*    transform: translate(-50%, -50%);*/
    /*    -webkit-transform: translate(-50%, -50%);*/
    /*}*/
    /*.slick-next {*/
    /*    right: 0;*/
    /*}*/
    /*.slick-next, .slick-prev {*/
    /*    position: absolute;*/
    /*    top: 0;*/
    /*    bottom: 0;*/
    /*    display: flex;*/
    /*    align-items: center;*/
    /*    justify-content: center;*/
    /*    width: 15%;*/
    /*    color: #fff;*/
    /*    text-align: center;*/
    /*    opacity: .5;*/
    /*}*/
    /*.prev-slick-arrow:before{*/
    /*    content: "<" !important;*/
    /*    color:red;*/
    /*    font-size: 30px;*/
    /*}*/
    /*.next-slick-arrow:before{*/
    /*    content: ">" !important;*/
    /*    color:red;*/
    /*    font-size: 30px;*/
    /*}*/


    /* Men sldier */
    .men-container_slider {

    .men-slick-slide {
        outline: none;
        height: auto;
        text-align: center;

    img {
        display: inline-block;
    }

    }
    }

    /* Accessories sldier */
    .accessories-container_slider {

    .accessories-slick-slide {
        outline: none;
        height: auto;
        text-align: center;

    img {
        display: inline-block;
    }

    }
    }


    /*Browse Button*/
    .menCategory a {
        right: 0;
        top: 0;
        background: #000;
        color: #fff;
        padding: 9px 20px;
        display: block;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Jost*';
        font-weight: 700;
        font-size: 10px;
        line-height: 16px;
    }

    /* Login Form*/

    .btnLoginTab {
        float: left;
        width: 50%;
        padding: 20px;
        font-family: 'Jost* 500';
        font-size: 12px;
        color: #a6a6a6;
        text-align: center;
        text-transform: uppercase;
        border-bottom: 2px solid #e5e5e5;
        transition: border-color .6s linear;
        -webkit-transition: border-color .6s linear;
    }

    .btnLoginTabSelected {
        color: #000;
        border-bottom-color: #000;
    }

    /* ENd Login Form*/
</style>
</body>
</html>
