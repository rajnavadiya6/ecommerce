@php
    $setting = \App\Models\Setting::pluck('value','key')->toArray();
@endphp
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> Profile | {{config('app.name')}}</title>
    <link rel="shortcut icon" href="{{ $setting['favicon'] }}" type="image/x-icon">
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">
    <link href="{{ asset('theme/css/web_theme.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/pagination.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/hover-min.css') }}" />
</head>
<body>
@include('by_pass.layout.header')

<app-root _nghost-sc0="" ng-version="8.2.14">
    <ng-progress>
        <ng-progress-bar>
            <div  class="ng-progress ng-star-inserted">
                <div  class="bar"
                     style="transition: all 200ms linear 0s; background-color: rgb(255, 255, 255); margin-left: -100%;">
                    <div  class="bar-shadow"
                         style="box-shadow: rgb(255, 255, 255) 0px 0px 10px, rgb(255, 255, 255) 0px 0px 5px;">

                    </div>
                </div>
            </div>
        </ng-progress-bar>
    </ng-progress>
    <app-header class="ng-tns-c2-0 ng-star-inserted">
        <div  class="col-xs-12 padding-zero"></div>
        <div  class="popupblock popupFromBottom ng-tns-c2-0 ng-star-inserted"
             id="headerOfferPopup" style="display:none;">
            <div  class="popuptbl">
                <div  class="popuptr">
                    <div  class="popupinner">
                        <div  class="popupcontentDiv offerTemplatePopup"><a
                                 class="popupClose" href="javascript:void(0)"
                                onclick="closePopup('headerOfferPopup')"><span
                                                                               class="kz-close"></span></a>
                            <div  class="col-12 popupbody" appnavigation="">pdp image issue
                                fixing
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </app-header>
    <div  class="clearfix"></div>
    <app-myaccount  class="ng-star-inserted">
        <app-mysettings  class="ng-star-inserted">
            <div  class="col-12 bodyContent">
                <div  class="container myAccountContainer">
                    <div  class="clearfix"></div>
                    <div  class="col-12 myAccountBlock">
                        <div  class="col-12 myAccountHeader">
                            <h1 class="myAccountPageTitle">
                                Your Account
                            </h1>
                            <a href="{{ url('logout') }}" class="maSignoutBtn signout"
                               onclick="event.preventDefault(); localStorage.clear();  document.getElementById('logout-form').submit();">
                                Signout <span class="kz-angle-right"></span>
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" class="d-none">
                                {{ csrf_field() }}
                            </form>
                        </div>
                        <div  class="clearfix"></div>
                        <div  class="col-12 myAccountUserLoggedIn">
                            <h2>Hi,{{ Auth::user()->name }}.</h2>
                        </div>
                        <div  class="clearfix"></div>
                        <div  class="col-12 myAccountSublinkList">
                            <div  class="row">
                                <div  class="col-12">
                                    <div  class="maTablink"><span >Your Orders</span>
                                        <small>Track, cancel or return an order</small><a
                                             class="btnborderButton"
                                            href="">View Orders</a></div>
                                </div>
{{--                                <div  class="col-6">--}}
{{--                                    <div  class="maTablink"><span >Your Wishlist</span><small--}}
{{--                                            >View all your favourite items here and move them--}}
{{--                                            to your bag.</small><a  class="btnborderButton"--}}
{{--                                                                   href="/myaccount/mywishlist">View Wishlist</a></div>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                        <div  class="clearfix"></div>
                        <div
                             class="col-12 myAccountHeader myAccountPageMiddleHeader  hideInMobile"><h1
                                 class="myAccountPageTitle">Edit your details</h1></div>
                        <div  class="clearfix"></div><!---->
                        <div  class="col-12 profileBlock ng-star-inserted">
                            <div  class="row">
                                <div  class="col-4 profileBlockLeft">
                                    <div  class="maPanelHeading">Your Profile</div>
                                </div>
                                <div  class="col-8 profileBlockRight">
                                    <div  class="row">
                                        <div  class="profileDetailsInner">
                                            <table>
                                                <tr>
                                                    <td >Name:-</td>
                                                    <td>{{ Auth::user()->name .' '. Auth::user()->last_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td >Email Address:-</td>
                                                    <td>{{ Auth::user()->email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Mobile:-</td>
                                                    <td>{{ Auth::user()->phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td >DOB:-</td>
                                                    <td>{{ Auth::user()->dob }}</td>
                                                </tr>
{{--                                                <tr>--}}
{{--                                                    <td class="text-left" colspan="2">--}}
{{--                                                        <a class="maUpdateProfile"--}}
{{--                                                           href="javascript:void(0)">Edit Profile--}}
{{--                                                        </a>--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
                                            </table>
                                        </div>
{{--                                        <div  class="profileDetailsInner">--}}
{{--                                            <table>--}}
{{--                                                <tr>--}}
{{--                                                    <td>Password:-</td>--}}
{{--                                                    <td>******</td>--}}
{{--                                                </tr>--}}
{{--                                                <tr>--}}
{{--                                                    <td  class="text-left" colspan="2">--}}
{{--                                                        <a class="maUpdateProfile"--}}
{{--                                                            href="javascript:void(0)">Change Password--}}
{{--                                                        </a>--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                            </table>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div  class="clearfix"></div>
                        <div  class="col-12 profileBlock">
                            <div  class="row">
                                <div  class="col-4 profileBlockLeft">
                                    <div  class="maPanelHeading">Your Default Address</div>
                                </div>
                                <div  class="col-8 profileBlockRight">
                                    @if(isset(Auth::user()->address_1) or isset(Auth::user()->address_2) )
                                    <div  class="col-12 maDefaultAddress ng-star-inserted"
                                         id="divContact">
                                        @if(Auth::user()->choose_address == 1)
                                        <address>
                                            <i class="kz-pin"></i>{{ isset(Auth::user()->address_1) ? Auth::user()->address_1 : ''}}
                                            <br
                                            > {{ isset(Auth::user()->city) ? Auth::user()->city . '-' : '' }}
                                            {{ isset(Auth::user()->pincode) ? Auth::user()->pincode . ',' : ''}}
                                            <br
                                                > {{ isset(Auth::user()->state) ? Auth::user()->state : '' }}
                                        </address>
                                        @else
                                            <address>
                                               <i class="kz-pin"></i> {{ isset(Auth::user()->address_2) ? Auth::user()->address_2 : ''}}
                                                <br
                                                > {{ isset(Auth::user()->city) ? Auth::user()->city . '-' : '' }}
                                                {{ isset(Auth::user()->pincode) ? Auth::user()->pincode . ',' : ''}}
                                                <br
                                                > {{ isset(Auth::user()->state) ? Auth::user()->state : '' }}
                                            </address>
                                        @endif
{{--                                        <span  class="checkoutSpnMobile">7894561230</span>--}}
                                    </div>
                                    @endif
                                    <a class="maUpdateProfile" data-toggle="modal" data-target="#chooseAddressModel"  href="javascript:void(0)">View All Address</a>
                                    @if(Auth::user()->address_2 == null)
                                        <a class="maUpdateProfile" data-toggle="modal" data-target="#addressModel" href="javascript:void(0)">
                                            Add New Address
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </app-mysettings>
    </app-myaccount>
    <div  class="clearfix"></div>
    <div  class="scrollup" onclick="scrollUP()" style="bottom: -150px;">
        <span class="kz-up-top"></span></div>
</app-root>

{{--Address Form--}}
<!-- Modal -->
<div class="modal fade" id="addressModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form autocomplete="off" name="addAddressForm" id="addAddressForm" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Delivery Address</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="inputBlock">
                        <label class="formLabel">Date Of Birth*</label>
                        <input name="dob" type="date" required class="form-control form-input"  onblur="fncOnblur(event,'inputBlock')" onfocus="fncFocus(event,'inputBlock')" />
                    </div>
                    <div class="inputBlock">
                        <label class="formLabel">Full Address*</label>
                        <textarea name="address_2" required class="form-control form-input" cols="5" rows="5" onblur="fncOnblur(event,'inputBlock')" onfocus="fncFocus(event,'inputBlock')" >
                        </textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btnLogin" data-dismiss="modal">Close</button>
                    <button type="submit" id="btnAddress" class="btn btnLogin">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
{{--End Address Form--}}



{{--Choose Address--}}

<div class="modal fade" id="chooseAddressModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form autocomplete="off" name="chooseAddressForm" id="chooseAddressForm" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Choose Your Delivery Address</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6 addressCard">
                            <div class="inputBlock">
                                <input type="radio" name="choose_address" @php if(Auth::user()->choose_address == 1) { echo "checked ='checked'"; } @endphp class="form-control chooseRadioButton" value="1">
                                {{ isset(Auth::user()->address_1) ? Auth::user()->address_1 : 'N/A'}}
                                <br
                                > {{ isset(Auth::user()->city) ? Auth::user()->city . '-' : '' }}
                                {{ isset(Auth::user()->pincode) ? Auth::user()->pincode . ',' : ''}}
                                <br
                                > {{ isset(Auth::user()->state) ? Auth::user()->state : '' }}
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-6 addressCard">
                            <div class="inputBlock">
                                <input type="radio" name="choose_address" @php if(Auth::user()->choose_address == 2)  { echo "checked ='checked'"; } @endphp  class="form-control chooseRadioButton" value="2">
                                {{ isset(Auth::user()->address_2) ? Auth::user()->address_2 : 'N/A'}}
                                <br
                                > {{ isset(Auth::user()->city) ? Auth::user()->city . '-' : '' }}
                                {{ isset(Auth::user()->pincode) ? Auth::user()->pincode . ',' : ''}}
                                <br
                                > {{ isset(Auth::user()->state) ? Auth::user()->state : '' }}
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnAddress" class="btn btnLogin">Confirm Address</button>
                </div>
            </div>
        </form>
    </div>
</div>
{{--End choose address--}}


@include('by_pass.layout.footer')
<div class="scrollup" onclick="scrollUP()"><span class="kz-up-top"></span></div>
@routes

<script src="{{ asset('theme/js/main.js') }}"></script>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ mix('assets/js/custom/custom.js') }}"></script>
<script>
    let addressFormUrl = '{{ route('storeOrderNewAddress') }}';
    let chooseAddressFormUrl = '{{ route('chooseAddress') }}';
    let removeProductToBag = '{{ route('removeProductToBag') }}';

    // Footer
    $('.footerData').click(function (){
        $(this).toggleClass('footerheadActive');
        $(this).siblings('.footerLinkDiv').toggleClass('displayData');
    });

    /* Address Form*/
    $(document).on('submit', '#addAddressForm', function (event) {
        event.preventDefault();
        $.ajax({
            url: addressFormUrl,
            type: 'POST',
            data: new FormData($(this)[0]),
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    displaySuccessMessage(result.message);
                    $('#addressModel').empty();
                    location.reload();
                }
            },
            error: function (result) {
                displayErrorMessage(result.responseJSON.message);
            },
            complete: function () {
            },
        });
    });

    /* ChooseAddress Form*/
    $(document).on('submit', '#chooseAddressForm', function (event) {
        event.preventDefault();
        $.ajax({
            url: chooseAddressFormUrl,
            type: 'POST',
            data: new FormData($(this)[0]),
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    displaySuccessMessage(result.message);
                    $('#addressModel').empty();
                    location.reload();
                }
            },
            error: function (result) {
                displayErrorMessage(result.responseJSON.message);
            },
            complete: function () {
            },
        });
    });

    // ENd address Form
</script>
<style>
    .displayData{
        display:block !important;
    }

    .brand-logo {
    @media screen and (min-width: 768px) {
        width: 180px;
        text-align: center;
    }
    padding: 5px 0;
    }

    /* Login Form*/

    .btnLoginTab {
        float: left;
        width: 50%;
        padding: 20px;
        font-family: 'Jost* 500';
        font-size: 12px;
        color: #a6a6a6;
        text-align: center;
        text-transform: uppercase;
        border-bottom: 2px solid #e5e5e5;
        transition: border-color .6s linear;
        -webkit-transition: border-color .6s linear;
    }

    .btnLoginTabSelected {
        color: #000;
        border-bottom-color: #000;
    }
    /* ENd Login Form*/



    .addressCard{
        float: left;
        min-width: 200px;
        padding: 15px 25px;
        margin-bottom: 15px;
        border: 1px solid #000;
        font-family: 'Jost* 500';
        font-size: 12px;
        text-align: center;
        text-transform: uppercase;
        transition: all .3s;
    }

    /*This page*/
    .myAccountUserLoggedIn{
        padding: 23px 0 0;
        color: #000;
        font-family: 'Jost* 500';
        font-weight: 400;
    }
    .myAccountPageTitle{
        font-size: 24px;
        color: #000;
        font-family: 'Jost*';
        font-weight: 700;
        display: inline-block;
        vertical-align: top;
        margin: 0;
    }
    .maTablink {
        display: flex;
        padding: 30px 45px;
        border: 1px solid #909090;
        background-color: #fff;
        height: 100%;
        font-size: 20px;
        color: #000;
        font-family: 'Jost* 500';
        flex-wrap: wrap;
    }
     .btnborderButton {
         margin: 0 0 10px;
         display: inline-block;
         vertical-align: top;
         border: 2px solid #000;
         color: #151515;
         font-size: 13px;
         font-family: 'Jost* 500';
         padding: 11px 20px;
         height: 44px;
         transition: all .3s;
     }
    .myAccountPageMiddleHeader .myAccountPageTitle{
        font-weight: 400;
        font-family: 'Jost*';
    }
     .maPanelHeading{
         font-size: 20px;
         color: #000;
         font-family: 'Jost* 500';
     }
     .profileBlock .row{
         margin: 0;
     }
    .profileDetailsInner{
        display: flex;
        padding-right: 60px;
    }
    .profileDetailsInner table{
        font-size: 15px;
        color: #151515;
        font-family: 'Jost* 500';
    }
    table {
        border-collapse: collapse;
    }
    .profileDetailsInner table tr td {
        padding: 5px 10px;
    }
     .profileBlockLeft{
         padding: 0;
     }
    .maPersonName{
        display: inline-block;
        margin-bottom: 4px;
        text-transform: uppercase;
        color: #151515;
        padding: 0 0 0 22px;
        font-size: 13px;
    }
    .checkoutSpnMobile{
        padding: 3px 0 0 22px;
        position: relative;
    }
    .maUpdateProfile {
        float: none;
        margin: 0 10px 0 0;
        display: inline-block;
        vertical-align: top;
        border: 2px solid #000;
        color: #151515;
        font-size: 13px;
        font-family: 'Jost* 500';
        padding: 11px 20px;
        height: 44px;
        min-width: 150px;
        text-align: center;
        background: #fff;
    }
     .checkoutSpnMobile {
         padding: 3px 0 0 22px;
         position: relative;
     }
     .maDefaultAddress address {
         position: relative;
         line-height: 24px;
         font-size: 15px;
         color: #151515;
         font-family: 'Jost* 500';
     }
    .myAccountHeader{
        padding: 15px 0 10px;
        border-bottom: 1px solid rgba(0,0,0,.2);
    }
    .maTablink span{
        display: inline-block;
        width: 100%;
        vertical-align: top;
    }
    .maTablink small{
        width: 100%;
        font-family: 'Jost*';
        display: inline-block;
        font-size: 15px;
        vertical-align: top;
        min-height: 44px;
        padding-bottom: 5px;
    }
    .myAccountUserLoggedIn h2{
        font-size: 20px;
    }
    .signout.maSignoutBtn{
        float: right;
        font-size: 14px;
        font-family: 'Jost* 500';
        font-weight: 500;
        color: #151515;
        margin: 4px 0 0;
    }
    .profileDetailsInner table tr td {
        padding: 5px 10px;
    }
    .machangingAddress-popup .maco-adressDiv {
        border: 1px solid #b2b2b2;
        padding: 20px;
        font-size: 13px;
        color: #454545;
        font-family: 'Jost* 500';
        font-weight: 500;
    }
    .maco-adressDivinner {
        padding: 0 0 0 30px;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom: 0px;
        padding-left: 30px;
    }
    .maco-adressDivinner:before {
        content: '';
        width: 20px;
        height: 20px;
        position: absolute;
        top: 0;
        left: 0;
        background: #ccc;
        border-radius: 100%;
        font-family: 'Glyphicons Halflings';
        opacity: 1;
    }
    .address {
        margin-bottom: 1rem;
        font-style: normal;
        line-height: inherit;
    }
    .machooseAddressBlock-popup-footer {
        padding: 0;
    }
    .machooseAddressBlock-popup-footer .row {
        margin: 0 -6px;
    }
    .machooseAddressBlock-popup-footerinner {
        padding: 12px;
        border-top: 1px solid #b2b2b2;
        z-index: 2;
        margin: 15px 0 0;
        text-align: center;
    }
    .macheckoutmakedefaultDiv {
        display: inline-block;
        vertical-align: top;
        margin: 20px 0;
    }
    .macheckoutmakedefaultDiv .mabtnSaveCheckout {
        display: inline-block;
        float: none;
        vertical-align: top;
        margin: 0;
    }
</style>
</body>
</html>
