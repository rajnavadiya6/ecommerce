<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <link href="{{ asset('theme/css/web_theme.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
</head>
<body>
@include('by_pass.layout.header')
</ngx-json-ld>
</app-home>
<div _ngcontent-sc0="" class="clearfix"></div><!---->
<div _ngcontent-sc0="" class="scrollup" onclick="scrollUP()">
    <span _ngcontent-sc0="" class="kz-up-top"></span>
</div>

<app-page class="ng-star-inserted">
    <div class="clerfix"></div>
    <div class="col-12 contentContainerBlock">
        <div class="container">
            <div class="col-12 contentContainerInner">
                <div class="col-12 contentHeader"><h1 class="contentPageTitle">Privacy Policy</h1></div>
                <div class="clearfix"></div>
                <div class="col-12 contentContainer"><!---->
                    <div class="row ng-star-inserted">
                        <div class="col-2 contentLeftBlock">
                            <ul>
                                <li><a class="contentLinkActive" href="javascript:void(0)">Privacy Policy</a></li>
                            </ul>
                        </div><!---->
                        <div appnavigation="" class="col-10 contentRightBlock ng-star-inserted">
                            <div class="col-12 padding-0">
                                {!!  $setting['privacy_policy']  !!}
                            </div>
                        </div>
                    </div><!---->
                </div>
            </div>
        </div>
    </div>
</app-page>
@include('by_pass.layout.footer')
<div class="scrollup" onclick="scrollUP()"><span class="kz-up-top"></span></div>
<script src="{{ asset('theme/js/main.js') }}"></script>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    function fncHomeVideoPlay(videoURL) {
        var videoid = document.getElementById('homevideo');
        if (videoid.paused) {
            $(videoid).html('<source src="' + videoURL + '" type="video/mp4"></source>');
            setTimeout(function () {
                videoid.play();
                $('.iconHomePlay').addClass('iconHomePause');
                $('.videoPosterDiv').css('visibility', 'hidden');
                $('#homevideo').parent().addClass('homeVideoPlaying');
            }, 1000);
        } else {
            videoid.pause();
            $('.iconHomePlay').removeClass('iconHomePause');
            $('.videoPosterDiv').css('visibility', 'show');
            $('#homevideo').parent().removeClass('homeVideoPlaying');
        }
    }

    // Footer
    $('.footerData').click(function () {
        $(this).toggleClass('footerheadActive');
        $(this).siblings('.footerLinkDiv').toggleClass('displayData');
    });

    $('.men-container_slider').slick({
        dots: false,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });

    $('.women-container_slider').slick({
        dots: false,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });

    $('.accessories-container_slider').slick({
        dots: false,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });

</script>
<style>
    .displayData {
        display: block !important;
    }

    .brand-logo {

    @media screen and (min-width: 768px) {
        width:

    180px

    ;
        text-align: center

    ;
    }

    padding:

    5
    px

    0
    ;
    }

    /* WoMen sldier */
    .women-container_slider {

    .women-slick-slide {
        outline: none;
        height: auto;
        text-align: center;

    img {
        display: inline-block;
    }

    }
    }

    /*.slick-prev.slick-arrow{*/
    /*    width: 22px;*/
    /*    height: 22px;*/
    /*    bottom: auto;*/
    /*    top: 50%;*/
    /*    border-radius: 100%;*/
    /*    transform: translate(-50%, -50%);*/
    /*    -webkit-transform: translate(-50%, -50%);*/
    /*}*/
    /*.slick-prev {*/
    /*    left: -18px;*/
    /*}*/
    /*.slick-next{*/
    /*    left: auto;*/
    /*    right: -49px !important;*/
    /*}*/
    /*.slick-next.slick-arrow{*/
    /*    width: 22px;*/
    /*    height: 22px;*/
    /*    bottom: auto;*/
    /*    top: 50%;*/
    /*    border-radius: 100%;*/
    /*    transform: translate(-50%, -50%);*/
    /*    -webkit-transform: translate(-50%, -50%);*/
    /*}*/
    /*.slick-next {*/
    /*    right: 0;*/
    /*}*/
    /*.slick-next, .slick-prev {*/
    /*    position: absolute;*/
    /*    top: 0;*/
    /*    bottom: 0;*/
    /*    display: flex;*/
    /*    align-items: center;*/
    /*    justify-content: center;*/
    /*    width: 15%;*/
    /*    color: #fff;*/
    /*    text-align: center;*/
    /*    opacity: .5;*/
    /*}*/
    /*.slick-prev.slick-arrow:before{*/
    /*    content: "<" !important;*/
    /*}*/
    /*.slick-next.slick-arrow:before{*/
    /*    content: ">" !important;*/
    /*}*/


    /* Men sldier */
    .men-container_slider {

    .men-slick-slide {
        outline: none;
        height: auto;
        text-align: center;

    img {
        display: inline-block;
    }

    }
    }

    /* Accessories sldier */
    .accessories-container_slider {

    .accessories-slick-slide {
        outline: none;
        height: auto;
        text-align: center;

    img {
        display: inline-block;
    }

    }
    }


    /*Browse Button*/
    .menCategory a {
        right: 0;
        top: 0;
        background: #000;
        color: #fff;
        padding: 9px 20px;
        display: block;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Jost*';
        font-weight: 700;
        font-size: 10px;
        line-height: 16px;
    }

    .contentContainer {
        padding: 20px 0 0;
    }

    .contentLeftBlock {
        padding: 0;
        border-right: 1px solid #7d7d7d;
    }

    .contentLeftBlock ul li a.contentLinkActive, .contentLeftBlock ul li a:hover {
        color: #000;
        text-decoration: underline;
    }

    .contentLeftBlock ul li a {
        color: #858585;
        font-size: 18px;
        font-family: 'Jost* 500';
    }

    .contentPageTitle {
        font-size: 24px;
        color: #000;
        font-family: 'Jost*';
        font-weight: 700;
        display: inline-block;
        vertical-align: top;
        margin: 0;
    }

    .contentHeader {
        padding: 15px 0 14px;
        border-bottom: 1px solid rgba(0,0,0,.2);
        display: inline-block;
        vertical-align: top;
        margin: 0;
    }
</style>
</body>
</html>
