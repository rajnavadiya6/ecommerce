@php
    $setting = \App\Models\Setting::pluck('value','key')->toArray();
@endphp
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> OrderDetail | {{config('app.name')}}</title>
    <link rel="shortcut icon" href="{{ $setting['favicon'] }}" type="image/x-icon">
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <link href="{{ asset('theme/css/web_theme.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/pagination.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/hover-min.css') }}" />
</head>
<body>
@include('by_pass.layout.header')

<app-root ng-version="8.2.14">
    <ng-progress>
        <ng-progress-bar><!---->
            <div class="ng-progress ng-star-inserted">
                <div class="bar"
                     style="transition: all 200ms linear 0s; background-color: rgb(255, 255, 255); margin-left: -100%;">
                    <div class="bar-shadow"
                         style="box-shadow: rgb(255, 255, 255) 0px 0px 10px, rgb(255, 255, 255) 0px 0px 5px;">
                    </div>
                </div>
            </div>
        </ng-progress-bar>
    </ng-progress>
    <app-header class="ng-tns-c2-0 ng-star-inserted">
        <div class="col-xs-12 padding-zero"></div>
        <div class="popupblock popupFromBottom ng-tns-c2-0 ng-star-inserted" id="headerOfferPopup" style="display:none;">
            <div class="popuptbl">
                <div class="popuptr">
                    <div class="popupinner">
                        <div class="popupcontentDiv offerTemplatePopup"><a
                                class="popupClose" href="javascript:void(0)"
                                onclick="closePopup('headerOfferPopup')"><span
                                    class="kz-close"></span></a>
                            <div class="col-12 popupbody" appnavigation="">pdp image issue
                                fixing
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </app-header>
    <div class="clearfix"></div>
    <app-myaccount class="ng-star-inserted">
        <app-mysettings class="ng-star-inserted">
            <div class="col-12 bodyContent">
                <div class="container myAccountContainer">
                    <div class="clearfix"></div>
                    <div class="col-12 myAccountBlock">
                        <div class="col-12 myAccountHeader">
                            <h1 class="myAccountPageTitle">
                                Order Details
                            </h1>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-12 padding col-xl-12">
                                {{ Form::open(['route' => 'storeOrderDetails' ,'method','POST','autoComplete' => 'off']) }}
                                <div class="row mt-3">
                                    <div class="form-group col-sm-12 my-0">
                                        {{ Form::label('Name', 'Name'.':') }}<span
                                            class="text-danger">*</span>
                                        <input type="text" name="name" required class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="form-group col-sm-12 my-0">
                                        {{ Form::label('Name', 'Last Name'.':') }}<span
                                            class="text-danger">*</span>
                                        <input type="text" name="last_name" required class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="form-group col-sm-12 my-0">
                                        {{ Form::label('last name', 'Phone'.':') }}<span
                                            class="text-danger">*</span>
                                        <input type="number" name="phone" minlength="10"  maxlength="10" required class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="form-group col-sm-12 my-0">
                                        {{ Form::label('pincode', 'Pin Code'.':') }}<span
                                            class="text-danger">*</span>
                                        <input type="number" name="pincode" required class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="form-group col-sm-12 my-0">
                                        {{ Form::label('city', 'City'.':') }}<span
                                            class="text-danger">*</span>
                                        <input type="text" name="city" required class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="form-group col-sm-12 my-0">
                                        {{ Form::label('state', 'State'.':') }}<span
                                            class="text-danger">*</span>
                                        <input type="text" name="state" required class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="form-group col-sm-12 my-0">
                                        {{ Form::label('address', 'Full Address'.':') }}<span
                                            class="text-danger">*</span>
                                        <textarea  name="address_1" required class="form-control" cols="5" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="form-group col-sm-12 my-0">
                                    {{ Form::label('address', 'Gender'.':') }}<span
                                        class="text-danger">*</span>
                                    <div class="radio">
                                        <input autocomplete="off" formcontrolname="gender" id="radio" name="gender" required title="Male" type="radio" value="{{ \App\Models\User::GENDER['Male'] }}" class="ng-untouched ng-pristine ng-invalid">
                                        <label for="radio">
                                            <span>Male</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <input autocomplete="off" formcontrolname="gender" id="radio2" name="gender" required title="Female" type="radio" value="{{ \App\Models\User::GENDER['Female'] }}" class="ng-untouched ng-pristine ng-invalid">
                                        <label for="radio2">
                                            <span>Female</span>
                                        </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="form-group col-sm-12">
                                        {{ Form::submit('Save', ['class' => 'btn saveChangesBtn mr-2']) }}
                                        {{ Form::reset('Re Enter', ['class' => 'btn cancleChangesBtn text-dark']) }}
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </app-mysettings>
    </app-myaccount>
    <div class="clearfix"></div>
    <div class="scrollup" onclick="scrollUP()" style="bottom: -150px;">
        <span class="kz-up-top"></span></div>
</app-root>
@include('by_pass.layout.footer')
<div class="scrollup" onclick="scrollUP()"><span class="kz-up-top"></span></div>
@routes

<script src="{{ asset('theme/js/main.js') }}"></script>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    let removeProductToBag = '{{ route('removeProductToBag') }}';

    // Footer
    $('.footerData').click(function () {
        $(this).toggleClass('footerheadActive');
        $(this).siblings('.footerLinkDiv').toggleClass('displayData');
    });
</script>
<style>
    .displayData {
        display: block !important;
    }

    .saveChangesBtn{
        float: left;
        min-width: 250px;
        padding: 15px;
        background: #000;
        border: 1px solid #000;
        font-family: 'Jost* 500';
        font-size: 12px;
        color: #fff;
        text-align: center;
        text-transform: uppercase;
        transition: all .3s;
        margin: 10px 0 0;
    }
    .cancleChangesBtn{
        float: left;
        min-width: 250px;
        padding: 15px;
        border: 1px solid #000;
        font-family: 'Jost* 500';
        font-size: 12px;
        color: #fff;
        text-align: center;
        text-transform: uppercase;
        transition: all .3s;
        margin: 10px 0 0;
    }

    .brand-logo {
    @media screen and (min-width: 768px) {
        width:180px;
        text-align: center;
    }
        padding:5px 0;
    }

    /* Login Form*/

    .btnLoginTab {
        float: left;
        width: 50%;
        padding: 20px;
        font-family: 'Jost* 500';
        font-size: 12px;
        color: #a6a6a6;
        text-align: center;
        text-transform: uppercase;
        border-bottom: 2px solid #e5e5e5;
        transition: border-color .6s linear;
        -webkit-transition: border-color .6s linear;
    }

    .btnLoginTabSelected {
        color: #000;
        border-bottom-color: #000;
    }

    /* ENd Login Form*/

    /*This page*/

    .myAccountPageTitle {
        font-size: 24px;
        color: #000;
        font-family: 'Jost*';
        font-weight: 700;
        display: inline-block;
        vertical-align: top;
        margin: 0;
    }

    .myAccountPageMiddleHeader .myAccountPageTitle {
        font-weight: 400;
        font-family: 'Jost*';
    }

    .maDefaultAddress address {
        margin: 0 0 5px;
        padding: 0 0 0 22px;
        position: relative;
        line-height: 24px;
    }

    .myAccountHeader {
        padding: 15px 0 10px;
        border-bottom: 1px solid rgba(0, 0, 0, .2);
    }
</style>
</body>
</html>
