@extends('settings.index')
@section('title')
    Front Settings
@endsection
@section('section')
    {{ Form::open(['route' => 'settings.update', 'files' => 'true', 'method' => 'post', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data', 'id'=>'editFrontSettingForm']) }}
    <div class="row mt-3">
        <div class="form-group col-sm-12 my-0">
            {{ Form::label('address', 'Address'.':') }}<span
                    class="text-danger">*</span>
            <textarea name="address" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">
               {{ $setting['address'] }}
            </textarea>
        </div>
        <div class="form-group col-sm-6 mt-3">
            {{ Form::label('phone', 'Phone'.':') }}<span
                    class="text-danger">*</span></br>
            {{ Form::text('phone', $setting['phone'], ['class' => 'form-control','required']) }}
        </div>
        <div class="form-group col-sm-6 mt-3">
            {{ Form::label('email', 'Email'.':') }}<span
                    class="text-danger">*</span>
            {{ Form::email('email', $setting['email'], ['class' => 'form-control', 'required']) }}
        </div>
        <div class="form-group col-sm-6">
            <div class="row">
                <div class="col-6 col-xl-4">
                    {{ Form::label('footer', 'Footer Image'.':') }}
                    <span class="text-danger">*</span>
                    <label class="image__file-upload">Choose
                        {{ Form::file('footer_image',['id'=>'footerImage','class' => 'd-none']) }}
                    </label>
                </div>
                <div class="col-6 pl-0 mt-1">
                    <img id='footerImagePreview' class="img-thumbnail thumbnail-preview mt-4 width-40px"
                         src="{{($setting['footer_image']) ? asset($setting['footer_image']) : asset('img/dmca_protection.jpg') }}">
                </div>
            </div>
        </div>
    </div>
{{--    <hr>--}}
{{--    <span class="font-weight-bold">MANUFACTURED BY</span>--}}
{{--    <div class="row mt-3">--}}
{{--        <div class="form-group col-sm-12">--}}
{{--            {{ Form::label('manufactured company name', 'Manufactured Company Name'.':') }}<span--}}
{{--                class="text-danger">*</span></br>--}}
{{--            {{ Form::text('manufactured_company_name', $setting['manufactured_company_name'], ['class' => 'form-control','required']) }}--}}
{{--        </div>--}}
{{--        <div class="form-group col-sm-12 my-0">--}}
{{--            {{ Form::label('Manufactured Company address', 'Manufactured Company Address'.':') }}<span--}}
{{--                class="text-danger">*</span>--}}
{{--            <textarea name="manufactured_company_address" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">--}}
{{--               {{ $setting['manufactured_company_address'] }}--}}
{{--            </textarea>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="row mt-4">
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
            {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark']) }}
        </div>
    </div>
    {{ Form::close() }}
@endsection
@push('scripts')
{{--    <script src="{{ asset('assets/js/inttel/js/intlTelInput.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/js/inttel/js/utils.min.js') }}"></script>--}}
{{--    <script>--}}
{{--        let utilsScript = "{{asset('assets/js/inttel/js/utils.min.js')}}";--}}
{{--        let isEdit = true;--}}
{{--    </script>--}}
{{--    <script src="{{ mix('assets/js/custom/phone-number-country-code.js') }}"></script>--}}
@endpush
