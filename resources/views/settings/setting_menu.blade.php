<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body px-0">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a href="{{ route('settings.index', ['section' => 'general']) }}"
                           class="nav-link {{ (isset($sectionName) && $sectionName == 'general') ? 'active' : ''}}">
                            General
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('settings.index', ['section' => 'front_office_details']) }}"
                           class="nav-link {{ (isset($sectionName) && $sectionName == 'front_office_details') ? 'active' : ''}}">
                            Footer Settings
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('settings.index', ['section' => 'social_settings']) }}"
                           class="nav-link {{ (isset($sectionName) && $sectionName == 'social_settings') ? 'active' : ''}}">
                            Social Settings
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('settings.index', ['section' => 'policy_setting']) }}"
                           class="nav-link {{ (isset($sectionName) && $sectionName == 'policy_setting') ? 'active' : ''}}">
                            Policy Settings
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        @yield('section')
    </div>
</div>

