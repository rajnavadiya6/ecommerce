@extends('settings.index')
@section('title')
    General
@endsection
@section('section')
    {{ Form::open(['route' => 'settings.update', 'files' => 'true', 'method' => 'post', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) }}
    <div class="row mt-3">
        <div class="form-group col-sm-6">
            {{ Form::label('application_name', 'Application Name'.':') }}<span
                    class="text-danger">*</span>
            {{ Form::text('application_name', $setting['application_name'], ['class' => 'form-control', 'required']) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('application_name', 'Company Url'.':') }}<span
                    class="text-danger">*</span>
            {{ Form::text('company_url', $setting['company_url'], ['class' => 'form-control', 'required']) }}
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('home_header_title', 'Home Header Title'.':') }}<span
                    class="text-danger">*</span>
            {{ Form::text('home_header_title', $setting['home_header_title'], ['class' => 'form-control', 'required']) }}
        </div>
        <div class="form-group col-sm-12 my-0">
            {{ Form::label('company_description', 'Company Description'.':') }}<span
                    class="text-danger">*</span>
            <textarea name="company_description" class="form-control summerNote"  id="generalSummerNote" required rows="5" cols="5" style="height: 75% !important;">
               {{ $setting['company_description'] }}
            </textarea>
        </div>
    </div>
    <div class="row mt-3">
        <!-- Logo Field -->
        <div class="form-group col-sm-6">
            <div class="row">
                <div class="col-6 col-xl-3">
                    {{ Form::label('app_logo', 'Logo'.':') }}<span class="text-danger">*</span>
                    <i class="fas fa-question-circle ml-1 mt-1 general-question-mark" data-toggle="tooltip"
                       data-placement="top" title="Upload 90 x 60 logo to get best user experience."></i>
                    <label class="image__file-upload"> Choose
                        {{ Form::file('logo',['id'=>'logo','class' => 'd-none']) }}
                    </label>
                </div>
                <div class="col-6 col-xl-6 pl-0 mt-1">
                    <img id='logoPreview' class="img-thumbnail thumbnail-preview"
                         src="{{($setting['logo']) ? asset($setting['logo']) : asset('img/bypass_logo.svg')}}">
                </div>
            </div>
        </div>
        <div class="form-group col-sm-6">
            <div class="row">
                <div class="col-6 col-xl-4">
                    {{ Form::label('favicon', 'Favicon'.':') }}
                    <span class="text-danger">*</span>
                    <i class="fas fa-question-circle ml-1 mt-1 general-question-mark"
                                                         data-toggle="tooltip" data-placement="top"
                                                         title="The image must be of pixel 16 x 16 and 32 x 32."></i>
                    <label class="image__file-upload">Choose
                        {{ Form::file('favicon',['id'=>'favicon','class' => 'd-none']) }}
                    </label>
                </div>
                <div class="col-6 pl-0 mt-1">
                    <img id='faviconPreview' class="img-thumbnail thumbnail-preview mt-4 width-40px"
                         src="{{($setting['favicon']) ? asset($setting['favicon']) : asset('img/favicon.ico')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
            {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark','id'=>'btn-reset']) }}
        </div>
    </div>
    {{ Form::close() }}
@endsection
