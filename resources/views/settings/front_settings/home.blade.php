@extends('layouts.app')
@section('title')
    Settings
@endsection
@push('css')
    <style>
        .image__file-upload {
            padding: 10px;
            background: #6777ef;
            display: table;
            color: #fff;
            border-radius: 0.25rem;
            border-color: #6777ef;
        }


        .ticket-attachment {
            margin-bottom: 10px;
            margin-right: 12px;
            height: 70px;
            width: 100px;
            padding: .25rem;
            background-color: #fff;
            border: 1px solid #dee2e6;
            border-radius: .25rem;
            object-fit: contain;
        }

        .gallery-item.ticket-attachment {
             display: flex !important;
             justify-content: center;
             align-items: center;
         }

        .ticket-attachment__icon {
            text-align: center;
        }

        .attachment__create {
             max-height: 20vh;
             overflow-y: auto;
         }

        .attachment__section {
             max-height: 39vh;
             overflow-y: auto;
         }
    </style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Home</h1>
        </div>
        <div class="section-body">
            <div class="card">
                @include('flash::message')
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="card-body py-0">
                    {{ Form::open(['route' => 'front.settings.home.update', 'files' => 'true', 'method' => 'post', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) }}

                    <div class="form-group col-xl-12 col-md-12 col-sm-12">
                        {{ Form::label('attachment', 'Home Images'.':') }}<small class="ml-3 text-warning">Recommend Size: (1200 * 500)</small>
                        <div class="d-flex">
                            <label class="image__file-upload bg-primary text-white mr-2"> Choose
                                {{ Form::file('hero_image[]',['id'=>'ticketAttachment','class' => 'd-none','multiple']) }}
                            </label>
                            <div id="attachmentFileSection" class="attachment__create"></div>
                        </div>
                    </div>
                    @if($heroImage)
                        <div class="col-xl-12 col-sm-12 col-lg-12 pl-0">
                            <div class="gallery gallery-md">
                                @foreach($heroImage as $img)
                                    <div class="gallery-item ticket-attachment"
                                         data-image="{{ url($img->name) }}"
                                         data-title="{{ $img->name }}"
                                         href="{{ url($img->name) }}" title="{{ $img->name }}">
                                        <div class="ticket-attachment__icon d-none">
                                            <a href="{{ url($img->name) }}" target="_blank"
                                               class="text-decoration-none text-primary" data-toggle="tooltip"
                                               data-placement="top"
                                               title="View"><i class="fas fa-eye"></i>
                                            </a>
                                            <a href="javascript:void(0)"
                                               class="text-danger text-decoration-none attachment-delete"
                                               data-id="{{ $img->id }}" data-toggle="tooltip" data-placement="top"
                                               title="Delete"><i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="row mt-3">
                            <div class="form-group col-sm-12 my-0">
                                {{ Form::label('title_1', 'Title 1'.':') }}<span
                                    class="text-danger">*</span>
                                {{ Form::text('title_1', $frontSetting['title_1'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="form-group col-sm-12 my-0">
                                {{ Form::label('title_1_description', 'Title Description 1'.':') }}<span
                                    class="text-danger">*</span>
                                <textarea name="title_1_description" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">
                                    {{ $frontSetting['title_1_description'] }}
                                </textarea>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="form-group col-sm-12 my-0">
                                {{ Form::label('title_2', 'Title 2'.':') }}<span
                                    class="text-danger">*</span>
                                {{ Form::text('title_2', $frontSetting['title_2'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="form-group col-sm-12 my-0">
                                {{ Form::label('title_2_description', 'Title Description 2'.':') }}<span
                                    class="text-danger">*</span>
                                <textarea name="title_2_description" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">
                                        {{ $frontSetting['title_2_description'] }}
                                    </textarea>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="form-group col-sm-12 my-0">
                                {{ Form::label('title_3', 'Title 3'.':') }}<span
                                    class="text-danger">*</span>
                                {{ Form::text('title_3', $frontSetting['title_3'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="form-group col-sm-12 my-0">
                                {{ Form::label('title_3_description', 'Title Description 3'.':') }}<span
                                    class="text-danger">*</span>
                                <textarea name="title_3_description" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">
                                        {{ $frontSetting['title_3_description'] }}
                                    </textarea>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="form-group col-sm-12 my-0">
                                {{ Form::label('title_4', 'Title 4'.':') }}<span
                                    class="text-danger">*</span>
                                {{ Form::text('title_4', $frontSetting['title_4'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="form-group col-sm-12 my-0">
                                {{ Form::label('title_4_description', 'Title Description 4'.':') }}<span
                                    class="text-danger">*</span>
                                <textarea name="title_4_description" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">
                                        {{ $frontSetting['title_4_description'] }}
                                    </textarea>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="form-group col-sm-12">
                                {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                                {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark']) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script>
    $(document).ready(function (){
        $('.summerNote').summernote({
            height:150,
        });
    });
    document.querySelector('#ticketAttachment').
    addEventListener('change', handleFileSelect, false);
    let selDiv = document.querySelector('#attachmentFileSection');

    function handleFileSelect (e) {
        if (!e.target.files || !window.FileReader) return;

        selDiv.innerHTML = '';
        let files = e.target.files;
        for (let i = 0; i < files.length; i++) {
            let f = files[i];
            let reader = new FileReader();
            reader.onload = function (e) {
                if (f.type.match('image*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="' +
                        e.target.result + '">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('pdf*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/pdf_icon.png">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('zip*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/zip_icon.png">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('sheet*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/xlsx_icon.png">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('text*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/txt_icon.png">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('msword*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/doc_icon.png">';
                    selDiv.innerHTML += html;
                } else {
                    selDiv.innerHTML += f.name;
                }

            };
            reader.readAsDataURL(f);
        }
    }

    $(document).on('mouseenter', '.ticket-attachment', function () {
        $(this).find('.ticket-attachment__icon').removeClass('d-none');
    });

    $(document).on('mouseleave', '.ticket-attachment', function () {
        $(this).find('.ticket-attachment__icon').addClass('d-none');
    });
    let deleteMediaUrl = '{{ url('delete-home-slider') }}';

    $(document).on('click', '.attachment-delete', function (event) {
        let id = $(event.currentTarget).data('id');
        swal({
                title: 'Delete !',
                text: 'Are you sure want to delete this "Attachment" ?',
                type: 'warning',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                confirmButtonColor: '#00b074',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No',
                confirmButtonText: 'Yes',
            },
            function () {
                $.ajax({
                    url: deleteMediaUrl + '/' + id,
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (obj) {
                        swal({
                            title: 'Deleted!',
                            text: 'Attachment has been deleted.',
                            type: 'success',
                            confirmButtonColor: '#00b074',
                            timer: 2000,
                        });
                        if (obj.success) {
                            window.location.reload();
                        }
                        
                    },
                    error: function (data) {
                        swal({
                            title: '',
                            text: data.responseJSON.message,
                            type: 'error',
                            confirmButtonColor: '#00b074',
                            timer: 5000,
                        });
                    },
                });
            });
    });
    </script>
@endpush
