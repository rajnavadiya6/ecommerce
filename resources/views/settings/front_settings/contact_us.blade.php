@extends('layouts.app')
@section('title')
    Contact us
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Contact Us</h1>
        </div>
        {{ Form::open(['route' => 'front.settings.contactUs.update','method'=> 'post']) }}
        <div class="section-body">
            @include('flash::message')

            <h3>CORPORATE OFFICE</h3>
            <div class="card">
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="card-body py-0">
                    <div class="row mt-3">
                        <div class="form-group col-sm-6 my-0">
                            {{ Form::label('for_corporate_email', 'Corporate Email'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_corporate_email', $frontSetting['for_corporate_email'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                        <div class="form-group col-sm-6 my-0">
                            {{ Form::label('for_corporate_phone', 'Corporate Phone'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_corporate_phone', $frontSetting['for_corporate_phone'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="form-group col-sm-12 my-0">
                            {{ Form::label('for_corporate_address', 'Corporate Address'.':') }}<span
                                class="text-danger">*</span>
                            <textarea name="for_corporate_address" class="form-control corporateAddressSummerNote" required rows="5" cols="5" style="height: 75% !important;">
                                    {{ $frontSetting['for_corporate_address'] }}
                                </textarea>
                        </div>
                    </div>
                </div>
            </div>

            <h3>FOR ORDERS ON BY PASS.IN</h3>
            <div class="card">
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="card-body py-0">
                    <div class="row mt-3">
                        <div class="form-group col-sm-6 my-0">
                            {{ Form::label('for_orders_phone', 'For Order Phone'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_orders_phone', $frontSetting['for_orders_phone'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                        <div class="form-group col-sm-6 my-0">
                            {{ Form::label('for_orders_email', 'For Order Email'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_orders_email', $frontSetting['for_orders_email'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="form-group col-sm-12 my-0">
                            {{ Form::label('for_orders_description', 'For Order Description'.':') }}<span
                                class="text-danger">*</span>
                            <textarea name="for_orders_description" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">
                                    {{ $frontSetting['for_orders_description'] }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>

            <h3>FOR ORDERS ON EXCLUSIVE BYPASS STORES</h3>
            <div class="card">
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="card-body py-0">
                    <div class="row mt-3">
                        <div class="form-group col-sm-6 my-0">
                            {{ Form::label('for_orders_exclusive_phone', 'For Order Exclusive Phone'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_orders_exclusive_phone', $frontSetting['for_orders_exclusive_phone'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                        <div class="form-group col-sm-6 my-0">
                            {{ Form::label('for_orders_exclusive_email', 'For Order Exclusive Email'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_orders_exclusive_email', $frontSetting['for_orders_exclusive_email'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="form-group col-sm-12 my-0">
                            {{ Form::label('for_orders_exclusive_description', 'For Order Exclusive Description'.':') }}<span
                                class="text-danger">*</span>
                            <textarea name="for_orders_exclusive_description" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">
                                    {{ $frontSetting['for_orders_exclusive_description'] }}
                                </textarea>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="form-group col-sm-12 my-0">
                            {{ Form::label('for_orders_exclusive_address', 'For Order Exclusive Address'.':') }}<span
                                class="text-danger">*</span>
                            <textarea name="for_orders_exclusive_address" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">
                                    {{ $frontSetting['for_orders_exclusive_address'] }}
                                </textarea>
                        </div>
                    </div>
                </div>
            </div>

            <h3>FOR CAREERS</h3>
            <div class="card">
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="card-body py-0">
                    <div class="row mt-3">
                        <div class="form-group col-sm-12 my-0">
                            {{ Form::label('for_careers_email', 'For Careers Email'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_careers_email', $frontSetting['for_careers_email'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                    </div>
                </div>
            </div>

            <h3>FRANCHISEE CONTACT (EXCLUSIVE OUTLET):</h3>
            <div class="card">
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="card-body py-0">
                    <div class="row mt-3">
                        <div class="form-group col-sm-6 my-0">
                            {{ Form::label('for_franchisee_contact_email', 'For Franchisee Contact Email'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_franchisee_contact_email', $frontSetting['for_franchisee_contact_email'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                        <div class="form-group col-sm-6 my-0">
                            {{ Form::label('for_franchisee_contact_phone', 'For Franchisee Contact Phone'.':') }}<span
                                class="text-danger">*</span>
                            {{ Form::text('for_franchisee_contact_phone', $frontSetting['for_franchisee_contact_phone'], ['class' => 'form-control','required','id'=>'phoneNumber']) }}
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="form-group col-sm-12">
                        {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                        {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark']) }}
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </section>
@endsection
@push('scripts')
    <script>
        $(document).ready(function (){
            $('.corporateAddressSummerNote').summernote({
                height:150,
            });
        });

        $(document).ready(function (){
            $('.summerNote').summernote({
                height:150,
            });
        });
    </script>
@endpush
