<div class="row">
    <div class="col-9 offset-md-3 selectedFiltersDivBlock ">
        <div class="filter-facet-selected"></div>
        {{--        <div class="sortByBlock">--}}
        {{--            <div class="sortByBlockInner">--}}
        {{--                <div class="selectedSortItem">Sort by</div>--}}
        {{--                <ul class="sortList">--}}
        {{--                    <li>Popular</li>--}}
        {{--                    <li>Price : Low to high</li>--}}
        {{--                    <li>Price : High to low</li>--}}
        {{--                    <li>Best seller</li>--}}
        {{--                </ul>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <div class="clearfix"></div>
    <div class="col-3 leftSideFilter filters-area" id="filters">
        <div class="filtersInner">
            <div class="clearfix"></div>
            <div class="col-12 filtersPanel ng-star-inserted ">
                <a class="btnCloseFilters" href="javascript:void(0)"></a>
                <div class="col-12 filterTitleDiv">
                    <h6>Filter By</h6>
                </div>

                <div class="panel-group " id="accordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading  panelHeadingActive categoryMenu" id="headingCategory">
                            <h4 class="panel-title">
                                <a href="javascript:void(0)" title="Categories">
                                    Categories
                                </a>
                                <a class="filClear" href="javascript:void(0)"> Clear </a>
                            </h4>
                        </div>
                        <div class="panel-collapse in collapse filterListBody " role="tabpanel" id="categories">
                            <div class="row">
                                <a class="mobileFilterCategoryHeading" href="javascript:void(0)" title="Categories">
                                    Categories</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body categoryList" id="">
                                <ul class="filterList " id="categorieslist">
                                    @foreach($subCategories as $category)
                                        <li class="">
                                            {{--                                            <label class="custom-control fill-checkbox">--}}
                                            {{--                                                <input wire:model="categoryFilter" class="fill-control-input ng-untouched ng-pristine ng-valid" name="categories" type="radio" id="categories-{{ $category->id }}" value="{{ $category->id }}">--}}
                                            {{--                                                <span class="fill-control-indicator"></span>--}}
                                            {{--                                                <span class="fill-control-description">{{ $category->name }}</span>--}}
                                            {{--                                            </label>--}}
                                            <div class="radio">
                                                <input name="gender" wire:model="categoryFilter" type="radio" id="categories-{{ $category->id }}" value="{{ $category->id }}" class="ng-dirty ng-valid ng-touched">
                                                <label for="categories-{{ $category->id }}"><span>{{ $category->name }}</span></label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-heading panelHeadingActive categoryMenu" id="headingCategory">
                            <h4 class="panel-title"><a href="javascript:void(0)" title="Size">
                                    Size
                                </a><a class="filClear" href="javascript:void(0)"> Clear </a>
                            </h4>
                        </div>
                        <div class="panel-collapse in collapse filterListBody" role="tabpanel" id="size">
                            <div class="row">
                                <a class="mobileFilterCategoryHeading" href="javascript:void(0)" title="Size">
                                    Size</a>
                            </div>
                            <div class="clearfix"></div>
                            <div clascategory=s="panel-body" id="">
                                <ul class="filterList ng-star-inserted" id="sizelist">
                                    @foreach($variants as $variant)
                                        <li class="">
                                            <div class="radio">
                                                <input name="gender" wire:model="variantFilter" type="radio" id="variants-{{ $variant->id }}" value="{{ $variant->name }}" class="ng-dirty ng-valid ng-touched">
                                                <label for="variants-{{ $variant->id }}"><span>{{ $variant->name }}</span></label>
                                            </div>
                                            {{--                                            <div class="checkbox">--}}
                                            {{--                                                <label class="fill-checkbox" for="size-s">--}}
                                            {{--                                                    <input class="" name="size" id="size_{{$variant->name}}" type="checkbox" wire:model="variantFilter" value="{{ $variant->name }}">--}}
                                            {{--                                                    <span class="fill-control-indicator"> {{ $variant->name }}</span>--}}
                                            {{--                                                </label>--}}
                                            {{--                                            </div>--}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-heading panelHeadingActive categoryMenu" id="headingCategory">
                            <h4 class="panel-title"><a href="javascript:void(0)" title="Color">
                                    Color
                                </a><a class="filClear" href="javascript:void(0)"> Clear </a>
                            </h4>
                        </div>
                        <div class="panel-collapse in collapse filterListBody" role="tabpanel" id="color">
                            <div class="row">
                                <a class="mobileFilterCategoryHeading" href="javascript:void(0)" title="Color">
                                    Color</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body" id="">
                                <ul class="filterList colorsList " id="colorlist">
                                    @foreach($colors as $color)
                                        <li>
                                            <div class="radio">
                                                <input name="color" wire:model="colorFilter"  type="radio" id="colors-{{ $color->id }}" value="{{ $color->name }}" class="colorInput ng-dirty ng-valid ng-touched">
                                                <label for="colors-{{ $color->id }}" class="colors">
                                                    <span>{{ $color->name }}</span>
                                                </label>
                                            </div>
                                            {{--                                            <div class="checkbox">--}}
                                            {{--                                                <input type="checkbox" id="color-black" value="{{ $color->name }}" wire:model="colorFilter" class="ng-untouched ng-pristine ng-valid">--}}
                                            {{--                                                <label for="color-black" htmlfor="color-black">--}}
                                            {{--                                                    <span class="colorbox " style="background:{{ $color->name }};"></span>--}}
                                            {{--                                                    <small>{{ $color->name }}</small>--}}
                                            {{--                                                </label>--}}
                                            {{--                                            </div>--}}
                                        </li>
                                        {{--
                                        <div class="col-auto">--}}
                                        {{--                                        <label class="colorinput">--}}
                                        {{--                                            <input name="colors" type="checkbox" value="{{ $color->name }}"--}}
                                        {{--                                                   class="colorinput-input chk-color" >--}}
                                        {{--                                            <span class="colorinput-color" style="background-color: {{ $color->name }};"></span>--}}
                                        {{--                                        </label>--}}
                                        {{--
                                     </div>
                                     --}}
                                    @endforeach
                                </ul>
                                {{--                                <a class="filterShowMore " href="javascript:void(0)"><span class="spnMoreFil">Show--}}
                                {{--                     More</span><span class="spnLessFil">Show Less</span></a>--}}
                            </div>
                        </div>
                    </div>
                    {{--                    <div class="panel panel-default ">--}}
                    {{--                        <div class="panel-heading panelHeadingActive categoryMenu" id="headingCategory">--}}
                    {{--                            <h4 class="panel-title"><a href="javascript:void(0)" title="Price">--}}
                    {{--                                    Price--}}
                    {{--                                </a><a class="filClear" href="javascript:void(0)"> Clear </a>--}}
                    {{--                            </h4>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="panel-collapse in collapse filterListBody" role="tabpanel" id="price">--}}
                    {{--                            <div class="row">--}}
                    {{--                                <a class="mobileFilterCategoryHeading" href="javascript:void(0)" title="Price">--}}
                    {{--                                    Price</a>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="clearfix"></div>--}}
                    {{--                            <div class="panel-body" id="">--}}
                    {{--                                <div class="filterList listPriceSlider " id="pricelist">--}}
                    {{--                                    <div class="custom-slider ">--}}
                    {{--                                        <ng5-slider class="priceSlider ng5-slider" _nghost-sc10="">--}}
                    {{--                                            <span class="ng5-slider-span ng5-slider-bar-wrapper ng5-slider-left-out-selection" ng5sliderelement="" style="opacity:1;visibility:hidden;">\--}}
                    {{--                                                <span class="ng5-slider-span ng5-slider-bar"></span></span><span class="ng5-slider-span ng5-slider-bar-wrapper ng5-slider-right-out-selection" ng5sliderelement="" style="opacity:1;visibility:hidden;"><span class="ng5-slider-span ng5-slider-bar"></span></span><span class="ng5-slider-span ng5-slider-bar-wrapper ng5-slider-full-bar" ng5sliderelement="" style="opacity:1;visibility:visible;"><span class="ng5-slider-span ng5-slider-bar"></span></span><span class="ng5-slider-span ng5-slider-bar-wrapper ng5-slider-selection-bar" ng5sliderelement="" style="opacity:1;visibility:visible;"><span class="ng5-slider-span ng5-slider-bar ng5-slider-selection"></span></span><span class="ng5-slider-span ng5-slider-pointer ng5-slider-pointer-min" ng5sliderhandle="" style="opacity:1;visibility:visible;" role="" tabindex="" aria-orientation="" aria-label="" aria-labelledby="" aria-valuenow="" aria-valuetext="" aria-valuemin="" aria-valuemax=""></span><span class="ng5-slider-span ng5-slider-pointer ng5-slider-pointer-max" ng5sliderhandle="" style="display:inherit;opacity:1;visibility:visible;" role="" tabindex="" aria-orientation="" aria-label="" aria-labelledby="" aria-valuenow="" aria-valuetext="" aria-valuemin="" aria-valuemax=""></span><span class="ng5-slider-span ng5-slider-bubble ng5-slider-limit ng5-slider-floor" ng5sliderlabel="" style="opacity:1;visibility:visible;"></span><span class="ng5-slider-span ng5-slider-bubble ng5-slider-limit ng5-slider-ceil" ng5sliderlabel="" style="opacity:1;visibility:visible;"></span><span class="ng5-slider-span ng5-slider-bubble ng5-slider-model-value" ng5sliderlabel="" style="opacity:1;visibility:visible;"></span><span class="ng5-slider-span ng5-slider-bubble ng5-slider-model-high" ng5sliderlabel="" style="opacity:1;visibility:visible;"></span><span class="ng5-slider-span ng5-slider-bubble ng5-slider-combined" ng5sliderlabel="" style="opacity:1;visibility:visible;"></span><span class="ng5-slider-ticks" ng5sliderelement="" hidden="" style="opacity:1;visibility:visible;">--}}
                    {{--                                            </span>--}}
                    {{--                                        </ng5-slider>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
                <div class="clearfix"></div>
                <div class="col-12 filtersBottom">
                    <div class="row"></div>
                    <div class="clearfix"></div>
                    {{--                    <div class="filtersButton">
                                                <a href="javascript:void(0)">Submit Filters</a>--}}
                    {{--                    </div>--}}
                    <div class="productsItemsCountFilters">
                        <b>{{ count($products) }}</b> Items
                    </div>
                    <a class="resetFilters" wire:click="resetFilters" href="javascript:void(0);">
                        <b><span class="kz-refresh"></span></b>
                        Reset Filter
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-9 rightsideProducts">
        <div class="productListShow">
            <div class="clearBoth"></div>
            <div class="col-12 productsFetchHere listTwoBlock ">
                <div class="row">
                    @foreach($products as $product)
                        <div class="col-md-4 eachProData {{ ($product->colors()->count()) ? '' : 'productHaveColors' }}  ">
                            <div class="col-12 productItemBlock">
                                <a class="col-12 productItemBlockInner" target="_blank" href="{{ route('men.details',$product->id) }}" title="{{ $product->name }}" id="black-bold-brief-fp01">
                                    <div class="productItemDivBlock">
                                        <img alt="Product Image" class="productBlankImg product-image-{{$product->id}}" src="{{ $product->image }}" style="object-fit: cover;width: 230px;height: 350px;"/>
                                        <div class="popularProDiv">
                                            {{--                                                                                    <img id="product01" src="{{ $product->image }}" alt="Black Bold Brief">--}}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-12 productItemInfo">
                                        <div class="productItemInfoInner">
                                            <span class="spnListColorCount">{{ $product->colors()->count() }} <small>colors</small></span>
                                            <span class="spnProrice spnStyleNum">Style - FP01</span>
                                            <div class="clearfix"></div>
                                            <h4>{{ $product->name }}</h4>
                                            <div class="divPriceStyle">
                                                <span class="spnProrice">₹ {{ $product->price }}</span>
                                                @if($product->updated_at->format('y-m-d') >= \Carbon\Carbon::now()->format('y-m-d'))
                                                    <span class="proItemTag">New</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                @if($product->colors()->count())
                                    <div class="availableSizesList ">
                                        <div class="availableColorsBlock">
                                            <ul class="availableColorlist">
                                                @foreach($product->colors as $color)
                                                    <li>
                                                      <span class="spnAvailColor color-hover" data-url="{{ $color->image }}" data-id="{{ $product->id }}">
                                                        <span style="background-color: {{ $color->color }};border-radius: 50%;border: 1px"></span>
                                                      </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <div class="divPriceStyle">
                                                <span class="spnProrice">₹ {{ $product->price }}</span><span class="proItemTag ng-star-inserted">New</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-end">
            @if($products->count() > 0)
                {{$products->links()}}
            @endif
        </div>
    </div>
</div>
