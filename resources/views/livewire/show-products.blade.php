<div class="card">
    <div class="card-header justify-content-end">
        <div class="card-header-form">
            <div class="input-group">
                <input wire:model="txtSearch" type="text" class="form-control" placeholder="Search">
                <div class="input-group-btn">
                    <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            @foreach($products as $product)
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3">
                    <div class="card product-card">
                        <div class="ribbon float-right ribbon-primary favorite-companies-ribbon">
                            {{ \App\Models\Product::STATUS[$product->status] }}
                        </div>
                        <a href="{{ route('products.show',$product->id) }}" class="card-image-hover">
                            <img class="card-img product-card__image" src="{{ $product->image }}" alt="Vans">
                        </a>
                        <!-- Slide Description Image Area (316 x 328) -->
                        {{--                        <div class="script-wrap">--}}
                        {{--                            <ul class="script-group">--}}
                        {{--                                <li><div class="inner-script"><img class="img-responsive" src="{{ asset('web/images/baa1.jpg') }}" alt="Dummy Image" /></div></li>--}}
                        {{--                                <li><div class="inner-script"><img class="img-responsive" src="{{ asset('web/images/baa2.jpg') }}" alt="Dummy Image" /></div></li>--}}
                        {{--                                <li><div class="inner-script"><img class="img-responsive" src="{{ asset('web/images/baa3.jpg') }}" alt="Dummy Image" /></div></li>--}}
                        {{--                            </ul>--}}
                        {{--                            <div class="slide-controller">--}}
                        {{--                                <a href="#" class="btn-prev"><img src="{{ asset('web/images/btn_prev.png') }}" alt="Prev Slide" /></a>--}}
                        {{--                                <a href="#" class="btn-play"><img src="{{ asset('web/images/btn_play.png') }}" alt="Start Slide" /></a>--}}
                        {{--                                <a href="#" class="btn-pause"><img src="{{ asset('web/images/btn_pause.png') }}" alt="Pause Slide" /></a>--}}
                        {{--                                <a href="#" class="btn-next"><img src="{{ asset('web/images/btn_next.png') }}" alt="Next Slide" /></a>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="clearfix"></div>
                        <div class="card-body">
                            <a href="{{ route('products.show',$product->id) }}" class="card-title h4 text-primary">{{ $product->name }}</a>
                            <h6 class="card-subtitle mt-3 mb-2 text-muted">Category: {{ $product->category_name }}</h6>
                            <h6 class="card-subtitle mb-2 text-muted">Sub
                                Category: {{ $product->sub_category_name }}</h6>
                            @foreach($product->variants as $variant)
                                <span class="badge badge-primary">{{ $variant->name }}</span>
                            @endforeach
                            <div class="d-flex mt-2 justify-content-start">
                                @foreach($product->colors as $color)
                                    <div class="">
                                        <label class="colorinput">
                                            <input name="colors" type="checkbox" class="colorinput-input chk-color">
                                            <span class="colorinput-color" style="background-color: {{ $color->color }};border-radius: 50%;border: 1px"></span>
                                            <input type="hidden" class="product-other-image-src" value="{{ $color->image }}">
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="buy d-flex justify-content-between align-items-center">
                                <div class="price text-primary"><h5 class="mt-4">${{ $product->price }}</h5></div>
                                {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('products.edit', [$product->id]) !!}" class='btn btn-warning action-btn edit-btn mr-2'><i class="fa fa-edit"></i></a>
                                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="card-footer d-flex justify-content-end">
        @if($products->count() > 0)
            {{$products->links()}}
        @endif
    </div>
</div>
