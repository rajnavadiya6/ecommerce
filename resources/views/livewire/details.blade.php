<div>
    @if(Session::has('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif
    <div class="col-12 product-info-wrapper">
        <div class="row rowWrapper">
            <div class="col-12 productDIv">
                <div class="row productDIvrow">
                    <div class="col-7 product-left-block">
                        <div class="col-xs-12 pdpLeftBlockinner">
                            <a class="mobileWishlist" href="javascript:void(0)"></a>
                            {{--                                            <span class="productPacks ">--}}
                            {{--                                                <span class="">--}}
                            {{--                                                    <img alt="Product pack image" src="{{ asset('theme/images/men/icon-1pack.jpg') }}"--}}
                            {{--                                                                    class="">--}}
                            {{--                                                </span>--}}
                            {{--                                            </span>--}}
                            <span class="mobilewishlist "></span>
                            <div class="carousel slide" data-interval="false" data-ride="carousel" id="pdpSlider">
                                <div class="carousel-inner my-gallery" tabindex="0">
                                    <div class="carousel-item active">
                                        <a attr.data-size="1098x1463" class="item active"
                                            data-fancybox="gallery" data-med-size="1098x1463"
                                            href="{{ $product->image }}" tabindex="0"
                                            data-med="{{ $product->image }}">
                                            <img class="pdpSliderImg sideBarImageDisplay"
                                                 data-toggle="tooltip" src="{{ $product->image }}"
                                                 alt="{{ $product->name }}"
                                                 title="">
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <ul class="carousel-indicators mCustomScrollbar _mCS_1 mCS-autoHide  mCS_no_scrollbar"
                                    id="carousel-indicators" style="overflow: visible;">
                                    <div id="mCSB_1" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside"
                                         style="max-height: none;" tabindex="0">
                                        <div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y"
                                             style="position:relative; top:0; left:0;" dir="ltr">
                                            @foreach($product->colors as $color)
                                                @foreach($color->all_image as $image)
                                                <li data-target="#pdpSlider" class="" data-slide-to="{{ $color->id }}">
                                                    <a href="javascript:void(0)">
                                                        <img data-toggle="tooltip"
                                                             src="{{ $image->getFullUrl() }}" alt="image"
                                                             title="{{ $product->name }}"
                                                             class="mCS_img_loaded sideBarImage">
                                                    </a>
                                                </li>
                                                @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                    <div id="mCSB_1_scrollbar_vertical"
                                         class="mCSB_scrollTools mCSB_1_scrollbar mCS-minimal-dark mCSB_scrollTools_vertical"
                                         style="display: none;">
                                        <div class="mCSB_draggerContainer">
                                            <div id="mCSB_1_dragger_vertical" class="mCSB_dragger"
                                                 style="position: absolute; min-height: 50px; height: 0px; top: 0px; display: block; max-height: 14px;">
                                                <div class="mCSB_dragger_bar"
                                                     style="line-height: 50px;">
                                                </div>
                                            </div>
                                            <div class="mCSB_draggerRail"></div>
                                        </div>
                                    </div>
                                </ul>

                                <a class="carousel-control-prev" data-slide="prev"
                                   href="#pdpSlider" role="button">
                                    <span aria-hidden="true" class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" data-slide="next"
                                   href="#pdpSlider" role="button">
                                    <span aria-hidden="true" class="carousel-control-next-icon"></span>
                                </a>
                                <div class="pdpDisclaimer"></div>
                                <div class="videoBlock">
                                    <video class="pdpVideodiv" controls=""
                                           id="pdpVideo" loop="" muted="" width="100%">
                                        <source id="source_video" type="video/mp4"
                                                src="https://jockey.pc.cdn.bitgravity.com/1011.mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                                <a class="watchVideo hideInMobile "
                                   href="javascript:void(0)" id="pdpVideo_desktop"
                                   style="display: none;"> Watch video
                                </a>
                                <a class="watchVideo hideInMobile "
                                   href="javascript:void(0)" id="pdpVideo_desktop_pause"
                                   style="display: none;"> Pause video
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-5 product-right-block">
                        <div class="col-12 productInforight">
                            <div class="col-12 pdpTitleHeader ">
                                <div class="clearfix"></div>
                                <h1 class="pdp-proname">
                                    {{ $product->name }}
                                </h1>
                                {{--                                                <div class="collection-heads">--}}
                                {{--                                                    <div class="pdpsmtext">--}}
                                {{--                                                        <a href="javascript:void(0);">Style :--}}
                                {{--                                                            #1011</a>--}}
                                {{--                                                    </div>--}}
                                {{--                                                    <div class="pdpsmtext">Collection : <a--}}
                                {{--                                                            href="men/collection/elance.html">Elance</a></div>--}}
                                {{--                                                    <div class="pdpsmtext pdp-nq">Net Qty : 2 N</div>--}}
                                {{--                                                </div>--}}
                            </div>
                            <div class="clearfix"></div>
                            <div class="fullFledge pdp-priceBlock">
                                <div class="pdp-price">
                                    <h2 class="display-price ">MRP <b>₹</b> {{ $product->price }}.00
                                    </h2><small>(incl. of all taxes)</small>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-12 pdp-pro-colors">
                                <span class="colorssmhead"><b>Color</b> : <br>  </span>
                                <div class="col-xs-12 padding-zero">
                                    <ul class="pdpColorslist colorLblGap">
                                        @foreach($product->colors as $color)
                                            <li class="productColorImage">
                                                <div class="checkbox roundedcheck">
                                                    <input name="check" type="checkbox" value="{{ $color->name }}" id="color0">
                                                    <label class="notclickable pdpcolorselected" for="color0" htmlfor="color0">
                                                        <span class="colorbox">
                                                            <img data-toggle="tooltip" src="{{ $color->image }}" title="{{ $product->name }}" alt="img" style="cursor:pointer;">
                                                        </span>
                                                        <small></small>
                                                    </label>
                                                    <div class="newTagPDP ">New</div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-12 pdp-pro-sizes">
                                <div class="pdp-sizehover sizehoverarrow">
                                    <span class="spnSizelabel"><b>Size : </b>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-12 pdp-pro-sizesinner">
                                    @foreach($product->variants as $variant)
                                        <div class="pdpSizeDiv">
                                            <a class="pdpSizeLink variantproductSize  {{ $productSize == $variant->name ? 'sizeSelected' :'' }}" wire:click="addSizeToCart({{ $variant->id }})" value="{{ $variant->name }}"  data-id="{{ $variant->id }}" href="javascript:void(0)">
                                                <span>{{ $variant->name }}</span>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="fullFledge pdp-buybuttons">
                                <div class="pdptQty">
                                    <a class="qtyPlus" href="javascript:void(0)">
                                        <span wire:click="increment" class="kz-plus"></span>
                                    </a>
                                    <input class="form-control" readonly="" type="text" value="{{ $qty }}">
                                    @if($qty <= 1)
                                        <a class="qtyMinus pointerNone" href="javascript:void(0)">
                                            <span wire:click="decrement" class="kz-minus"></span>
                                        </a>
                                    @else
                                        <a class="qtyMinus" href="javascript:void(0)">
                                            <span wire:click="decrement" class="kz-minus"></span>
                                        </a>
                                    @endif
                                </div>
                                <div class="col-12 pdpBtnAddtocartDiv">
                                    <a class="pdpBtnAddtocart" wire:click="addToBag({{ $product->id }})"
                                       value="{{ $product->id }}" href="javascript:void(0)">
                                        <span>Add to Bag</span>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="fabricReturns">
                                <a href="{{ route('page.return-exchange') }}">Know More</a>
                                about returns and exchange
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
