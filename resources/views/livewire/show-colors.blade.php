<div class="card">
    <div class="card-body">
        <div class="row">
            @foreach($colors as $color)
                <div class="d-flex flex-column align-items-center justify-content-center mr-3">
                    <div class="mb-3" style="height: 50px;width: 50px;background-color: {{$color->name}}"></div>
                    <div class="buy d-flex justify-content-between align-items-center">
                        <a href="javascript:void(0)" data-id="{{$color->id}}" class='edit-btn  mr-1'><i class="fa fa-edit text-warning"></i></a>
                        <a href="javascript:void(0)" data-id="{{$color->id}}" class='delete-btn mr-1 '><i class="fa fa-trash text-danger "></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="card-footer d-flex justify-content-end">
        @if($colors->count() > 0)
            {{$colors->links()}}
        @endif
    </div>
</div>
