<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fashion Website</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- //for-mobile-apps -->
    <link href="{{ asset('web/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <!-- pignose css -->
    <link href="{{ asset('web/css/pignose.layerslider.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <!-- //pignose css -->
    <link href="{{ asset('web/css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>

    <script type="text/javascript" src="{{ asset('web/js/jquery-2.1.4.min.js') }}"></script>
    <!-- cart -->
    <script type="text/javascript" src="{{ asset('web/js/simpleCart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/bootstrap-3.1.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/jquery.easing.min.js') }}"></script>
    @stack('css')
</head>
<body>
<header>
    @include('web.layout.header')
</header>
    @yield('content')
<footer>
    @include('web.layout.footer')
</footer>
<script type="application/x-javascript">
    addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
</script>
<!-- js -->
@stack('js')
</body>
</html>
