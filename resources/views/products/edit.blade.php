@extends('layouts.app')
@section('title')
    Edit Product
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading m-0">Edit Product</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                <a href="{{ route('products.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
        <div class="content">
            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body ">
                                {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'patch','files' => true]) !!}
                                <div class="row">
                                    @include('products.fields')
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script src="{{ asset('js/jquery.uploadPreview.min.js') }}"></script>
    <script>
        let productColorDelete = "{{ route('productColorDelete') }}";
        let productColorImageDelete = "{{ route('productColorImageDelete') }}";

        $.uploadPreview({
            input_field: '#image-upload',   // Default: .image-upload
            preview_box: '#image-preview',  // Default: .image-preview
            label_field: '#image-label',    // Default: .image-label
            label_default: 'Choose File',   // Default: Choose File
            label_selected: 'Change File',  // Default: Change File
            no_label: false,                // Default: false
            success_callback: null,          // Default: null
        });
        let getSubCategoriesURL = '{{ route('get.sub.categories') }}';
        let oldSubCategory = '{{ old('sub_category_id') ?? $product->sub_category_id }}';

        $('.searchIsActive').on('change', function () {
            $.ajax({
                url: '/products/change-is-active',
                method: 'post',
                data: $('#searchIsActive').serialize(),
                dataType: 'JSON',
                success: function (result) {
                    if (result.success) {
                        displaySuccessMessage(result.message);
                        location.reload();
                    }
                },
                error: function (result) {
                    displayErrorMessage(result.responseJSON.message);
                },
            });
        });

        $('.productColorDelete').on('click',function (){
            let productColorId = $(this).attr('data-id');
            $.ajax({
                url : productColorDelete,
                data : {id:productColorId},
                methods:'GET',
                success: function (result) {
                    if (result.success) {
                        displaySuccessMessage(result.message);
                        location.reload();
                    }
                },
                error: function (result) {
                    displayErrorMessage(result.responseJSON.message);
                },
            });
        });

        $('.productColorImageDelete').on('click',function (){
            let productImageId = $(this).attr('data-id');
            $.ajax({
                url : productColorImageDelete,
                data : {id:productImageId},
                methods:'GET',
                success: function (result) {
                    if (result.success) {
                        displaySuccessMessage(result.message);
                        location.reload();
                    }
                },
                error: function (result) {
                    displayErrorMessage(result.responseJSON.message);
                },
            });
        });
    </script>
    <script src="{{ mix('assets/js/ticket/create-edit.js') }}"></script>
@endpush
