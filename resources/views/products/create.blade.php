@extends('layouts.app')
@section('title')
    Create Product
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading m-0">New Product</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                <a href="{{ route('products.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
        <div class="content">
            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body ">
                                {!! Form::open(['route' => 'products.store','files' => true]) !!}
                                <div class="row">
                                    @include('products.fields')
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script src="{{ asset('js/jquery.uploadPreview.min.js') }}"></script>
    <script>
        let getSubCategoriesURL = '{{ route('get.sub.categories') }}';
        let oldSubCategory = '{{ old('sub_category_id') }}';

        $('.searchIsActive').on('change', function () {
            if($(this).prop("checked") == true){
                $('.productImageDiv').addClass('d-none');
            }else{
                $('.productImageDiv').removeClass('d-none');
            }
            $.ajax({
                url: '/products/change-is-active',
                method: 'post',
                data: $('#searchIsActive').serialize(),
                dataType: 'JSON',
                success: function (result) {
                    if (result.success) {
                        displaySuccessMessage(result.message);

                    }
                },
                error: function (result) {
                    displayErrorMessage(result.responseJSON.message);
                },
            });
        });
    </script>
    <script src="{{ mix('assets/js/ticket/create-edit.js') }}"></script>
@endpush
