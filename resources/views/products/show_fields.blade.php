<!-- Category Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('category_id', 'Category Name:') !!}
    <p>{{ $product->category_name }}</p>
</div>


<!-- Sub Category Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('sub_category_id', 'Sub Category Name:') !!}
    <p>{{ $product->sub_category_name }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-md-6">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $product->name }}</p>
</div>


<!-- Seller Name Field -->
<div class="form-group col-md-6">
    {!! Form::label('seller_name', 'Seller Name:') !!}
    <p>{{ $product->seller_name }}</p>
</div>

<!-- Price Field -->
<div class="form-group col-md-6">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $product->price }}</p>
</div>


<!-- Status Field -->
<div class="form-group col-md-6">
    {!! Form::label('status', 'Status:') !!}
    <br>
    <span class="badge {{ $product->status ? 'badge-primary' : 'badge-warning' }} ">{{ \App\Models\Product::STATUS[$product->status] }}</span>
</div>

<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ \Carbon\Carbon::parse($product->created_at)->diffForHumans() }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ \Carbon\Carbon::parse($product->updated_at)->diffForHumans() }}</p>
</div>


<!-- Description Field -->
<div class="form-group col-md-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $product->description }}</p>
</div>

<!-- Image Field -->
<div class="form-group col-md-12">
    {!! Form::label('id', 'Image:',['class' => 'font-weight-bold']) !!}
    <img src="{{ $product->image }}"/>
</div>

