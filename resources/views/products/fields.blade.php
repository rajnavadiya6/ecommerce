<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}<span class="text-danger">*</span>
    {!! Form::text('name', old('name'), ['class' => 'form-control '. ($errors->has('name') ? 'is-invalid':''),'required']) !!}
    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
</div>

<!-- Seller Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_name', 'Seller Name:') !!}
    {!! Form::text('seller_name', old('seller_name'), ['class' => 'form-control '. ($errors->has('seller_name') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('seller_name') }}</div>
</div>

<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category:') !!}<span class="text-danger">*</span>
    {!! Form::select('category_id', $categories, (isset($product)) ? $product->category_id : $categories->keys()->first(), ['class' => 'form-control select2 '. ($errors->has('category_id') ? 'is-invalid':'') ,'id' => 'category','required']) !!}
    <div class="invalid-feedback">{{ $errors->first('category_id') }}</div>
</div>

<!-- Sub Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sub_category_id', 'Sub Category:') !!}<span class="text-danger">*</span>
    {!! Form::select('sub_category_id', [], null, ['class' => 'form-control select2 '. ($errors->has('sub_category_id') ? 'is-invalid':''),'id' => 'subCategory','required']) !!}
    <div class="invalid-feedback">{{ $errors->first('sub_category_id') }}</div>
</div>


<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}<span class="text-danger">*</span>
    {!! Form::select('status', \App\Models\Product::STATUS, null, ['class' => 'form-control select2 '. ($errors->has('status') ? 'is-invalid':''),'required']) !!}
    <div class="invalid-feedback">{{ $errors->first('status') }}</div>
</div>


<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}<span class="text-danger">*</span>
    {!! Form::number('price', null, ['class' => 'form-control '. ($errors->has('price') ? 'is-invalid':''),'required']) !!}
    <div class="invalid-feedback">{{ $errors->first('price') }}</div>
</div>


<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', old('description'), ['class' => 'form-control '. ($errors->has('description') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
</div>
@if(isset($product))
    <div class="form-group col-sm-12">
        <label class="form-label">Colors :</label>
        @foreach($product->colors as $color)
            <div class="row mb-3 gutters-xs">
                <div class="col-auto d-flex flex-column align-items-center justify-content-center">
                    <label class="colorinput">
                        <span class="colorinput-color " style="background-color: {{ $color->color }}"></span>

                    </label>
                    <button class="btn btn-sm btn-outline-danger productColorDelete mb-2" data-id="{{ $color->id }}" style="width: 100px">Remove Color</button>
                    <button class="btn btn-sm btn-outline-success mt-2" style="width: 100px">Add Image</button>
                </div>
                <div class="col-10 ml-3">
                    <div class="d-flex align-items-center ">
                        @foreach($color->all_image as $image)
                            <img src="{{ $image->getFullUrl() }}" alt="" class=" mb-2 ml-2" width="80" height="80">
                            <button type="button"  class="ml-2 mb-2 btn-icon-round  btn btn-sm btn-outline-danger productColorImageDelete" data-id="{{ $image->id }}"><i class="fa fa-times-circle"></i></button>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@else
    <div class="form-group col-sm-12">
        <label class="form-label">Colors :</label>
        @foreach($colors as $color)
            <div class="row gutters-xs">
                <div class="col-auto">
                    <label class="colorinput">
                        <input name="colors[]" type="checkbox" value="{{ $color->name }}" class="colorinput-input chk-color">
                        <span class="colorinput-color " style="background-color: {{ $color->name }}"></span>
                    </label>
                </div>
                <div class="col-auto color-image d-none">
                    <div class="d-flex">
                        <div class="col-12 mb-3 align-items-center colo-input-main">
                            <div class="d-flex color-input-div align-items-center mb-3">
                                <input type="file" name="colors-img[{{ $color->name }}][]" class="form-control color-img-input">
                            </div>
                        </div>
                        <button type="button" class="btn-icon-round mt-2 btn btn-sm btn-outline-success btn-add-file">
                            <i class="fa fa-plus-circle"></i></button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif

<div class="form-group col-sm-6">
    <label class="form-label">Variants :</label>
    <div class="selectgroup selectgroup-pills">
        @foreach($variants as $variant)
            <label class="selectgroup-item">
                <input type="checkbox" name="variants[]" value="{{ $variant->name  }}" class="selectgroup-input">
                <span class="selectgroup-button">{{ $variant->name }}</span>
            </label>
        @endforeach
    </div>
</div>

{{--<div class="form-group col-sm-6">--}}
{{--    <form method="post" id="searchIsActive">--}}
{{--        @csrf--}}
{{--        <div class="form-group mb-1">--}}
{{--            <label class="custom-switch switch-label row">--}}
{{--                <input type="checkbox" name="product_is_active" class="custom-switch-input searchIsActive" {{ ($settings['product_is_active'] == 1) ? 'checked' : '' }} >--}}
{{--                <span class="custom-switch-indicator switch-span"></span>--}}
{{--            </label>--}}
{{--        </div>--}}
{{--    </form>--}}
{{--</div>--}}

{{--@if($settings['product_is_active'] == 0)--}}
    <!-- Image Field -->
    <div class="form-group col-sm-12 productImageDiv">
        <div id="image-preview" class="image-preview {{ $errors->has('image') ? 'border-danger' :''  }}" style="background-image: url('{{ isset($product) ? $product->image : '' }}')">
            <label for="image-upload" id="image-label">Choose File</label>
            <input type="file" name="image" id="image-upload" {{ isset($product) ? '' : 'required' }} >
        </div>
        <div class="text-danger">{{ $errors->first('image') }}</div>
    </div>
{{--@endif--}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('products.index') }}" class="btn btn-light">Cancel</a>
</div>















