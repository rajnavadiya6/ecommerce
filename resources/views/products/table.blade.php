<table class="table tab-bordered " id="products-table">
    <thead>
    <tr>
        <th>Image</th>
        <th>Category Name</th>
        <th>Sub Category Name</th>
        <th>Name</th>
        <th>Seller Name</th>
        <th>Price</th>
        <th>Description</th>
        <th class="action-column">Status</th>
        <th class="action-column">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td><img style="max-height: 50px;max-width: 50px;" src="{{ $product->image }}" alt="no image"/></td>
            <td>{{ $product->category_name }}</td>
            <td>{{ $product->sub_category_name }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->seller_name }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->description }}</td>
            <td class="text-center"><span
                        class="badge {{ $product->status ? 'badge-primary' : 'badge-warning' }} ">{{ \App\Models\Product::STATUS[$product->status] }}</span>
            </td>
            <td class="text-center">
                {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('products.show', [$product->id]) !!}" class='btn btn-light action-btn '><i
                                class="fa fa-eye"></i></a>
                    <a href="{!! route('products.edit', [$product->id]) !!}"
                       class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

