@extends('layouts.app')
@section('title')
    Products
@endsection
@push('css')
    @livewireStyles
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Products</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('products.create')}}" class="btn btn-primary form-btn"><i
                            class="fas fa-plus" style="font-size: 10px;"></i> Product</a>
            </div>
        </div>
        <div class="section-body">
            @livewire('show-products')
        </div>
    </section>
@endsection
@push('scripts')
    @livewireScripts
    <script type="text/javascript" src="{{ asset('web/js/pignose.layerslider.js') }}"></script>
    <script>
        $(document).on('click','.chk-color',function (){
            if($(this).is(":checked")) {
                $(this).parents().parent().parent().prevAll().find('.product-card__image').attr('src',$(this).next().next().val());
            }
        });
    </script>
@endpush
