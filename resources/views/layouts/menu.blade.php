<li class="side-menus {{ Request::is('dashboard*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('dashboard') }}">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>
<li class="side-menus {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <i class="fas fa-list-ul"></i><span>Categories</span>
    </a>
</li>

<li class="side-menus {{ Request::is('subCategories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('subCategories.index') }}">
        <i class="fas fa-sitemap"></i><span>Sub Categories</span>
    </a>
</li>

<li class="side-menus {{ Request::is('colors*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('colors.index') }}">
        <i class="fas fa-palette"></i><span>Colors</span>
    </a>
</li>

<li class="side-menus {{ Request::is('variants*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('variants.index') }}">
        <i class="fas fa-clipboard-list"></i><span>Variants</span>
    </a>
</li>

<li class="side-menus {{ Request::is('products*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('products.index') }}">
        <i class="fas fa-cubes"></i><span>Products</span>
    </a>
</li>

<li class="side-menus {{ Request::is('orders*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orders.index') }}">
        <i class="fas fa-first-aid"></i><span>Orders</span>
    </a>
</li>

<li class="side-menus {{ Request::is('setting/home*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('front.settings.home') }}">
        <i class="fas fa-home"></i><span>Home Page</span>
    </a>
</li>

<li class="side-menus {{ Request::is('setting/contact-us*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('front.settings.contactUs') }}">
        <i class="fas fa-id-card"></i><span>Contact us</span>
    </a>
</li>

<li class="side-menus {{ Request::is('settings*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('settings.index') }}">
        <i class="fas fa-cog"></i><span>Settings</span>
    </a>
</li>
