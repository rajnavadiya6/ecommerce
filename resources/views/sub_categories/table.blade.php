<div class="">
    <table class="table table-striped table-bordered " id="subCategories-table">
        <thead>
        <tr>
            <th>Category Name</th>
            <th>Image</th>
            <th>Name</th>
            <th class="action-column" style="width: 15%">Total Product</th>
            <th class="action-column">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subCategories as $subCategory)
            <tr>
                <td>{{ $subCategory->category_name }}</td>
                <td><img class="table-img" src="{{ $subCategory->image }}" alt="no image"/></td>
                <td>{{ $subCategory->name }}</td>
                <td class="text-center"><a href="{{ route('products.index') }}" class="badge badge-primary">{{ $subCategory->total_product }}</a> </td>
                <td class="text-center">
                    {!! Form::open(['route' => ['subCategories.destroy', $subCategory->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('subCategories.show', [$subCategory->id]) !!}"
                           class='btn btn-light action-btn '><i
                                    class="fa fa-eye"></i></a>
                        <a href="{!! route('subCategories.edit', [$subCategory->id]) !!}"
                           class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

