<!-- Category Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('category_id', 'Category:',['class' => 'font-weight-bold']) !!}
    <p>{{ $subCategory->category_name }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-md-6">
    {!! Form::label('name', 'Name:',['class' => 'font-weight-bold']) !!}
    <p>{{ $subCategory->name }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:',['class' => 'font-weight-bold']) !!}
    <p>{{ $subCategory->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:',['class' => 'font-weight-bold']) !!}
    <p>{{ $subCategory->updated_at }}</p>
</div>

<!-- Image Field -->
<div class="form-group col-md-12">
    {!! Form::label('id', 'Image:',['class' => 'font-weight-bold']) !!}
    <img src="{{ $subCategory->image }}" />
</div>
