@extends('layouts.app')
@section('title')
    Sub Category Details 
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Sub Category Details</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('subCategories.index') }}"
                     class="btn btn-primary form-btn float-right">Back</a>
            </div>
        </div>
        <div class="section-body">
           <div class="card">
                <div class="card-body">
                    <div class="row">
                        @include('sub_categories.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
