@extends('layouts.app')
@section('title')
    Sub Categories
@endsection
@push('css')
    <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Sub Categories</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('subCategories.create')}}" class="btn btn-primary form-btn"> <i class="fas fa-plus" style="font-size: 10px"></i> Sub Category </a>
            </div>
        </div>
        <div class="section-body">
            @include('flash::message')
           <div class="card">
                <div class="card-body">
                    @include('sub_categories.table')
                </div>
           </div>
       </div>
    </section>
@endsection
@push('scripts')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ mix('assets/js/custom/custom-datatable.js') }}"></script>
    <script>
        $('#subCategories-table').DataTable();
    </script>
@endpush
