<div id="createModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Variant</h5>
                <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route' => 'variants.store','id'=>'createForm']) !!}
            <div class="modal-body">
                <div class="row">
                    @include('variants.fields')
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
