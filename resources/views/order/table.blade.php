<div class="table-responsive-sm">
    <table class="table table-striped table-bordered" id="orders-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Product Image</th>
            <th>Quantity</th>
            <th>City</th>
            <th>State</th>
            <th>Pin Code</th>
            <th>Phone</th>
            <th>Address 1</th>
            <th>Address 2</th>
            <th class="action-column">Total Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td>{{ $order->user->name }}</td>
                <td><img class="table-img" src="{{ $order->product->image }}" alt="no image"></td>
                <td>{{ $order->qty }}</td>
                <td>{{ $order->user->city }}</td>
                <td>{{ $order->user->state }}</td>
                <td>{{ $order->user->pincode }}</td>
                <td>{{ $order->user->phone }}</td>
                <td>{{ $order->user->address_1 }}</td>
                <td>{{ $order->user->address_2 }}</td>
                <td>{{ $order->product_total_price}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
