@extends('layouts.app')
@section('title')
    Colors
@endsection
@push('css')
    @livewireStyles
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/themes/classic.min.css"/> <!-- 'classic' theme -->
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Colors</h1>
            <div class="section-header-breadcrumb">
                <a class="btn btn-primary" data-toggle="modal" data-target="#createModal"> <i class="fas fa-plus" style="font-size: 10px;"></i> Color </a>
            </div>
        </div>
    <div class="section-body">
        @livewire('show-colors')
   </div>
    </section>
    @include('colors.create')
    @include('colors.edit')
@endsection
@push('scripts')
    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.es5.min.js"></script>
    <script>
        let indexUrl = "{{ route('colors.index') }}/";
        let tableName = '#colorstbl';
        let createFormId = '#createForm';
        let editFormId = '#editForm';
        let createModelId = '#createModal';
        let editModelId = '#editModal';
        let showModelId = '#showModal';
        let editButtonSelector = '.edit-btn';
        let deleteButtonSelector = '.delete-btn';
    </script>
    <script src="{{ mix('assets/js/color/create-edit.js')}}"></script>
@endpush
