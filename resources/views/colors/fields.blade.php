<!-- Code Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Color:') !!}
    <button class="color-picker"></button>
    <input type="hidden" name="name" value="" id="name">
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {{ Form::button('save', ['type'=>'submit','class' => 'btn btn-primary','id'=>'btnSave','data-loading-text'=>"<span class='spinner-border spinner-border-sm'></span> Processing..."]) }}
    <a href="{{ route('colors.index') }}" class="btn btn-light">Cancel</a>
</div>
