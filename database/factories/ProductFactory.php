<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id'     => $this->faker->randomDigitNotNull,
            'sub_category_id' => $this->faker->randomDigitNotNull,
            'name'            => $this->faker->word,
            'seller_name'     => $this->faker->word,
            'status'          => $this->faker->randomDigitNotNull,
            'price'           => $this->faker->randomDigitNotNull,
            'description'     => $this->faker->text,
            'created_at'      => $this->faker->date('Y-m-d H:i:s'),
            'updated_at'      => $this->faker->date('Y-m-d H:i:s'),
        ];
    }
}
