<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Seeder;

class DefaultColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $input = [
            ['name' => '#3E46A0'],
            ['name' => '#23030DF2'],
            ['name' => '#F25D52'],
        ];

        Color::insert($input);
    }
}
