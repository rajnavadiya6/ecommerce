<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Database\Seeder;

class DefaultSubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Category $category */
        $category = Category::whereName('Men')->first();
        $category2 = Category::whereName('Women')->first();
        $input = [
            ['category_id' => $category->id, 'name' => 'Shirt'],
            ['category_id' => $category->id, 'name' => 'T-shirt'],
            ['category_id' => $category->id, 'name' => 'Woodies'],
            ['category_id' => $category->id, 'name' => 'Track pants'],
            ['category_id' => $category->id, 'name' => 'Joggers'],
            ['category_id' => $category->id, 'name' => 'Shorts'],
            ['category_id' => $category->id, 'name' => 'Underwear'],
            ['category_id' => $category->id, 'name' => 'Socks'],

            ['category_id' => $category2->id, 'name' => 'T-shirt'],
            ['category_id' => $category2->id, 'name' => 'Track pants'],
            ['category_id' => $category2->id, 'name' => 'Shorts'],
            ['category_id' => $category2->id, 'name' => 'Sport bra'],
            ['category_id' => $category2->id, 'name' => 'Running bra'],
            ['category_id' => $category2->id, 'name' => 'Maternity bra'],
            ['category_id' => $category2->id, 'name' => 'Panties'],
        ];
        SubCategory::insert($input);
    }
}
