<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DefaultRoleSeeder::class);
        $this->call(DefaultUserSeeder::class);
        $this->call(DefaultCategorySeeder::class);
        $this->call(DefaultSubCategorySeeder::class);
        $this->call(DefaultColorSeeder::class);
        $this->call(DefaultVariantSeeder::class);
        $this->call(DefaultProductSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(FrontSettingSeeder::class);
    }
}
