<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DefaultProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::whereName('Men')->first();
        $category2 = Category::whereName('Women')->first();
        $subCategory = SubCategory::whereCategoryId($category->id)->first();
        $subCategory2 = SubCategory::whereCategoryId($category->id)->first();

//        $input = [
//            ['category_id' => $category->id, 'sub_category_id' => $subCategory->id,'name' => 'Men Shirt 1','status' => 1,'price' => 999,'updated_at'=>Carbon::now()],
//            ['category_id' => $category->id, 'sub_category_id' => $subCategory->id,'name' => 'Men Shirt 2','status' => 1,'price' => 500,'updated_at'=>Carbon::now()],
//            ['category_id' => $category->id, 'sub_category_id' => $subCategory->id,'name' => 'Men T-Shirt 1','status' => 1,'price' => 400,'updated_at'=>Carbon::now()],
//            ['category_id' => $category->id, 'sub_category_id' => $subCategory->id,'name' => 'Men T-Shirt 2','status' => 1,'price' => 500,'updated_at'=>Carbon::now()],
//            ['category_id' => $category->id, 'sub_category_id' => $subCategory->id,'name' => 'Men T-Shirt 3','status' => 1,'price' => 750,'updated_at'=>Carbon::now()],
//
//            ['category_id' => $category2->id, 'sub_category_id' => $subCategory2->id,'name' => 'Women Shirt 1','status' => 1,'price' => 1200,'updated_at'=>Carbon::now()],
//            ['category_id' => $category2->id, 'sub_category_id' => $subCategory2->id,'name' => 'Women Shirt 2','status' => 1,'price' => 960,'updated_at'=>Carbon::now()],
//            ['category_id' => $category2->id, 'sub_category_id' => $subCategory2->id,'name' => 'Women T-Shirt 1','status' => 1,'price' => 700,'updated_at'=>Carbon::now()],
//            ['category_id' => $category2->id, 'sub_category_id' => $subCategory2->id,'name' => 'Women T-Shirt 2','status' => 1,'price' => 800,'updated_at'=>Carbon::now()],
//            ['category_id' => $category2->id, 'sub_category_id' => $subCategory2->id,'name' => 'Women T-Shirt 3','status' => 1,'price' => 1400,'updated_at'=>Carbon::now()],
//        ];

        $input = [
            ['category_id' => $category->id, 'sub_category_id' => $subCategory->id,'name' => 'Men Shirt 1','status' => 1,'price' => 999,'updated_at'=>Carbon::now()],
            ['category_id' => $category2->id, 'sub_category_id' => $subCategory2->id,'name' => 'Women Shirt 1','status' => 1,'price' => 1200,'updated_at'=>Carbon::now()],
        ];

        Product::insert($input);
    }
}
