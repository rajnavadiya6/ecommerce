<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class DefaultCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $input = [
            ['name' => 'Men'],
            ['name' => 'Women'],
            ['name' => 'Accessories'],
        ];

        Category::insert($input);
    }
}
