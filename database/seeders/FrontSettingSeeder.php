<?php

namespace Database\Seeders;

use App\Models\FrontSetting;
use App\Models\HomeImageSlider;
use Illuminate\Database\Seeder;

class FrontSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Home Page
        $heroImageHome = [asset('web/images/banner1.jpg')];
        foreach ($heroImageHome as $image) {
            $homeImage = HomeImageSlider::create([
                'name' => $image,
            ]);
        }
        
//        $heroImageHome = FrontSetting::create([
//            'key' => 'hero_image',
//            'page' => 'home',
//            'value' => ''
//        ]);
//        $heroImageHome->addMediaFromUrl(asset('web/images/a1.png'))->toMediaCollection(FrontSetting::IMG_PATH);
//        $heroImageHome->addMediaFromUrl(asset('web/images/a2.png'))->toMediaCollection(FrontSetting::IMG_PATH);
//        $heroImageHome->addMediaFromUrl(asset('web/images/a3.png'))->toMediaCollection(FrontSetting::IMG_PATH);
//        $heroImageHome->addMediaFromUrl(asset('web/images/a4.png'))->toMediaCollection(FrontSetting::IMG_PATH);
        FrontSetting::create([
            'key' => 'title_1',
            'page' => 'home',
            'value' => 'Jockey - Clothing Mankind Since 1876'
        ]);
        FrontSetting::create([
            'key' => 'title_1_description',
            'page' => 'home',
            'value' => 'Founded in 1876, Jockey pioneered innerwear, evolving and innovating not only the product, but also the way it has been marketed over the years. Jockey is a leading manufacturer and marketer of comfort apparel sold in more than 140 countries around the world. The company is committed to quality, comfort, fashion, innovation and value. As Jockey grows in size and sophistication, the simple commitment to serve its consumer’s need for comfort continues to be the brand’s hallmark.'
        ]);
        FrontSetting::create([
            'key' => 'title_2',
            'page' => 'home',
            'value' => 'Jockey.in - India’s Official Online Store for Jockey Products']);
        FrontSetting::create([
            'key' => 'title_2_description',
            'page' => 'home',
            'value' => 'Jockey was set up under Page Industries Ltd. in 1994 with the key objective of bringing the world-renowned brand Jockey to India. Jockey manufactures, distributes and markets products for the whole family – Men, Women and Kids.'
        ]);
        FrontSetting::create([
            'key' => 'title_3',
            'page' => 'home',
            'value' => 'Most Popular Collections'
        ]);
        FrontSetting::create([
            'key' => 'title_3_description',
            'page' => 'home',
            'value' => 'USA Originals | Athleisure | Miss Jockey | Jockey Juniors | Thermals | Shapewear'
        ]);
        FrontSetting::create([
            'key' => 'title_4',
            'page' => 'home',
            'value' => 'Page Industries Limited'
        ]);
        FrontSetting::create([
            'key' => 'title_4_description',
            'page' => 'home',
            'value' => 'Page Industries Limited, located in Bangalore, India is the exclusive licensee of Jockey International Inc. (USA) for manufacture, distribution and marketing of the Jockey brand in India, Sri Lanka, Oman, Bangladesh, Nepal and UAE. Page Industries Ltd. became public limited in March 2007 and is listed on the BSE and the NSE of India.The promoters of Jockey in India are the Genomal family, who have been associated with Jockey International Inc. for over 65 years as their sole licensee in Philippines.'
        ]);

        //Contact us
        FrontSetting::create([
            'key' => 'for_corporate_address',
            'page' => 'contact_us',
            'value' => 'Page Industries Ltd,
Umiya Business Bay - Tower - 1, 7th Floor, Cessna Business Park,
Kadubeesanahalli, Varthur Hobli, Bengaluru- 560103.
CIN: L18101KA1994PLC016554'
        ]);
        FrontSetting::create([
            'key' => 'for_corporate_email',
            'page' => 'contact_us',
            'value' => 'care@jockeyindia.com'
        ]);
        FrontSetting::create([
            'key' => 'for_corporate_phone',
            'page' => 'contact_us',
            'value' => '1800-572-1299(Toll Free) / 1860-425-3333 (Monday to Saturday, IST 10:00 AM to 7:00 PM)'
        ]);

        FrontSetting::create([
            'key' => 'for_orders_description',
            'page' => 'contact_us',
            'value' => 'Please contact us at these details for queries, complaints regarding any purchase made at Jockey.in.'
        ]);
        FrontSetting::create([
            'key' => 'for_orders_email',
            'page' => 'contact_us',
            'value' => 'care@jockeyindia.com'
        ]);
        FrontSetting::create([
            'key' => 'for_orders_phone',
            'page' => 'contact_us',
            'value' => '1800-572-1299(Toll Free) / 1860-425-3333 (Monday to Saturday, IST 10:00 AM to 7:00 PM)'
        ]);

        FrontSetting::create([
            'key' => 'for_orders_exclusive_description',
            'page' => 'contact_us',
            'value' => 'Please contact us at these details for queries, complaints regarding any purchase made at retail stores.'
        ]);
        FrontSetting::create([
            'key' => 'for_orders_exclusive_address',
            'page' => 'contact_us',
            'value' => 'Page Industries Ltd,
Customer Care Department. Cessna Business Park, Tower-1, 3rd Floor,
Umiya Business Bay, Kadubeesanahalli Village, Varthur Hobli, Sarjapur
Marathahalli Outer Ring Road, Bangalore - 560103, Karnataka, INDIA'
        ]);
        FrontSetting::create([
            'key' => 'for_orders_exclusive_email',
            'page' => 'contact_us',
            'value' => 'care@jockeyindia.com; wecare@jockeyindia.com'
        ]);
        FrontSetting::create([
            'key' => 'for_orders_exclusive_phone',
            'page' => 'contact_us',
            'value' => '1800-572-1299(Toll Free) / 1860-425-3333 (Monday to Saturday, IST 10:00 AM to 7:00 PM)'
        ]);


        FrontSetting::create([
            'key' => 'for_careers_email',
            'page' => 'contact_us',
            'value' => 'care@jockeyindia.com'
        ]);

        FrontSetting::create([
            'key' => 'for_franchisee_contact_email',
            'page' => 'contact_us',
            'value' => 'franchisee@jockeyindia.com'
        ]);
        FrontSetting::create([
            'key' => 'for_franchisee_contact_phone',
            'page' => 'contact_us',
            'value' => '+91-80-49464646, +91-80-49454545'
        ]);
    }
}
