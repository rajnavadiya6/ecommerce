<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imageUrl = asset('img/bypass_logo.svg');
        $favicon = asset('img/favicon.ico');
        $footerImage = asset('img/dmca_protection.jpg');

        Setting::create(['key' => 'application_name', 'value' => 'Bypass']);
        Setting::create(['key' => 'company_url', 'value' => 'Bypass']);
        Setting::create(['key' => 'logo', 'value' => $imageUrl]);
        Setting::create(['key' => 'favicon', 'value' => $favicon]);
        Setting::create(['key' => 'footer_image', 'value' => $footerImage]);
        Setting::create(['key' => 'manufactured_company_name', 'value' => 'Page Industries Limited']);
        Setting::create(['key' => 'manufactured_company_address', 'value' => 'Umiya Business Bay-Tower-1, 7th Floor, Cessna Business Park,
                        Kadubeesanahalli, Varthur Hobli, Bengaluru- 560103.CIN: L18101KA1994PLC016554']);
        Setting::create(['key' => 'company_description', 'value' => 'Leading Laravel Development Company of India']);
        Setting::create([
            'key'   => 'address',
            'value' => 'Umiya Business Bay-Tower-1, 7th Floor, Cessna Business Park,
Kadubeesanahalli, Varthur Hobli, Bengaluru- 560103.
CIN: L18101KA1994PLC016554']);
        Setting::create(['key' => 'phone', 'value' => '+91 70163 33243']);
        Setting::create(['key' => 'email', 'value' => 'support@acchhe.com']);
        Setting::create(['key' => 'facebook_url', 'value' => 'https://www.facebook.com']);
        Setting::create(['key' => 'twitter_url', 'value' => 'https://twitter.com']);
        Setting::create(['key' => 'instagram_url', 'value' => 'https://instagram.com']);
        Setting::create([
            'key'   => 'youtube_url',
            'value' => 'https://www.youtube.com',
        ]);
        Setting::create([
            'key'   => 'pinterest_url',
            'value' => 'https://www.pinterest.com',
        ]);
        Setting::create(['key' => 'product_is_active', 'value' => '1']);
        Setting::create(['key' => 'home_header_title', 'value' => 'Festival']);
        Setting::create([
            'key' => 'return_policy',
            'value' => '<p>We’re not happy, if you’re not happy. Jockey is committed to providing utmost
                                    satisfaction to all the customers with superior quality products, exemplary shopping
                                    experience and eager customer support.</p>
                                <p>Despite all our efforts, if you are unsatisfied with the product you received, please
                                    call our Customer Care Team on 1800-572-1299(Toll Free) / 1860-425-3333 (Monday to
                                    Saturday, IST 10:00 AM to 7:00 PM)</p>
                                <p> or write to us at care@jockeyindia.com. Our team will revert to you at the
                                    earliest.</p>
                                <p><strong>Hygiene Best Practices</strong></p>
                                <p>To maintain hygiene for all our customers we do not accept returns on Face Masks,
                                    Innerwear - Vests, Underwear, Bras, Panties, Camisoles, Tank Tops, Shapewears,
                                    Thermals, Socks, Towels and Boxers or any other product falling under our innerwear
                                    category. </p>
                                <p><strong>Return Criteria</strong></p>
                                <p>You can return outerwear if it meets the following criteria</p>

                                <p>We do not accept return of any product(s) which is not purchased from www.jockey.in.
                                    We will not take responsibility for any such product(s) sent to us.</p>
                                <p><strong>How to return your product if…</strong></p>
                                <p><strong>1.The External Packaging was Damaged</strong></p>
                                <p>At the time of delivery if you notice that the external packing of the consignment is
                                    damaged or tampered with, please do not accept the package. Instead, add your
                                    remarks on the courier delivery sheet and raise a complaint with us immediately,
                                    citing your order details. We will resolve your issue with the respective courier
                                    partner.</p>
                                <p>Once our courier partner updates that your parcel is accepted for return and updates
                                    this on their website, we will start your refund process. </p>
                                <p>
                                    In the case of Prepaid order (Credit Card, Net Banking, Debit Card, Gift Cards,
                                    Wallets), we will refund the money to your mode of payment.
                                    In case of Cash-on-Delivery (COD) order, we will cancel your order. There is no need
                                    for refund in this scenario.
                                </p>
                                <p><strong>2.The Product was Damaged, had Manufacturing Defect(s) or the Wrong Product
                                        was Delivered</strong></p>
                                <p><strong>a) Return via Jockey.in:</strong></p>
                                <p>In the unlikely event that you do not receive your product in a good condition, is
                                    damaged or defective, is not the product you ordered or of a different size you can
                                    return your product.</p>
                                <p>Your product should fit the Return Criteria as mentioned above. You can call us on
                                    1800-572-1299(Toll Free) / 1860-425-3333 (Monday to Saturday, IST 10:00 AM to 7:00
                                    PM) or write to us at care@jockeyindia.com. Our Customer Care Team will guide you
                                    through the return and refund process. Our team will also send you a confirmation
                                    for return via SMS. </p>
                                <p>DO NOT return any product, before receiving confirmation from our team. In case any
                                    product is returned without confirmation, we do not guarantee any credit or
                                    refund.</p>
                                <p>Once our Quality Check Team has received the returned goods, our team will examine
                                    the returned products for all defects/variations, based on your claim. Upon
                                    confirmation from our Quality Check Team that your return fulfils all conditions
                                    mentioned above, we will initiate your refund. We will send you an email and SMS
                                    informing you if your return has been accepted or rejected.</p>
                                <p>Upon examination of product/invoice/order receipt that the error/defect/damage/delay
                                    has occurred due to customer, you will not be entitled for Refund. In such a case,
                                    the same product you returned will be sent back to you.</p>
                                <p><strong>b) Return through Self-Shipment:</strong></p>
                                <p>For ease of return we provide Reverse Pickup for free, depending on your area or PIN
                                    Code. In case Reverse Pickup is not possible, you are requested to return the
                                    product(s) by courier in original packaging, unused, with tags to Jockey India.
                                    Please send the package to:</p>
                                <address>
                                    Page Industries Ltd,
                                    C/O DHL Supply Chain India Pvt. Ltd.
                                    Survey No 313/1, 46/8, 50, 51/4, 51/5, 313/2A, 313/2B, 313/3,
                                    Mayasandra Village, Anekal - Attibele Road,
                                    Anekal Taluk,
                                    Bengaluru - 562107,
                                    Contact Number: 9513660705
                                </address>
                                <p>Upon examination of product/invoice/order receipt that the error/defect/damage/delay
                                    has occurred due to customer, you will not be entitled for Refund. In such a case,
                                    the same product you returned will be sent back to you.</p>
                                <p>Jockey India will issue a gift coupon up to a maximum of Rs. 100/- against the
                                    shipping charges, which can be only redeemed by the customer at www.jockey.in.</p>
                                <p><strong>Refund Process</strong></p>
                                <p>
                                    We may provide refunds if:
                                    (i) The order is cancelled by the customer or jockey.in (Page Industries Limited),
                                    before we ship
                                </p>
                                <p>
                                    (ii) The returned items fulfil all criteria for return as mentioned above
                                    <strong>Prepaid and COD Refunds</strong>
                                </p>
                                <p><strong>a) Refunds for Pre-paid Orders:</strong></p>
                                <p>If you have paid using a credit card, refund will be credited to your credit card
                                    account, (Will be reflected in next statement) within 5 to 7 working days. If you
                                    have paid using debit card/internet banking, amount will be refunded to your bank
                                    account within 7 to 14 working days. However, the actual credit to your account will
                                    depend on your bank’s processing time. If you do not receive a credit within this
                                    time, please check with your bank and let us know if you face any issues.</p>
                                <p><strong>b) Refunds for Cash-On-Delivery (COD) Orders:</strong></p>
                                <p>If you have returned one or more products purchased using our COD payment method, you
                                    must provide Bank Account details for refund:</p>
                                <ul>
                                    <li>Bank Account number</li>
                                    <li>Account holders name</li>
                                    <li>Bank branch</li>
                                    <li>Bank name</li>
                                    <li>IFSC Code</li>
                                    <li>Order ID</li>
                                    <li>Product Name against which refund must be initiated</li>
                                </ul>
                                <p>The amount will be refunded in the Bank account through Electronic Funds Transfer. We
                                    do not provide any cash/cheque/DD.</p>
                                <p>Except for the events as explicitly stated in this Policy, you will not be entitled
                                    for any refund.</p>
                                <p><strong>c) Refund Timelines</strong></p>
                                <p>We usually initiate eligible refunds within 7 working days from receipt of product(s)
                                    at our warehouse. We will keep you informed via email on the refund status. The
                                    refund to your bank or credit card account depends on your bank’s processing
                                    time.</p>
                                <p>
                                </p>
                                <p>&nbsp;</p>
                                <p><strong>Please Note:</strong> The product that you receive may vary in print and
                                    colour. Please check the product details and disclaimer on the product page before
                                    ordering.</p>',
        ]);
        Setting::create([
            'key' => 'privacy_policy',
            'value' => '
                                <p>Registration to our Services shall entitle us to send you promotional and
                                    transactional emails from us. Subscription to our newletters entitle us to send you
                                    periodic newsletters and promotional emails. . If you do not want to receive e-mails
                                    or any other communication from us, you may unsubscribe by clicking on ‘unsubscribe’
                                    at the footer of any email received from us. Alternately, you may also email us at
                                    care@jockeyindia.com.</p>
                                <p><strong>SMS Communications:</strong></p>
                                <p>Upon placing any order on our website, we shall be entitled to use your registered
                                    mobile number on the website to send transaction related SMS to you, irrespective of
                                    DND services being activated on your mobile. We may occasionally send promotional
                                    SMS to your registered mobile number. Customer hereby authorizes us to send
                                    transactional SMS to his registered number, even if the number is registered for DND
                                    “Do not Disturb” service.</p>
                                <p><strong>Information from Other Sources:</strong></p>
                                <p>We may also receive relevant information from other sources and use such information
                                    for providing better customer experience. </p>
                                <p><strong>Payment Information:</strong></p>
                                <p>We do not take Credit/Debit card or Net banking details on our website. On clicking
                                    the option Pay Now, you will be redirected to either Secure Payment Gateway or
                                    Bank’s Net Banking website for completing the transaction. You will then be required
                                    to enter your relevant card details or net banking details on the page to complete
                                    the transaction. On successful completion of the transaction, you will be redirected
                                    to our website with your order details and order number being displayed.</p>
                                <p>It is to be noted that we will not be storing any Bank related information on our
                                    records and none of our staffs will hold or be exposed to this information.</p>
                                <p><strong>4. Cookies:</strong></p>
                                <p>We do not take Credit/Debit card or Net banking details on our website. On clicking
                                    the option Pay Now, you will be redirected to either Secure Payment Gateway or
                                    Bank’s Net Banking website for completing the transaction. You will then be required
                                    to enter your relevant card details or net banking details on the page to complete
                                    the transaction. On successful completion of the transaction, you will be redirected
                                    to our website with your order details and order number being displayed.</p>
                                <p><strong>4. Cookies:</strong></p>
                                <p>It is not a mandatory requirement to accept cookies for using your account., However,
                                    cookies can be used to ensure quick and improved shopping experience. Cookies are
                                    files which will identify your computer to our server as a unique user when you
                                    visit pages on our website. This will save your time while you are using our website
                                    since your computer will already be identified as a unique user on our server. We
                                    use cookies only to ensure your improved shopping experience and not for obtaining
                                    or using any other personally identifiable information about you. Our cookies are
                                    free from virus and do not contain any personal details. You may configure your
                                    browser to prevent cookies from being set on your computer. If you reject cookies,
                                    you may still use the site, but your ability to use some areas of the site may be
                                    limited and you may not be able to use certain features of the site.</p>
                                <p><strong>5. Server Logs</strong></p>
                                <p>In order to ensure easy and comfortable surfing on our website, each time you visit
                                    our website, the server collects certain statistical information. These statistics
                                    are only used to provide us information in relation to the type of user using our
                                    website by maintaining history of page viewed and at no point they identify the
                                    personal details of the user. We may make use of this data to understand as to how
                                    our website is being used.</p>
                                <p><strong>6. Usage by Children</strong></p>
                                <p>We do not knowingly collect, maintain or sell products or services to people under age
                                    18 or use personal information from our website about children under age 18.
                                    Children should seek the consent of their parents before providing any information
                                    about themselves or their parents and other family members online.</p>
                                <p>ou can visit this site without revealing any personal information about yourself.
    However, some pages on this site may require registration/Login to access. We
                                    collect this information to better understand your needs and provide you with better
                                    services. If you choose not to provide the information, you may still access the
                                    other pages of this site. To access certain services or offerings you may be
                                    requested for personal information to enable us to contact you from time to time by
                                    either email, mobile, postal mail or other means; to provide you with the
                                    information about the products and services offered by us, special deals and
    promotions that may possibly be of interest to you; to measure consumer interest in
                                    our products and services; to customize the user experience; enforce the legal terms
    or any subscriber agreement which govern your subscription for services; perform
                                                                   market research; offer you extension of services; or to detect and protect against
                                    error, fraud and other criminal activity. While providing such information you
                                    accept that we may use personal information about you to improve the site, review
                                    the usage of the site, the content and design of the site, improve our promotional
                                    efforts, the product offerings and services.</p>
                                <p>Should you need to update or correct the information you provided, you may log in to
                                    your Account and update the same from ‘My Account’ section.
    PIL at its discretion may update this policy from time to time and you are advised
                                    to read the policy every time you access our website. </p>
                                <p><strong>7. Security</strong></p>
                                <p>We have in place appropriate technical and security measures to prevent unauthorized
    or unlawful access to or accidental loss of or destruction or damage to your
                                    information. When we collect data through our website, we collect your personal
                                    details on a secured server. The payment details are entered on the Payment
                                    Gateway\’s or Bank\’s page on a secured SSL. The data is transferred between Bank and
    gateways in an encrypted manner.</p>
                                <p>Though we are unable to guarantee 100% security, encryption makes it hard for a
                                                                                                                 hacker to decrypt your details. You are strongly recommended not to send full credit
    or debit card details in unencrypted electronic communications with us. Except on
                                    the Payment Gateways page of your respective bank, you are requested not to submit
    or otherwise / send to the website/ e-mail ids provided therein or to any other ids,
                                    your debit or credit card or any other banking details. We maintain physical,
                                    electronic and procedural safeguards in connection with the collection, storage and
    disclosure of your information. As per our security procedures we may occasionally
                                    request proof of identity before we disclose personal information to you. You are
                                    responsible for protecting against unauthorized access to your password and to your
                                    computer.</p>
                                <p><strong>8. Safety and Security Tips</strong></p>
                                <p>A. Protect your passwords.</p>
                                <p>B. Ensure your Personal Computer is protected:</p>
                                <p>C. Tips to bear in mind:</p>
                                <p><strong>9. Terms of Use, Notices, and Revisions</strong></p>
                                <p>If you choose to visit this site, your visit and any dispute over privacy is subject
                                    to this Notice and our Terms of Use, including limitations on damages, resolution of
                                    disputes, and application of the laws of India.</p>
                                <p>If you have any concern about privacy at our website, please contact us with a
                                    thorough description, and we will try to resolve it. Our business changes
                                    constantly, and our Privacy Policy and the Terms of Use will also change; but you
                                    should check our web site frequently to see recent changes. Unless stated otherwise,
                                    our current Privacy Policy applies to all information that we have about you and
    your account.</p>',
        ]);
        Setting::create([
            'key' => 'offer_policy',
            'value' => '                                    <p>&nbsp;</p>
                                    <div><strong>Terms and Conditions – Jockey Juniors Funcil Box Offer</strong></div>
                                    <ol>
                                        <li>This is the offer: Shop for Jockey Juniors products worth Rs. 799 and above
                                            and get a Funcil Box FREE
                                        </li>
                                    </ol>
                                </div>
                                <div>
                                    <p>&nbsp;</p>
                                    <div><strong>Qwikcilver Gift Card T&amp;C</strong></div>
                                    <ol>
                                        <li>The Gift Card is only valid on <a href="https://www.jockey.in/">www.jockey.in</a>
                                            The offer is not applicable on exclusive Jockey stores, Multibrand Jockey
                                            Store or other partner websites selling Jockey products.
                                        </li>
                                    </ol>
                                </div>
                                <p>&nbsp;</p>
                                <div><strong>HDFC Cashback Terms and Conditions</strong></div>
                                <ol>
                                    <li>The offer indicated in the promotion shall only be redeemed by purchasing any
                                        product through www.jockey.in and by paying through HDFC Credit or Debit Card.
                                    </li>
                                    <li><strong>This is the offer:</strong> Shop for Rs. 1500 &amp; above and get Flat
                                        10% instant OFF from HDFC Credit/Debit Cards, for a maximum of Rs. 500.
                                    </li>
                                    <li>In case of return, if the total order amount goes below 1500, the cashback will
                                        be calculated as below:
                                    </li>
                                </ol>
                                <div>
                                    <p>&nbsp;Note: Refund formula</p>
                                    <p>(amount paid by customer after cashback / Total Order Value) x Returned Items
                                        Value</p>
                                    <div>4. The offer is valid from 12th Jan 2021 to 31st March 2021.</div>
                                    <div>&nbsp;</div>
                                </div>
                                <div>
                                    <p>&nbsp;</p>
                                    <div><strong>IDFC Bank Terms and Conditions</strong></div>
                                    <ol>
                                        <li>In case, after return, the total order value goes below Rs. 1500, the refund
                                            will be calculated as below.
                                            <p><strong>&nbsp; &nbsp; &nbsp; For Example: </strong></p>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Total Order Value</th>
                                                        <th>Instant Off</th>
                                                        <th>Amount Paid by customer</th>
                                                        <th>Returned Items Value</th>
                                                        <th>Refund after return</th>
                                                        <th>Refund</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>1500</td>
                                                        <td>150</td>
                                                        <td>1500 – 150 = 1350</td>
                                                        <td>500</td>
                                                        <td>(1350/1500) x 500</td>
                                                        <td>Rs. 450</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <p><strong>Note:</strong> Refund formula = (amount paid by customer after
                                                cashback / Total Order Value) x Returned Items Value</p>
                                        </li>
                                    </ol>
                                </div>
                                <div>
                                    <p>&nbsp;</p>
                                    <div><strong>MX Player Terms and Conditions</strong></div>
                                    <ol>
                                        <li>In case, after return, the total order value goes below Rs. 2000, the refund
                                            will be calculated as below.
                                            <p><strong>&nbsp; &nbsp; &nbsp; For Example: </strong></p>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Total Order Value</th>
                                                        <th>Instant Off</th>
                                                        <th>Amount Paid by customer</th>
                                                        <th>Returned Items Value</th>
                                                        <th>Refund after return</th>
                                                        <th>Refund</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>2000</td>
                                                        <td>300</td>
                                                        <td>2000 – 300 = 1700</td>
                                                        <td>1000</td>
                                                        <td>(1700/2000) x 1000</td>
                                                        <td>Rs. 850</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <p><strong>Note:</strong> Refund formula = (amount paid by customer after
                                                cashback / Total Order Value) x Returned Items Value</p>
                                        </li>
                                        <li>In case an offer is non-functional because of any technical reason, the same
                                            shall be reissued/replaced with a same offer of the same value.
                                        </li>
                                    </ol>
                                </div>
                                <div>
                                    <p>&nbsp;</p>
                                    <div><strong>Times Points Terms and Conditions</strong></div>
                                    <ol>
                                        <li>The customer must use the Unique Code deployed by Times Points to avail this
                                            offer. Failing to use the code will exempt the user from the offer and
                                            Jockey India, the banking partner or any other partner will not be liable to
                                            refund the amount.
                                        </li>
                                        <li>In case, after return, the total order value goes below Rs. 2000, the refund
                                            will be calculated as below.
                                            <p><strong>&nbsp; &nbsp; &nbsp; For Example: </strong></p>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Total Order Value</th>
                                                        <th>Instant Off</th>
                                                        <th>Amount Paid by customer</th>
                                                        <th>Returned Items Value</th>
                                                        <th>Refund after return</th>
                                                        <th>Refund</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>2000</td>
                                                        <td>300</td>
                                                        <td>2000 – 300 = 1700</td>
                                                        <td>1000</td>
                                                        <td>(1700/2000) x 1000</td>
                                                        <td>Rs. 850</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <p><strong>Note:</strong> Refund formula = (amount paid by customer after
                                                cashback / Total Order Value) x Returned Items Value</p>
                                        </li>
                                    </ol>
                                </div>
                                <div>
                                    <p>&nbsp;</p>
                                    <div><strong>Vantage Circle Terms and Conditions</strong></div>
                                    <ol>
                                        <li>The offer indicated in the promotion shall only be redeemed by purchasing
                                            any product through www.jockey.in
        </li>
                                        <li>This is the offer: Get Gift Card worth Rs. 300 on minimum order value of Rs.
        2000.
        </li>
                                        <li>The customer must use the Code VCJCKY to avail this offer. Failing to use
            the code will exempt the user from the offer and Jockey India, the banking
                                            partner or any other partner will not be liable to refund the amount.
                                        </li>
                                        <li>This offer is only valid on www.jockey.in and is not valid at any EBO or MBO
        or affiliate Jockey stores or any other online platform across India.
                                        </li>
                                        <li>In case an offer is non-functional because of any technical reason, the same
                                            shall be reissued/replaced with a same offer of the same value.
                                        </li>
                                    </ol>
                                </div>
                                <div>
                                    <p>&nbsp;</p>
                                    <div><strong>Mastercard T&amp;C:- The Offer is closed on 31st August 2020.</strong>
                                    </div>
                                    <p>This is the offer</p>
                                    <ul>
                                        <li>Rs.200 Qwikcilver Gift Card by quoting the promotion code JOCKEY200</li>
                                        <li>Rs.250 Qwikcilver Gift Card by quoting the promotion code JOCKEY250</li>
                                    </ul>',
        ]);
        Setting::create([
            'key' => 'return_exchange',
            'value' => ' <p>We\’re not happy, if you\’re not happy. Jockey is committed to providing utmost
                                    satisfaction to all the customers with superior quality products, exemplary shopping
                                    experience and eager customer support.</p>
                                <p>Despite all our efforts, if you are unsatisfied with the product you received, please
                                    call our Customer Care Team on 1800-572-1299(Toll Free) / 1860-425-3333 (Monday to
                                    Saturday, IST 10:00 AM to 7:00 PM)
                                </p>
                                <p> or write to us at care@jockeyindia.com. Our team will revert to you at the
                                    earliest.</p>
                                <p><strong>Hygiene Best Practices</strong></p>
                                <p>To maintain hygiene for all our customers we do not accept returns on Face Masks,
                                    Innerwear - Vests, Underwear, Bras, Panties, Camisoles, Tank Tops, Shapewears,
                                    Thermals, Socks, Towels and Boxers or any other product falling under our innerwear
                                    category. </p>
                                <p><strong>Return Criteria</strong></p>
                                <p>You can return outerwear if it meets the following criteria</p>
                                <ul>
                                    <li>The product(s) should be purchased only from www.jockey.in and not from any
                                        other retailer, be it online or offline.
                                    </li>
                                    <li>Return request should be raised within 7 days from the date of delivery.</li>
                                    <li>Product(s) should be unused (other than fit trial) and unwashed.</li>
                                    <li>There should be no stains, marks, stitches or holes in the product(s), caused by
                                        the consumer
                                    </li>
                                    <li>All tags and original packaging should remain intact and should be sent along
                                        with the product(s)
                                    </li>
                                    <li>Original invoice should be returned with the product(s)</li>
                                    <li>Product(s) should reach us in sellable condition.</li>
                                    <li>Kindly note that we will not accept returns on consumer promotion items /
                                        freebies if the correct product has been delivered. In case we send a wrong
                                        product or the wrong size, we will accept your return.
                                    </li>
                                </ul>
                                <p>We do not accept return of any product(s) which is not purchased from www.jockey.in.
                                    We will not take responsibility for any such product(s) sent to us.</p>
                                <p><strong>How to return your product if…</strong></p>
                                <p><strong>1.The External Packaging was Damaged</strong></p>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Payment Mode</th>
                                            <th>Refund Method</th>
                                            <th>Refund Receipt Time (After Initiation)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Credit Card</td>
                                            <td>Reverse Credit</td>
                                            <td>5 to 7 Banking Days</td>
                                        </tr>
                                        <tr>
                                            <td>Debit Card</td>
                                            <td>Credit in Bank Account</td>
                                            <td>7 to 14 Banking Days</td>
                                        </tr>
                                        <tr>
                                            <td>Net Banking</td>
                                            <td>Credit in Bank Account</td>
                                            <td>7 to 14 Banking Days</td>
                                        </tr>
                                        <tr>
                                            <td>COD</td>
                                            <td>Bank Transfer/NEFT</td>
                                            <td>2 to 4 Banking Days</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <p>&nbsp;</p>
                                <p><strong>Please Note:</strong> The product that you receive may vary in print and
                                    colour. Please check the product details and disclaimer on the product page before
                                    ordering.</p>',
        ]);
    }
}
