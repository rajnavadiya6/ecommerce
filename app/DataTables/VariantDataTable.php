<?php

namespace App\DataTables;

use App\Models\Variant;

class VariantDataTable
{
    public function get()
    {
        $model = new Variant();
        /** @var Post $query */
        $query = Variant::all();
        $query->map(function ($item) {
            $id = $item['id'];
            $item['action'] = view('variants.datatables_actions', compact('id'))->render();

            return $item;
        });
        $field = $model->getFillable();
        array_push($field, 'action');
        $data = $query->map->only($field)->toArray();

        return array_map('array_values', $data);
    }
}
