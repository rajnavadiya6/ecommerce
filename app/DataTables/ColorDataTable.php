<?php

namespace App\DataTables;

use App\Models\Color;

class ColorDataTable
{
    public function get()
    {
        $model = new Color();
        /** @var Color $query */
        $query = Color::all();
        $query->map(function ($item) {
            $id = $item['id'];
            $item['action'] = view('colors.datatables_actions', compact('id'))->render();

            return $item;
        });
        $field = $model->getFillable();
        array_push($field, 'action');
        $data = $query->map->only($field)->toArray();

        return array_map('array_values', $data);
    }
}
