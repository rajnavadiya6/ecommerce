<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class ShowProducts extends Component
{
    public $txtSearch = '', $colorImg = '';
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $products = $this->searchProduct();
        return view('livewire.show-products', compact('products'));
    }

    public function searchProduct()
    {
        $query = Product::when($this->txtSearch, function (Builder $query) {
            $query->orwhereHas('subCategory', function ($query) {
                $query->where('name', 'like', '%' . strtolower($this->txtSearch) . '%');
            });
            $query->orwhere('name', 'like', '%' . strtolower($this->txtSearch) . '%');
            $query->orwhere('seller_name', 'like', '%' . strtolower($this->txtSearch) . '%');
            $query->orwhere('price', 'like', '%' . strtolower($this->txtSearch) . '%');
        })->orderByDesc('created_at');
        return $query->paginate(8);
    }

    /**
     * @param $param
     * @param $value
     */
    public function changeFilter($param, $value)
    {
        $this->resetPage();
        $this->$param = $value;
    }
}
