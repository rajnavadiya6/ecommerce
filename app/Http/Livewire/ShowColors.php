<?php

namespace App\Http\Livewire;

use App\Models\Color;
use Livewire\Component;

class ShowColors extends Component
{
    /**
     * @var mixed
     */
    public $txtSearch = '';
    protected $listeners = ['refreshComponent' => '$refresh'];


    public function render()
    {
        $colors = $this->searchColor();
        return view('livewire.show-colors',compact('colors'));
    }
    public function searchColor()
    {
        $query = Color::latest();
        return $query->paginate(8);
    }
}
