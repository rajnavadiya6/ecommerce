<?php

namespace App\Http\Livewire;

use App\Models\AddToBag;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Variant;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Details extends Component
{
    public $productId;
    public $colors, $variants;
    public $qty = 1;
    public $addToBag;
    public $removeProductToBag;
    public $addSizeToCart, $productSize;
    public $selectedProductSize = '';

    public function mount()
    {
        $this->colors = Color::all();
        $this->variants = Variant::all();
        $this->selectedProductSize = $this->productSize;
    }

    public function addSizeToCart($variantId)
    {
        $variantName = ProductVariant::find($variantId)->toArray();
        $this->productSize = $variantName['name'];
    }

    public function addToBag($productId)
    {
        if(!Auth::check()){
            return redirect()->back()->with('error', 'Please Login');
        }else{
//        $product = Product::find($productId)->toArray();
//        $product['product_qty'] = $this->qty;
//        $product['product_size'] = $this->productSize;
//        Session::put('addToBag',$product);

        $cartInCheck = AddToBag::where('product_id',$productId)->where('user_id',Auth::id())->exists();
        if($cartInCheck){
            return redirect()->back()->with('error', 'This Product Already in your Bag');
        }else {
            AddToBag::create([
                'user_id' => Auth::id(),
                'product_id' => $productId,
                'product_qty' => $this->qty,
                'product_size' => $this->productSize,
            ]);
            $this->dispatchBrowserEvent('refresh');
        }

            return redirect()->back()->with('success', 'This Product Add in your Bag');
        }
    }

    public function increment()
    {
        $this->qty++;
    }

    public function decrement()
    {
        $this->qty--;
    }

    public function render()
    {
        $products = $this->searchSubProduct();
        $product = Product::find($this->productId);
        $addToCarts = AddToBag::where('user_id',Auth::id())->get();

        return view('livewire.details',compact('products','product','addToCarts'));
    }

    public function searchSubProduct()
    {
        $query = Product::whereStatus('1');
//        $query = Product::find($this->productId);;

        return $query->get();
    }
}
