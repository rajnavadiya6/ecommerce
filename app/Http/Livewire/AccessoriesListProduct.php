<?php

namespace App\Http\Livewire;

use App\Models\Color;
use App\Models\Product;
use App\Models\SubCategory;
use App\Models\Variant;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class AccessoriesListProduct extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    protected $queryString = ['category'];

    public $categoryFilter, $category = '';
    public $colorFilter = '';
//    public $variantFilter = [];
    public $variantFilter = '';
    public $priceFilter = '';
    public $colors, $subCategories, $variants = [];


//    protected $queryString = ['category'];

    public function mount()
    {
        $this->colors = Color::all();
        $this->subCategories = SubCategory::whereCategoryId('3')->get();
        $this->variants = Variant::all();
    }


//    /**
//     * @return string
//     */
//    public function paginationView()
//    {
//        return 'livewire.custom-pagenation';
//    }
//
//    public function nextPage($lastPage)
//    {
//        if ($this->page < $lastPage) {
//            $this->page = $this->page + 1;
//        }
//    }
//
//    public function previousPage()
//    {
//        if ($this->page > 1) {
//            $this->page = $this->page - 1;
//        }
//    }
//
//    public function updatingsearchByTicket()
//    {
//        $this->resetPage();
//    }
//
//    public function updatingfilterTickets()
//    {
//        $this->resetPage();
//    }
//
//    public function updatingcategoryFilter()
//    {
//        $this->resetPage();
//    }
//
    public function resetFilters()
    {
        $this->categoryFilter = '';
        $this->category = '';
        $this->variantFilter = '';
        $this->colorFilter = '';
        $this->resetPage();
    }

    /**
     * @return Factory|View
     */
    public function render()
    {
        $products = $this->searchAccessories();

        return view('livewire.accessories-list-product', compact('products'));
    }

    /**
     * @return Collection|Builder[]
     */
    public function searchAccessories()
    {
        if($this->category){
            $query = Product::whereStatus('1')->whereCategoryId('3')->with(['colors']);
            $query->whereHas('subCategory',function (Builder $query){
                $query->Where('name', $this->category);
            });
        }else{
            $query = Product::whereStatus('1')->whereCategoryId('3')->with('colors');
        }


//        $query->when($this->colorFilter, function (Builder $query) {
//            $query->where(function (Builder $query) {
//                $query->whereHas('colors',function (Builder $query){
//                    $query->WhereIn('color', $this->colorFilter);
//                });
//            });
//        });
        $query->when($this->colorFilter != '', function (Builder $query) {
            $query->where(function (Builder $query) {
                $query->whereHas('colors',function (Builder $query){
                    $query->Where('color', $this->colorFilter);
                });
            });
        });
        $query->when($this->variantFilter != '', function (Builder $query) {
            $query->where(function (Builder $query) {
                $query->whereHas('variants',function (Builder $query){
                    $query->Where('name', $this->variantFilter);
                });
            });
        });
//        $query->when(count($this->variantFilter) > 0 , function (Builder $query) {
//            $query->where(function (Builder $query) {
//                $query->whereHas('variants',function (Builder $query){
//                    $query->WhereIn('name', $this->variantFilter);
//                });
//            });
//        });
        $query->when($this->categoryFilter != '', function (Builder $query) {
            $query->where('sub_category_id', $this->categoryFilter);
        });

        return $query->paginate('9');
    }
}
