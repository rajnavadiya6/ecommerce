<?php

namespace App\Http\Controllers;

use App\DataTables\ColorDataTable;
use App\Http\Requests\CreateColorRequest;
use App\Http\Requests\UpdateColorRequest;
use App\Models\Color;
use App\Repositories\ColorRepository;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ColorController extends AppBaseController
{
    /** @var  ColorRepository */
    private $colorRepository;

    public function __construct(ColorRepository $colorRepo)
    {
        $this->colorRepository = $colorRepo;
    }

    /**
     * Display a listing of the Color.
     *
     * @param Request $request
     *
     * @return View|Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data['data'] = (new ColorDataTable())->get();

            return $data;
        }

        return view('colors.index');
    }

    /**
     * Store a newly created Color in storage.
     *
     * @param CreateColorRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateColorRequest $request)
    {
        $input = $request->all();

        $color = $this->colorRepository->create($input);

        return $this->sendSuccess('Color saved successfully.');
    }

    /**
     * Display the specified Color.
     *
     * @param Color $color
     *
     * @return JsonResponse
     */
    public function show(Color $color)
    {
        return $this->sendResponse($color, 'Color Retrieved Successfully.');
    }

    /**
     * Show the form for editing the specified Color.
     *
     * @param Color $color
     *
     * @return JsonResponse
     */
    public function edit(Color $color)
    {
        return $this->sendResponse($color, 'Color Retrieved Successfully.');
    }

    /**
     * Update the specified Color in storage.
     *
     * @param UpdateColorRequest $request
     * @param Color $color
     *
     * @return JsonResponse
     */
    public function update(UpdateColorRequest $request,Color $color)
    {
        $this->colorRepository->update($request->all(), $color->id);

       return $this->sendSuccess('Color updated successfully.');
    }

    /**
     * Remove the specified Color from storage.
     *
     * @param Color $color
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function destroy(Color $color)
    {
        $color->delete();

        return $this->sendSuccess('Color deleted successfully.');
    }
}
