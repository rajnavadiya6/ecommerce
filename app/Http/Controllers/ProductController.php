<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductVariant;
use App\Models\Setting;
use App\Models\SubCategory;
use App\Models\Variant;
use App\Repositories\ProductRepository;
use Flash;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Response;
use Spatie\MediaLibrary\Models\Media;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository $productRepo */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function index(Request $request)
    {
        $products = Product::with(['category', 'subCategory'])->get();

        return view('products.index')
            ->with('products', $products);
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        /** @var Category $categories */
        $categories = Category::whereHas('subCategory')->pluck('name', 'id');
        $colors = Color::all();
        $variants = Variant::all();
        $settings = Setting::pluck('value','key')->toArray();

        return view('products.create',compact('categories','colors','variants','settings'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param  CreateProductRequest  $request
     *
     * @return Redirector
     */
    public function store(CreateProductRequest $request)
    {
        $input = $request->all();

        /** @var Product $product */
        $product = $this->productRepository->create($input);
        if(isset($input['variants']) && $input['variants']){
            foreach ($input['variants'] as $index => $variant) {
                ProductVariant::create([
                    'name' => $variant,
                    'product_id' => $product->id,
                ]);
            }
        }

        if(isset($input['colors']) && $input['colors']){
            foreach ($input['colors'] as $index => $color) {
                $productColor = ProductColor::create([
                    'color' => $color,
                    'product_id' => $product->id,
                ]);
                if(isset($input['colors-img']) && $input['colors-img']){
                    foreach ($input['colors-img'][$color] as $image){
                        $productColor->addMedia($image)
                            ->toMediaCollection(ProductColor::IMG_PATH);
                    };
                }
            }
        }

        if ((isset($input['image']))) {
            $product->addMedia($input['image'])
                ->toMediaCollection(Product::IMG_PATH);
        }

        Flash::success('Product saved successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|Redirector|Response
     */
    public function show($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param  int  $id
     *
     * @return Application|RedirectResponse|Redirector|Response
     */
    public function edit($id)
    {
        $data['product'] = $this->productRepository->find($id);
        $data['colors'] = Color::all();
        $data['variants'] = Variant::all();
        $data['settings'] = Setting::pluck('value','key')->toArray();

        if (empty($data['product'])) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $data['categories'] = Category::pluck('name', 'id');
        $data['subCategories'] = SubCategory::pluck('name', 'id');

        return view('products.edit')->with($data);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  Product  $product
     *
     * @param  UpdateProductRequest  $request
     *
     * @return Application|RedirectResponse|Redirector|Response
     */
    public function update(Product $product, UpdateProductRequest $request)
    {
        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }
        $input = $request->all();
        /** @var Product $prod */
        $prod = $this->productRepository->update($input, $product->id);
        if(isset($input['variants']) && $input['variants']){
            foreach ($input['variants'] as $index => $variant) {
                ProductVariant::create([
                    'name' => $variant,
                    'product_id' => $product->id,
                ]);
            }
        }
        if(isset($input['colors']) && $input['colors']){
            foreach ($input['colors'] as $index => $color) {
                $productColor = ProductColor::create([
                    'color' => $color,
                    'product_id' => $product->id,
                ]);
                $productColor->addMedia($input['colors-img-'.$color])
                    ->toMediaCollection(ProductColor::IMG_PATH);
            }
        }
        if ((isset($input['image']))) {
            $product->clearMediaCollection(Product::IMG_PATH);
            $product->addMedia($input['image'])
                ->toMediaCollection(Product::IMG_PATH);
        }

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int  $id
     *
     * @throws \Exception
     *
     * @return Application|RedirectResponse|Redirector|Response
     */
    public function destroy($id)
    {
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }
        $product->colors()->delete();
        $product->variants()->delete();
        $product->clearMediaCollection(Product::IMG_PATH);
        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeIsActive(Request $request)
    {
        /** @var Setting $setting */
        $setting = Setting::where('key', 'product_is_active')->first();
        $setting->update(['value' => !$setting->value]);

        return $this->sendsuccess('Status changed successfully.');
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function productColorDelete(Request $request)
    {
        $input = $request->all();
        $productColor = ProductColor::find($input['id']);
        $productColor->clearMediaCollection(ProductColor::IMG_PATH);
        $productColor->delete();

        return $this->sendSuccess('Product Color Remove SuccessFully');
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function productColorImageDelete(Request $request)
    {
        $input = $request->all();
        $productColorImage = Media::find($input['id']);
        $productColorImage->delete();

        return $this->sendSuccess('Product Color Image Remove SuccessFully');
    }
}
