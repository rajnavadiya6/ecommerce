<?php

namespace App\Http\Controllers;

use App\Models\FrontSetting;
use App\Models\HomeImageSlider;
use App\Repositories\FrontSettingRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Laracasts\Flash\Flash;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig;
use Spatie\MediaLibrary\Models\Media;

class FrontSettingController extends AppBaseController
{
    /** @var  FrontSettingRepository $frontSettingRepository */
    private $frontSettingRepository;

    public function __construct(FrontSettingRepository $frontSettingRepository)
    {
        $this->frontSettingRepository = $frontSettingRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function home(Request $request)
    {
        $frontSetting = FrontSetting::pluck('value', 'key')->toArray();
        $heroImage = HomeImageSlider::all();

        return view("settings.front_settings.home", compact('frontSetting','heroImage'));
    }

    /**
     *
     * @throws DiskDoesNotExist
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     *
     * @return RedirectResponse
     */
    public function homeUpdate(Request $request)
    {
        $this->frontSettingRepository->homeUpdateSetting($request->all());

        Flash::success('Setting updated successfully.');

        return Redirect::back();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function contactUs(Request $request)
    {
        $frontSetting = FrontSetting::pluck('value', 'key')->toArray();

        return view("settings.front_settings.contact_us", compact('frontSetting'));
    }

    /**
     *
     * @throws DiskDoesNotExist
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     *
     * @return RedirectResponse
     */
    public function contactUsUpdate(Request $request)
    {
        $this->frontSettingRepository->contactUsUpdateSetting($request->all());

        Flash::success('Contact us updated successfully.');

        return Redirect::back();
    }

    public function deleteHomeSlider(HomeImageSlider $homeImageSlider)
    {
        $homeImageSlider->delete();
        
        return $this->sendSuccess('Image Deleted.'); 
    }
}
