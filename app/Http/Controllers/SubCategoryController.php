<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubCategoryRequest;
use App\Http\Requests\UpdateSubCategoryRequest;
use App\Models\Category;
use App\Models\SubCategory;
use App\Repositories\SubCategoryRepository;
use Flash;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Response;

class SubCategoryController extends AppBaseController
{
    /** @var  SubCategoryRepository */
    private $subCategoryRepository;

    public function __construct(SubCategoryRepository $subCategoryRepo)
    {
        $this->subCategoryRepository = $subCategoryRepo;
    }

    /**
     * Display a listing of the SubCategory.
     *
     * @param Request $request
     *
     * @return Application|Factory|View|Response
     */
    public function index(Request $request)
    {
        $data['subCategories'] = SubCategory::with('category')->get();

        return view('sub_categories.index')->with($data);
    }

    /**
     * Show the form for creating a new SubCategory.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        $data['categories'] = Category::pluck('name','id');

        return view('sub_categories.create')->with($data);
    }

    /**
     * Store a newly created SubCategory in storage.
     *
     * @param CreateSubCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateSubCategoryRequest $request)
    {
        $input = $request->all();

        /** @var SubCategory $subCategory */
        $subCategory = $this->subCategoryRepository->create($input);

        if ((isset($input['image']))) {
            $subCategory->addMedia($input['image'])
                ->toMediaCollection(SubCategory::IMG_PATH);
        }
        Flash::success('Sub Category saved successfully.');

        return redirect(route('subCategories.index'));
    }

    /**
     * Display the specified SubCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subCategory = $this->subCategoryRepository->find($id);

        if (empty($subCategory)) {
            Flash::error('Sub Category not found');

            return redirect(route('subCategories.index'));
        }

        return view('sub_categories.show')->with('subCategory', $subCategory);
    }

    /**
     * Show the form for editing the specified SubCategory.
     *
     * @param int $id
     *
     * @return Application|Factory|View|Response
     */
    public function edit($id)
    {
        $data['subCategory'] = $this->subCategoryRepository->find($id);

        if (empty($data['subCategory'])) {
            Flash::error('Sub Category not found');

            return redirect(route('subCategories.index'));
        }
        $data['categories'] = Category::pluck('name','id');

        return view('sub_categories.edit')->with($data);
    }

    /**
     * Update the specified SubCategory in storage.
     *
     * @param int $id
     * @param UpdateSubCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubCategoryRequest $request)
    {
        $subCategory = $this->subCategoryRepository->find($id);

        if (empty($subCategory)) {
            Flash::error('Sub Category not found');

            return redirect(route('subCategories.index'));
        }

        $input = $request->all();

        /** @var SubCategory $subCategory */
        $subCategory = $this->subCategoryRepository->update($input, $id);

        if ((isset($input['image']))) {
            $subCategory->clearMediaCollection(SubCategory::IMG_PATH);
            $subCategory->addMedia($input['image'])
                ->toMediaCollection(SubCategory::IMG_PATH);
        }

        Flash::success('Sub Category updated successfully.');

        return redirect(route('subCategories.index'));
    }

    /**
     * Remove the specified SubCategory from storage.
     *
     * @param  int  $id
     *
     * @throws \Exception
     *
     * @return Application|RedirectResponse|Redirector|Response
     */
    public function destroy($id)
    {
        /** @var SubCategory $subCategory */
        $subCategory = $this->subCategoryRepository->find($id);

        if (empty($subCategory)) {
            Flash::error('Sub Category not found');

            return redirect(route('subCategories.index'));
        }
        $subCategory->clearMediaCollection(SubCategory::IMG_PATH);
        $this->subCategoryRepository->delete($id);

        Flash::success('Sub Category deleted successfully.');

        return redirect(route('subCategories.index'));
    }

    public function getSubCategory(Request $request)
    {
        /** @var SubCategory $subCategories */
        $subCategories = SubCategory::where('category_id',$request->get('category_id'))->pluck('name','id');

        return $this->sendResponse($subCategories,'sub categories retrieved successfully');
    }
}
