<?php

namespace App\Http\Controllers;

use App\Models\AddToBag;
use App\Models\Order;
use App\Models\User;
use App\Repositories\orderRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class OrderController extends AppBaseController
{
    /** @var  orderRepository */
    private $orderRepository;

    public function __construct(orderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the Category.
     *
     * @param Request $request
     *
     * @return View
     */
    public function index(Request $request)
    {
        $orders = Order::all();

        return view('order.index',compact('orders'));
    }

    public function placeOrder(Request $request)
    {
        $addToBags = AddToBag::where('user_id',Auth::id())->get();
        foreach ($addToBags as $addToBag){
            $price = ($addToBag->product_qty*$addToBag->product->price);
            $charge = ($price*5/100);
            $totalPrice = ($price + $charge);
            Order::create([
                'product_id'          =>  $addToBag->product_id,
                'qty'                 =>  $addToBag->product_qty,
                'product_total_price' =>  $totalPrice,
                'user_id'             =>  Auth::id(),
                'product_size'        =>  $addToBag->product_size,
            ]);
        }

        return redirect(route('getOrderDetails'));
    }

    public function getOrderDetails()
    {
        return view('by_pass.order_details');
    }

    public function storeOrderDetails(Request $request)
    {
        $input = $request->all();

        $user = User::find(Auth::id());
        $user->update([
            'name' => $input['name'],
            'last_name' => $input['last_name'],
            'phone' => $input['phone'],
            'pincode' => $input['pincode'],
            'city' => $input['city'],
            'state' => $input['state'],
            'address_1' => $input['address_1'],
        ]);

        return redirect(route('mySetting'));
    }

    public function storeOrderNewAddress(Request $request)
    {
        $input = $request->all();

        $user = User::find(Auth::id());
        $user->update([
            'address_2' => $input['address_2'],
            'dob'       => $input['dob'],
        ]);

        return $this->sendSuccess('Address store successfully.');
    }

    public function chooseAddress(Request $request)
    {
        $input = $request->all();
        /** @var User $user */
        $user = User::find(Auth::id());
        $user->update(['choose_address' => $input['choose_address']]);

        return $this->sendSuccess('Address selected successfully.');
    }

    /**
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function removeProductToBag(Request $request)
    {
        $input = $request->all();
        $addToBag = AddToBag::find($input['addToBag']);
        $addToBag->delete();

        return $this->sendSuccess('This Product remove in your Bag.');

//        return redirect(route('new_arrivals'));
//        ->with('success', 'This Product remove in your Bag');
    }
}
