<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Exception;
use Flash;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class CategoryController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     *
     * @param Request $request
     *
     * @return View
     */
    public function index(Request $request)
    {
        $categories = $this->categoryRepository->all();

        return view('categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return View
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return View|RedirectResponse|Redirector
     */
    public function store(CreateCategoryRequest $request)
    {
        $input = $request->all();
        /** @var Category $category */
        $category = $this->categoryRepository->create($input);

        if ((isset($input['image']))) {
            $category->addMedia($input['image'])
                ->toMediaCollection(Category::IMG_PATH);
        }

        Flash::success('Category saved successfully.');

        return redirect(route('categories.index'));
    }

    /**
     * Display the specified Category.
     *
     * @param int $id
     *
     * @return View|RedirectResponse|Redirector
     */
    public function show($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('categories.index'));
        }

        return view('categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param int $id
     *
     * @return View|RedirectResponse|Redirector
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('categories.index'));
        }

        return view('categories.edit')->with('category', $category);
    }

    /**
     * Update the specified Category in storage.
     *
     * @param Category $category
     *
     * @param UpdateCategoryRequest $request
     *
     * @return View|RedirectResponse|Redirector
     */
    public function update(Category $category, UpdateCategoryRequest $request)
    {
        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('categories.index'));
        }
        $input = $request->all();
        /* @Category $cate */
        $cate = $this->categoryRepository->update($input, $category->id);

        if ((isset($input['image']))) {
            $cate->clearMediaCollection(Category::IMG_PATH);
            $cate->addMedia($input['image'])
                ->toMediaCollection(Category::IMG_PATH);
        }

        Flash::success('Category updated successfully.');

        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param int $id
     *
     * @throws Exception
     *
     * @return View|RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        /** @var Category $category */
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('categories.index'));
        }
        $category->clearMediaCollection(Category::IMG_PATH);
        $this->categoryRepository->delete($id);

        Flash::success('Category deleted successfully.');

        return redirect(route('categories.index'));
    }
}
