<?php

namespace App\Http\Controllers;

use App\Models\FrontSetting;
use App\Models\HomeImageSlider;
use App\Models\Product;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends AppBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function dashboard()
    {
        if(Auth::check() && Auth::user()->hasRole('Admin')) {

            return view('dashboard.dashboard');
        }

        return redirect('/');
    }

    /**
     * @param  Request  $request
     */
    public function register(Request $request)
    {
        $input = $request->all();
        $request->validate([
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed', 'min:8'],
        ]);

        $user = User::create([
            'name'     => $input['name'],
            'email'    => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
        $user->assignRole('User');

        Auth::login($user);

        return $this->sendSuccess('Register Successfully');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    public function home()
    {
        $mens = Product::where('status', '1')->whereCategoryId('1')->get();
        $womens = Product::where('status', '1')->whereCategoryId('2')->get();
        $accessories = Product::where('status', '1')->whereCategoryId('3')->get();
        $frontSetting = FrontSetting::with('media')->wherePage('home')->pluck('value', 'key')->toArray();
        $heroImage = HomeImageSlider::all();

        return view('by_pass.home', compact('mens', 'womens', 'accessories', 'frontSetting', 'heroImage'));
    }

    public function men()
    {
        $subCategoryProduct = Product::where('status', '1')->whereCategoryId('1')->get();

        return view('by_pass.new_arrivals', compact('subCategoryProduct'));
    }

    public function woMen()
    {
        $subCategoryProduct = Product::where('status', '1')->whereCategoryId('2')->get();

        return view('by_pass.women_arrivals', compact('subCategoryProduct'));
    }

    public function Accessories()
    {
        $subCategoryProduct = Product::where('status', '1')->whereCategoryId('3')->get();

        return view('by_pass.accessories', compact('subCategoryProduct'));
    }

    public function menDetails($id)
    {
        $products = Product::whereStatus('1')->get();

        return view('by_pass.details', compact('id', 'products'));
    }

    public function womenDetails($id)
    {
        return view('by_pass.women_details', compact('id'));
    }

    public function contactUs()
    {
        $frontSetting = FrontSetting::wherePage('contact_us')->pluck('value', 'key')->toArray();

        return view('by_pass.setting.contact_us', compact('frontSetting'));
    }

    public function returnExchange()
    {
        $setting = Setting::pluck('value', 'key')->toArray();

        return view('by_pass.setting.return_exchange', compact('setting'));
    }

    public function privacyPolicy()
    {
        $setting = Setting::pluck('value', 'key')->toArray();

        return view('by_pass.setting.privacy_policy', compact('setting'));
    }

    public function returnPolicy()
    {
        $setting = Setting::pluck('value', 'key')->toArray();

        return view('by_pass.setting.return_policy', compact('setting'));
    }

    public function offerPolicy()
    {
        $setting = Setting::pluck('value', 'key')->toArray();

        return view('by_pass.setting.offer_policy', compact('setting'));
    }

    public function mySetting()
    {
        return view('by_pass.mysetting');
    }
}
