<?php

namespace App\Http\Controllers;

use App\DataTables\VariantDataTable;
use App\Http\Requests\CreateVariantRequest;
use App\Http\Requests\UpdateVariantRequest;
use App\Models\Variant;
use App\Repositories\VariantRepository;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

class VariantController extends AppBaseController
{
    /** @var  VariantRepository */
    private $variantRepository;

    public function __construct(VariantRepository $variantRepo)
    {
        $this->variantRepository = $variantRepo;
    }

    /**
     * Display a listing of the Variant.
     *
     * @param Request $request
     *
     * @return View|Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data['data'] = (new VariantDataTable())->get();

            return $data;
        }

        return view('variants.index');
    }

    /**
     * Store a newly created Variant in storage.
     *
     * @param CreateVariantRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateVariantRequest $request)
    {
        $input = $request->all();

        $variant = $this->variantRepository->create($input);

        return $this->sendSuccess('Variant saved successfully.');
    }

    /**
     * Display the specified Variant.
     *
     * @param Variant $variant
     *
     * @return JsonResponse
     */
    public function show(Variant $variant)
    {
        return $this->sendResponse($variant, 'Variant Retrieved Successfully.');
    }

    /**
     * Show the form for editing the specified Variant.
     *
     * @param Variant $variant
     *
     * @return JsonResponse
     */
    public function edit(Variant $variant)
    {
        return $this->sendResponse($variant, 'Variant Retrieved Successfully.');
    }

    /**
     * Update the specified Variant in storage.
     *
     * @param UpdateVariantRequest $request
     * @param Variant $variant
     *
     * @return JsonResponse
     */
    public function update(UpdateVariantRequest $request,Variant $variant)
    {
        $this->variantRepository->update($request->all(), $variant->id);

       return $this->sendSuccess('Variant updated successfully.');
    }

    /**
     * Remove the specified Variant from storage.
     *
     * @param Variant $variant
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function destroy(Variant $variant)
    {
        $variant->delete();

        return $this->sendSuccess('Variant deleted successfully.');
    }
}
