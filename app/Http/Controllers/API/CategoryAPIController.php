<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateCategoryAPIRequest;
use App\Http\Requests\API\UpdateCategoryAPIRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

/**
 * Class CategoryController
 * @package App\Http\Controllers\API
 */
class CategoryAPIController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     * GET|HEAD /categories
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $categories = Category::all()->toArray();

        return $this->sendResponse($categories, 'Categories retrieved successfully.');
    }

    /**
     * Store a newly created Category in storage.
     * POST /categories
     *
     * @param  CreateCategoryAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateCategoryAPIRequest $request)
    {

        try {
            DB::beginTransaction();
            $input = $request->all();
            /** @var Category $category */
            $category = $this->categoryRepository->create($input);

            if (isset($input['image']) && $input['image'] != '') {
                $category->addMediaFromUrl($input['image'])
                    ->toMediaCollection(Category::IMG_PATH, config('app.media_disk'));
            }

            DB::commit();

            return $this->sendResponse($category->toArray(), 'Category saved successfully');

        } catch (Exception $e) {
            $this->categoryRepository->delete($category->id);
            DB::rollBack();
            return $this->sendError($e->getMessage());

        }

    }

    /**
     * Display the specified Category.
     * GET|HEAD /categories/{id}
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Category $category */
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            return $this->sendError('Category not found');
        }

        return $this->sendResponse($category->toArray(), 'Category retrieved successfully');
    }

    /**
     * Update the specified Category in storage.
     * PUT/PATCH /categories/{id}
     *
     * @param  int  $id
     * @param  UpdateCategoryAPIRequest  $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Category $category */
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            return $this->sendError('Category not found');
        }

        $category = $this->categoryRepository->update($input, $id);

        return $this->sendResponse($category->toArray(), 'Category updated successfully');
    }

    /**
     * Remove the specified Category from storage.
     * DELETE /categories/{id}
     *
     * @param  int  $id
     *
     * @throws Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Category $category */
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            return $this->sendError('Category not found');
        }

        $category->delete();

        return $this->sendSuccess('Category deleted successfully');
    }
}
