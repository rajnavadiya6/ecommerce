<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Order;

/**
 * Class orderRepository
 * @package App\Repositories
 * @version January 3, 2021, 10:53 am UTC
 */

class orderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order::class;
    }
}
