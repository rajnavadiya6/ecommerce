<?php

namespace App\Repositories;

use App\Models\Color;

/**
 * Class ColorRepository
 * @package App\Repositories
 * @version January 15, 2021, 9:48 am UTC
*/

class ColorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Color::class;
    }
}
