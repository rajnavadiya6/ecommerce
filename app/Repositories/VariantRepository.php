<?php

namespace App\Repositories;

use App\Models\Variant;

/**
 * Class VariantRepository
 * @package App\Repositories
 * @version January 15, 2021, 10:01 am UTC
*/

class VariantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Variant::class;
    }
}
