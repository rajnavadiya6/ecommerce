<?php

namespace App\MediaLibrary;

use App\Models\Category;
use App\Models\FrontSetting;
use App\Models\HomeImageSlider;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\Setting;
use App\Models\SubCategory;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\PathGenerator\PathGenerator;

/**
 * Class CustomPathGenerator
 * @package App\MediaLibrary
 */
class CustomPathGenerator implements PathGenerator
{
    /**
     * @param  Media  $media
     *
     * @return string
     */
    public function getPath(Media $media): string
    {
        $path = '{PARENT_DIR}'.DIRECTORY_SEPARATOR.$media->id.DIRECTORY_SEPARATOR;

        switch ($media->collection_name) {
            case Category::IMG_PATH:
                return str_replace('{PARENT_DIR}', Category::IMG_PATH, $path);
            case SubCategory::IMG_PATH:
                return str_replace('{PARENT_DIR}', SubCategory::IMG_PATH, $path);
            case Product::IMG_PATH:
                return str_replace('{PARENT_DIR}', Product::IMG_PATH, $path);
            case ProductColor::IMG_PATH:
                return str_replace('{PARENT_DIR}', ProductColor::IMG_PATH, $path);
            case Setting::IMG_PATH:
                return str_replace('{PARENT_DIR}', Setting::IMG_PATH, $path);
            case FrontSetting::IMG_PATH:
                return str_replace('{PARENT_DIR}', FrontSetting::IMG_PATH, $path);
            case HomeImageSlider::IMG_PATH:
                return str_replace('{PARENT_DIR}', HomeImageSlider::IMG_PATH, $path);
            case Setting::FOOTERIMAGE:
                return str_replace('{PARENT_DIR}', Setting::FOOTERIMAGE, $path);
            case 'default' :
                return '';
        }
    }

    /**
     * @param  Media  $media
     *
     * @return string
     */
    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media).'thumbnails/';
    }

    /**
     * @param  Media  $media
     *
     * @return string
     */
    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media).'rs-images/';
    }
}
