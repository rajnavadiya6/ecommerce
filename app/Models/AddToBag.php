<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\AddToBag
 *
 * @method static \Illuminate\Database\Eloquent\Builder|AddToBag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AddToBag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AddToBag query()
 * @mixin \Eloquent
 */
class AddToBag extends Model
{
    use HasFactory;

    protected $table = "add_to_bag";

    protected $fillable = [
        'user_id',
        'product_id',
        'product_qty',
        'product_color',
        'product_size',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
