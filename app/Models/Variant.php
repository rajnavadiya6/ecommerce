<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Variant
 * @package App\Models
 * @version January 15, 2021, 10:01 am UTC
 *
 * @property string $name
 */
class Variant extends Model
{
    use HasFactory;

    public $table = 'variants';

    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:variants,name'
    ];

}
