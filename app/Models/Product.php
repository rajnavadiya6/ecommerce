<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Product
 *
 * @package App\Models
 * @version January 14, 2021, 5:34 am UTC
 * @property Category $category
 * @property SubCategory $subCategory
 * @property integer $category_id
 * @property integer $sub_category_id
 * @property string $name
 * @property string $seller_name
 * @property integer $status
 * @property integer $price
 * @property string $description
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $category_name
 * @property-read mixed $image
 * @property-read mixed $sub_category_name
 * @property-read \Illuminate\Database\Eloquent\Collection|Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSubCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @mixin Model
 */
class Product extends Model implements HasMedia
{
    use HasMediaTrait;

    public $table = 'products';

    const IMG_PATH = 'products';

    const STATUS = [
        0 => 'UnPublish',
        1 => 'Publish',
    ];

    public $fillable = [
        'category_id',
        'sub_category_id',
        'name',
        'seller_name',
        'status',
        'price',
        'description',
        'active',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'              => 'integer',
        'category_id'     => 'integer',
        'sub_category_id' => 'integer',
        'name'            => 'string',
        'seller_name'     => 'string',
        'status'          => 'integer',
        'price'           => 'integer',
        'description'     => 'string',
        'active'          => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category_id'     => 'required',
        'sub_category_id' => 'required',
        'name'            => 'required|unique:products,name',
        'seller_name'     => 'nullable',
        'status'          => 'required',
        'price'           => 'required',
        'description'     => 'nullable',
//        'image'           => 'required',
    ];

    /**
     * @return BelongsTo
     **/
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function subCategory(): BelongsTo
    {
        return $this->belongsTo(SubCategory::class);
    }

    /**
     * @return mixed
     */
    public function getImageAttribute(): string
    {
        /** @var Media $media */
        $media = $this->getMedia(self::IMG_PATH)->first();
        if (! empty($media)) {
            return $media->getFullUrl();
        }
        if($this->colors()->count()){
            return $this->colors()->first()->image;
        }

        return asset('img/default_mens.jpg');
    }

    /**
     * @return string
     */
    public function getCategoryNameAttribute(): string
    {
        return $this->category ? $this->category->name : 'N/A';
    }

    /**
     * @return string
     */
    public function getSubCategoryNameAttribute(): string
    {
        return $this->subCategory ? $this->subCategory->name : 'N/A';
    }

    /**
     * @return HasMany
     */
    public function colors(): HasMany
    {
        return $this->hasMany(ProductColor::class);
    }

    /**
     * @return HasMany
     */
    public function variants(): HasMany
    {
        return $this->hasMany(ProductVariant::class);
    }
}
