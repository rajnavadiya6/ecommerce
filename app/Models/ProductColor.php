<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class ProductColor extends Model implements HasMedia
{
    use HasMediaTrait;

    public $table = 'product_colors';

    const IMG_PATH = 'product_colors';

    public $fillable = [
        'product_id',
        'color',
    ];

    /**
     * @return string
     */
    public function getImageAttribute()
    {
        /** @var Media $media */
        $media = $this->getMedia(self::IMG_PATH)->first();
        if (! empty($media)) {
            return $media->getFullUrl();
        }

        return 'http://webyzona.com/templates/themeforest/royal-market/images/product-7.jpg';
    }

    /**
     * @return string
     */
    public function getAllImageAttribute()
    {
        return $this->getMedia(self::IMG_PATH)->all();
    }

    /**
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
