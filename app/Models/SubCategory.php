<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


/**
 * Class SubCategory
 *
 * @package App\Models
 * @version January 14, 2021, 4:19 am UTC
 * @property Category $category
 * @property Product $products
 * @property integer $category_id
 * @property string $name
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $category_name
 * @property-read mixed $image
 * @property-read mixed $total_product
 * @property-read \Illuminate\Database\Eloquent\Collection|Media[] $media
 * @property-read int|null $media_count
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereUpdatedAt($value)
 * @mixin Model
 */
class SubCategory extends Model implements HasMedia
{
    use HasMediaTrait;

    public $table = 'sub_categories';

    const IMG_PATH = 'sub_categories';


    public $fillable = [
        'category_id',
        'name',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'category_id' => 'integer',
        'name'        => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category_id' => 'required',
        'image'       => 'required',
        'name'        => 'required',
    ];

    /**
     * @return mixed
     */
    public function getImageAttribute()
    {
        /** @var Media $media */
        $media = $this->getMedia(self::IMG_PATH)->first();
        if (! empty($media)) {
            return $media->getFullUrl();
        }

        return asset('img/logo.png');
    }

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }


    /**
     * @return mixed
     */
    public function getCategoryNameAttribute()
    {
        return $this->category ? $this->category->name : 'N/A';
    }

    /**
     * @return mixed
     */
    public function getTotalProductAttribute()
    {
        return $this->products ? $this->products()->count() : 0;
    }
}
