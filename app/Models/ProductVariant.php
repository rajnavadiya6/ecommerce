<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    public $table = 'product_variants';

    public $fillable = [
        'product_id',
        'name',
    ];
}
