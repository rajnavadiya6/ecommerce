<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Category
 *
 * @package App\Models
 * @version January 3, 2021, 9:37 am UTC
 * @property string $name
 * @property SubCategory $subCategory
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $image
 * @property-read mixed $total_sub_category
 * @property-read \Illuminate\Database\Eloquent\Collection|Media[] $media
 * @property-read int|null $media_count
 * @property-read int|null $sub_category_count
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @mixin Model
 */
class Category extends Model implements HasMedia
{
    use HasFactory,HasMediaTrait;

    const IMG_PATH = 'categories';

    public $table = 'categories';

    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:categories,name',
        'image' => 'required',
    ];

    protected $appends = ['image'];
    protected $hidden = ['media'];

    /**
     * @return mixed
     */
    public function getImageAttribute()
    {
        /** @var Media $media */
        $media = $this->getMedia(self::IMG_PATH)->first();
        if (! empty($media)) {
            return $media->getFullUrl();
        }

        return asset('img/logo.png');
    }

    /**
     * @return HasMany
     */
    public function subCategory()
    {
        return $this->hasMany(SubCategory::class);
    }


    /**
     * @return mixed
     */
    public function getTotalSubCategoryAttribute()
    {
        return $this->subCategory ? $this->subCategory()->count() : 0;
    }

}
