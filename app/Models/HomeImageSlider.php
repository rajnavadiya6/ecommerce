<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeImageSlider extends Model 
{
    use HasFactory;


    public $table = 'home_page_slider_image';

    const IMG_PATH = 'upload/banner_image';

    public $fillable = [
        'name',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'              => 'integer',
    ];

    /**
     * @return mixed
     */
    public function getImageAttribute(): string
    {
        /** @var Media $media */
        $media = $this->getMedia(self::IMG_PATH)->first();
        if (! empty($media)) {
            return $media->getFullUrl();
        }

        return asset('img/default_mens.jpg');
    }
}
