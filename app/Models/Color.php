<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Color
 * @package App\Models
 * @version January 15, 2021, 9:48 am UTC
 *
 * @property string $code
 */
class Color extends Model
{
    public $table = 'colors';

    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
}
