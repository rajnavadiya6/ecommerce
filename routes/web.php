<?php

use App\Http\Controllers\FrontSettingController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/register',[HomeController::class,'register'])->name('register');

// Home
Route::get('/', [HomeController::class, 'home'])->name('home');
// Men
Route::get('/men', [HomeController::class, 'men'])->name('new_arrivals');
Route::get('/women', [HomeController::class, 'woMen'])->name('women_arrivals');
Route::get('/accessories', [HomeController::class, 'Accessories'])->name('accessories_arrivals');
Route::get('product/{id}/details', [HomeController::class, 'menDetails'])->name('men.details');
Route::get('page/contact-us', [HomeController::class, 'contactUs'])->name('page.contact-us');
Route::get('page/return-exchange', [HomeController::class, 'returnExchange'])->name('page.return-exchange');
Route::get('page/privacy-policy', [HomeController::class, 'privacyPolicy'])->name('page.privacy-policy');
Route::get('page/return-policy', [HomeController::class, 'returnPolicy'])->name('page.return-policy');
Route::get('page/offer-policy', [HomeController::class, 'offerPolicy'])->name('page.offer-policy');
//Route::get('women/{id}/details',[HomeController::class,'womenDetails'])->name('women.details');
//Route::get('men/details',function (){
//    return view('by_pass.men_details');
//})->name('men_details');


Route::get('/welcome', function () {
    return view('web.welcome');
});
Route::get('/mens', function () {
    return view('web.mens');
})->name('mens');
Route::get('/single', function () {
    return view('web.single');
})->name('single');
Route::get('/contact', function () {
    return view('web.contact');
})->name('contact');

//Route::middleware(['auth:sanctum', 'verified','role:Admin'])->get('/dashboard', function () {
//    return view('dashboard.dashboard');
//})->name('dashboard');

//Route::group(['middleware' => ['auth:sanctum', 'verified','role:Admin']],function (){
    Route::get('dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
//});


Route::group(['middleware' => 'auth'], function () {

    Route::get('mySetting', [HomeController::class, 'mySetting'])->name('mySetting');

});


// settings routes
Route::get('settings', [SettingController::class, 'index'])->name('settings.index');
Route::post('settings', [SettingController::class, 'update'])->name('settings.update');

// front settings
Route::get('setting/home', [FrontSettingController::class, 'home'])->name('front.settings.home');
Route::post('setting/home', [FrontSettingController::class, 'homeUpdate'])->name('front.settings.home.update');
Route::delete('delete-home-slider/{homeImageSlider}', [FrontSettingController::class, 'deleteHomeSlider'])->name('front.settings.deleteHomeSlider');

Route::get('setting/contact-us', [FrontSettingController::class, 'contactUs'])->name('front.settings.contactUs');
Route::post('setting/contact-us',
    [FrontSettingController::class, 'contactUsUpdate'])->name('front.settings.contactUs.update');

// Order
Route::get('orders', [OrderController::class, 'index'])->name('orders.index');
Route::post('placeOrder', [OrderController::class, 'placeOrder'])->name('placeOrder');
Route::get('getOrderDetails', [OrderController::class, 'getOrderDetails'])->name('getOrderDetails');
Route::post('getOrderDetails', [OrderController::class, 'storeOrderDetails'])->name('storeOrderDetails');
Route::post('storeOrderNewAddress', [OrderController::class, 'storeOrderNewAddress'])->name('storeOrderNewAddress');
Route::post('chooseAddress', [OrderController::class, 'chooseAddress'])->name('chooseAddress');
Route::get('removeProductToBag', [OrderController::class, 'removeProductToBag'])->name('removeProductToBag');


Route::resource('categories', App\Http\Controllers\CategoryController::class);
Route::resource('subCategories', App\Http\Controllers\SubCategoryController::class);
Route::get('get-sub-categories',
    [App\Http\Controllers\SubCategoryController::class, 'getSubCategory'])->name('get.sub.categories');
Route::resource('products', App\Http\Controllers\ProductController::class);
Route::post('products/change-is-active',
    [App\Http\Controllers\ProductController::class, 'changeIsActive'])->name('products.change-is-active');
Route::get('productColorDelete',
    [App\Http\Controllers\ProductController::class, 'productColorDelete'])->name('productColorDelete');
Route::get('productColorImageDelete',
    [App\Http\Controllers\ProductController::class, 'productColorImageDelete'])->name('productColorImageDelete');

Route::resource('colors', App\Http\Controllers\ColorController::class);
Route::resource('variants', App\Http\Controllers\VariantController::class);


Route::group(['prefix' => 'crud-gui'], function () {
    Route::get('generator_builder',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

    Route::get('field_template',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

    Route::get('relation_field_template',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

    Route::post('generator_builder/generate',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

    Route::post('generator_builder/rollback',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

    Route::post(
        'generator_builder/generate-from-file',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
    )->name('io_generator_builder_generate_from_file');
});
